
Ext.define('WinLoja',{
        extend:'Ext.Window',
        ID : 0,
        id:'winLoja',
        modal:true,
        //width:400,
        //height:200,
        title:'Nova Loja',
        layout:'fit',
        closeAction:'destroy',

        setID:function(id){
            this.ID = id;
        }
    });



    Ext.define('LojaForm',{
         extend:'Ext.form.Panel',
         bodyStyle: 'padding:15px;',
         border: false,
         width:460,
         height:120,
         autoScroll: true,
	 defaultType: 'textfield',
         defaults: {anchor: '-19'},
         buttons:[
             {text:'Salvar',
                 icon:'images/icons/accept.png',
                 handler: function() {
                // The getForm() method returns the Ext.form.Basic instance:
                var form = this.up('form').getForm();
                var record = form.getRecord();
                 if (form.isValid()) { // Incluir verificação se o record é novo ou não
                // Updates the store
                     form.updateRecord(record);
                     // Verifica se o registro é novo

                     var regexpObj = new RegExp(/ext/);
                     if (regexpObj.test(record.id)) {
                        Ext.data.StoreManager.lookup('lojaStore').add(record);
                     }
                     Ext.data.StoreManager.lookup('lojaStore').sync();
                     Ext.data.StoreManager.lookup('lojaStore').load();
                     Ext.getCmp('winLoja').close();
                    }
                 }

         }],
        items:[{
                xtype:'textfield'
                ,fieldLabel	: 'Descrição:'
                ,name	: 'descricao'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
            },{
                xtype:'textfield'
                ,fieldLabel	: 'Localização:'
                ,name	: 'localizacao'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
            }
            ]

        });






Ext.onReady(function(){

    //Store Creation
    var storeLoja = Ext.create('LojaStore');
    //Adjust number of rows
    storeLoja.pageSize=19;
     //storeCliente.load();


    Ext.create('Ext.grid.Panel',{
        id:'gridLoja',
        store:Ext.data.StoreManager.lookup('lojaStore'),
        renderTo:'main',
        border:false,
        title:'Lojas da rede',
        height:498,
        columns:[
            {header:'Descrição',dataIndex:'descricao',flex:2},
            {header:'Localização',dataIndex:'localizacao',flex:4}
        ],
        viewConfig:{
            listeners:{
                itemdblclick: function(dataview, record, item, index, e) {
                    var winLoja = Ext.create('WinLoja');
                    winLoja.title='Atualizar Loja';
                    var lojaForm = Ext.create('LojaForm');
                    lojaForm.loadRecord(record);
                    winLoja.add(lojaForm);
                    winLoja.show();
                }
            }
        },
        dockedItems:[
            {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            id:'pagingToolbarLoja',
            emptyMsg:'Nenhum registro',
            displayMsg:'Exibindo {0} - {1} de {2} reg. localizados',
            store:Ext.data.StoreManager.lookup('lojaStore'),
            displayInfo: true
            },{
            xtype:'toolbar',
            dock:'top',
            id:'toolbarLoja',
            items:[
                {
                 text : 'Nova Loja',
                 xtype:'button',
                 icon:'images/icons/user_add.png',
                 handler:function() {
                      var record = Ext.create('Loja');
                      var winLoja = Ext.create('WinLoja');
                      var lojaForm = Ext.create('LojaForm');
                      lojaForm.loadRecord(record);
                      winLoja.add(lojaForm);
                      winLoja.show();
                    }
                },{
                 text : 'Atualizar dados da Loja',
                 xtype:'button',
                 icon:'images/icons/application_form_edit.png',
                 handler:function() {
                     if (Ext.getCmp('gridLoja').getSelectionModel().hasSelection()) {
                            var record =Ext.getCmp('gridLoja').getSelectionModel().getSelection()[0];
                            var winLoja = Ext.create('WinLoja');
                            winLoja.title='Atualizar dados da Loja';
                            var lojaForm = Ext.create('LojaForm');
                            lojaForm.loadRecord(record);
                            winLoja.add(lojaForm);
                            winLoja.show();
                        }
                    }
                }
               ]
            },{
            xtype:'toolbar',
            dock:'top',
            id:'toolbarSearchLoja',
            items:[
                {
                style:{marginLeft:'25px',marginRight:'125px'},
                xtype:"textfield",
                flex:4,
                labelWidth:75,
                fieldLabel:"Buscar por",
                name:"textBusca",
                id:"textBusca",
                enableKeyEvents:true,
                listeners: {     // Fires the filter on 3th character
                       keyup: function(campo) {
                           if (campo.getValue().length > 2) {

                                var store = Ext.getCmp('gridLoja').store;
                                store.clearFilter(true);
                                store.filter('descricao',campo.getValue());

                           }

                       }

                    }
                },{
                 text : 'Listar',
                 xtype:'button',
                 style:{marginLeft:'25px',marginRight:'80px'},
                 icon:'images/icons/script_lightning.png',
                 handler:function() {
                    var store = Ext.getCmp('gridLoja').store
                    store.clearFilter(true);
                    store.pageSize=17;
                    store.load();
                    }
                }
                ]
            }
        ]
    });



});

