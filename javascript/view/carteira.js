Ext.define('WinCarteira',{
        extend:'Ext.Window',
        ID : 0,
        id:'winCarteira',
        modal:true,
        //width:400,
        //height:200,
        title:'Definir intervalo',
        layout:'fit',
        closeAction:'destroy',

        setID:function(id){
            this.ID = id;
        }
    });

     Ext.define('CarteiraForm',{
         extend:'Ext.form.Panel',
         id:'formIntervalo',
         bodyStyle: 'padding:15px;',
         border: false,
         width:260,
         height:120,
         autoScroll: true,
	 defaultType: 'textfield',
         defaults: {anchor: '-19'},
         buttons:[
             {text:'Gerar',
                 icon:'images/icons/accept.png',
                 handler: function() {
                    store = Ext.getCmp('gridCarteira').store;
                    store.load({params:{'startDate':Ext.getCmp('startDate').getValue(),
                                        'endDate':Ext.getCmp('endDate').getValue()}
                                });
                    Ext.getCmp('winCarteira').close();                    

                 }

         }],
        items:[{
                xtype:'datefield',
                fieldLabel	: 'Data inicial:',
                name	: 'startDate',
                id	: 'startDate',
                anchor: '100%',
                submitFormat:'Y-m-d',
                submitValue:true,
                vtype: 'daterange',
                endDateField: 'endDate', // id of the end date field
                maxValue: new Date(),
                width:100
                ,allowBlank	: false
                ,maxLength	: 100
            },{
                xtype:'datefield',
                fieldLabel	: 'Data final:',
                name	: 'endDate',
                id	: 'endDate',
                anchor: '100%',
                submitFormat:'Y-m-d',
                vtype: 'daterange',
                startDateField: 'startDate', // id of the start date field
                submitValue:true,
                maxValue: new Date(),
                width:100
                ,allowBlank	: false
                ,maxLength	: 100
            }]

        });





Ext.onReady(function(){

        // Add the additional 'advanced' VTypes
    Ext.apply(Ext.form.field.VTypes, {
        daterange: function(val, field) {
            var date = field.parseDate(val);

            if (!date) {
                return false;
            }
            if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                var start = field.up('form').down('#' + field.startDateField);
                start.setMaxValue(date);
                start.validate();
                this.dateRangeMax = date;
            }
            else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                var end = field.up('form').down('#' + field.endDateField);
                end.setMinValue(date);
                end.validate();
                this.dateRangeMin = date;
            }           
           
            return true;
        },

        daterangeText: 'Data inicial deve ser menor que data final.'
         });

    //Store Creation

   
   var storeCarteira = Ext.create('CarteiraStore');
   Ext.data.StoreManager.lookup('carteiraStore').setProxy(new Ext.data.proxy.Ajax({
                                                                    model: 'Carteira',
                                                                    reader: 'json',
                                                                     api: {
                                                                        read:'php/ajax/carteira.php?action=getSituacao'
                                                                        },
                                                                    reader: {
                                                                        type:'json',
                                                                        root:'rows',
                                                                        totalProperty:'totalCount'
                                                                        }
                                                                    }
                                                                )
                                                                );    
       
    //Adjust number of rows
    storeCarteira.pageSize=17;

    Ext.create('Ext.grid.Panel',{
        id:'gridCarteira',
        store:Ext.data.StoreManager.lookup('carteiraStore'),
        renderTo:'main',
        border:false,
        title:'Recebimentos - Total de Vendas',
        height:498,
        columns:[
            {header:'Modalidade de pagamento',dataIndex:'descricao',flex:4},
            {header:'valor',dataIndex:'valor',flex:2,renderer:function(v){return Ext.util.Format.currency(v, 'R$',2);}}
        ],        
        dockedItems:[
            {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            id:'pagingToolbarCarteira',
            emptyMsg:'Nenhum registro',
            displayMsg:'Exibindo {0} - {1} de {2} reg. localizados',
            store:Ext.data.StoreManager.lookup('carteiraStore'),
            displayInfo: true
            },{
            xtype:'toolbar',
            dock:'top',
            id:'toolbarCarteira',
            items:[
                {
                 text : 'Definir intervalo',
                 xtype:'button',
                 icon:'images/icons/user_add.png',
                 handler:function() {
                     
                      var winCarteira = Ext.create('WinCarteira');
                      var carteiraForm = Ext.create('CarteiraForm');
                      winCarteira.add(carteiraForm);
                      winCarteira.show();
                    }
                },{
                 text : 'Hoje',
                 xtype:'button',
                 style:{marginLeft:'25px'},
                 icon:'images/icons/script_lightning.png',
                 handler:function() {
                     store = Ext.getCmp('gridCarteira').store;
                     store.load({params:{'startDate': new Date(),
                                        'endDate': new Date()}
                                });
                    }
                     
                   
                },{
                 text : 'Este mês',
                 xtype:'button',
                 style:{marginLeft:'25px',marginRight:'80px'},
                 icon:'images/icons/script_lightning.png',
                 handler:function() {
                     var fim = new Date();
                     var inicio = new Date();
                     inicio.setDate(1);
                     store = Ext.getCmp('gridCarteira').store;
                     store.load({params:{'startDate': inicio,
                                        'endDate': fim}
                                });
                    }


                }
               ]
            }
        ]
    });



});
