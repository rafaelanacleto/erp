function fieldValueToFloat(Strvalue) {

    var Strvalue=Strvalue.replace("R$","");
    var Strvalue=Strvalue.replace(" ","");
    //var Strvalue=Strvalue.replace(".","");
    var Strvalue=Strvalue.replace(",",".");
    return parseFloat(Strvalue);
}

function formatValue(value) {
    return Ext.util.Format.currency(value, 'R$ ',2);
}


Ext.define('WinCaixa',{
        extend:'Ext.Window',
        ID : 0,
        id:'winCaixa',
        modal:true,
        //width:400,
        //height:200,
        title:'Caixa',
        layout:'fit',
        closeAction:'destroy',

        setID:function(id){
            this.ID = id;
        }
    });

     Ext.define('CaixaCloseForm',{
         extend:'Ext.form.Panel',
         id:'formCaixaClose',
         bodyStyle: 'padding:15px;',
         border: false,
         width:300,
         height:276,
         autoScroll: true,
	 defaultType: 'textfield',
         defaults: {anchor: '-19'},
         buttons:[
             {text:'Fechar',
                 icon:'images/icons/accept.png',
                 handler: function() {
                var form = this.up('form').getForm();
                var record = form.getRecord();
                 if (form.isValid()) {
                     form.updateRecord(record);
                     storeRecord = Ext.data.StoreManager.lookup('storeCaixaClose').getAt(0);
                     storeRecord.set('troco_inicial',fieldValueToFloat(Ext.getCmp('troco_inicial').getRawValue()));
                     storeRecord.set('entradas',fieldValueToFloat(Ext.getCmp('entradas').getRawValue()));                     
                     storeRecord.set('sangrias',fieldValueToFloat(Ext.getCmp('sangrias').getRawValue()));
                     storeRecord.set('retirada',fieldValueToFloat(Ext.getCmp('retirada').getRawValue()));
                     storeRecord.set('sobra',fieldValueToFloat(Ext.getCmp('sobra').getRawValue()));
                     storeRecord.set('falta',fieldValueToFloat(Ext.getCmp('falta').getRawValue()));                     
                     storeRecord.set('troco_final',fieldValueToFloat(Ext.getCmp('troco_final').getRawValue()));
                     //Valores disponíveis globalmente
                     storeRecord.set('usuario_id',id_usuario);
                     storeRecord.set('origem_loja_id',id_loja);

                     Ext.data.StoreManager.lookup('storeCaixaClose').sync();
                     Ext.data.StoreManager.lookup('storeCaixaOpen').load();
                     Ext.data.StoreManager.lookup('caixaStore').load();
                     Ext.data.StoreManager.lookup('storeCaixaClose').load();
                     Ext.getCmp('winCaixa').close();
                    }
                 }

         }],
        items:[{
                xtype:'field-money',
                labelWidth:140,
                width:210,
                fieldLabel	: 'Troco Inicial',
                name	: 'troco_inicial',
                id	: 'troco_inicial',
                allowZero      : true
                ,allowBlank	: false,
                readOnly:true
                ,maxLength	: 100
            },{
                xtype:'field-money',
                fieldLabel	: 'Entradas em Dinheiro',
                labelWidth:140,
                name	: 'entradas',
                id	: 'entradas',
                width:210,
                allowZero      : true,
                readOnly:true
                ,allowBlank	: false                
                ,maxLength	: 100
            },{
               xtype:'field-money',
                fieldLabel	: 'Total do Caixa',

                name	: 'gaveta',
                id	: 'gaveta',
                labelWidth:140,
                width:210,
                allowBlank	: false,
                allowZero      : true,
                readOnly:true
                ,maxLength	: 100
            },{
               xtype:'field-money',
                fieldLabel	: 'Depósitos efetuados',
                name	: 'sangrias',
                id	: 'sangrias',
                labelWidth:140,
                width:210,
                allowBlank	: false,
                allowZero      : true,
                readOnly:true,
                allowNegative : true
                ,maxLength	: 100
            },{
               xtype:'field-money',
                fieldLabel	: 'Depósito de fechamento',
                name	: 'retirada',
                id	: 'retirada',
                labelWidth:140,
                width:210,
                allowBlank	: false,
                allowZero      : true,
                maxLength	: 100,
                listeners:{
                change:function( formField,novoValor,antValor,e ){
                        max=fieldValueToFloat(Ext.getCmp('gaveta').getRawValue());
                        curr=fieldValueToFloat(novoValor);
                        falta=fieldValueToFloat(Ext.getCmp('falta').getRawValue());
                        sobra=fieldValueToFloat(Ext.getCmp('sobra').getRawValue());
                        if (curr > max) {
                            formField.setValue(formatValue(max));
                        }
                        Ext.getCmp('troco_final').setValue(formatValue(max-curr-falta+sobra));
                    }
                }
            },{
                xtype:'field-money',
                fieldLabel	: 'Sobra',
                name	: 'sobra',
                id	: 'sobra',
                labelWidth:140,
                width:210,
                allowBlank	: false,
                allowZero      : true,
                readOnly:false,
                maxLength	: 100,
                listeners:{
                change:function( formField,novoValor,antValor,e ){
                    if (fieldValueToFloat(formField.getRawValue()) != 0) {
                        Ext.getCmp('falta').setValue(formatValue(0));
                       }
                       gaveta = fieldValueToFloat(Ext.getCmp('gaveta').getRawValue());
                       retirada = fieldValueToFloat(Ext.getCmp('retirada').getRawValue());
                       falta=fieldValueToFloat(Ext.getCmp('falta').getRawValue());
                       sobra=fieldValueToFloat(Ext.getCmp('sobra').getRawValue());
                       Ext.getCmp('troco_final').setValue(formatValue(gaveta-retirada-falta+sobra));                      
                    }
                 }
            },{
                xtype:'field-money',
                fieldLabel	: 'Falta',
                name	: 'falta',
                id	: 'falta',
                labelWidth:140,
                width:210,
                allowBlank	: false,
                allowZero      : true,
                readOnly:false,
                maxLength: 100,
                 listeners:{
                change:function( formField,novoValor,antValor,e ){
                       if (fieldValueToFloat(formField.getRawValue()) != 0) {
                        Ext.getCmp('sobra').setValue(formatValue(0));
                       }
                       gaveta = fieldValueToFloat(Ext.getCmp('gaveta').getRawValue());
                       retirada = fieldValueToFloat(Ext.getCmp('retirada').getRawValue());
                       falta=fieldValueToFloat(Ext.getCmp('falta').getRawValue());
                       sobra=fieldValueToFloat(Ext.getCmp('sobra').getRawValue());
                       Ext.getCmp('troco_final').setValue(formatValue(gaveta-retirada-falta+sobra));                      
                    }
                 }
            },{
                xtype:'field-money',
                fieldLabel	: 'Troco Final',                
                name	: 'troco_final',
                id	: 'troco_final',
                labelWidth:140,
                width:210,
                allowBlank	: false,
                allowZero      : true,
                readOnly:true
                ,maxLength	: 100
            }]
        });

     Ext.define('CaixaOpenForm',{
         extend:'Ext.form.Panel',
         id:'formCaixaOpen',
         bodyStyle: 'padding:15px;',
         border: false,
         width:270,
         height:140,
         autoScroll: true,
	 defaultType: 'textfield',
         defaults: {anchor: '-19'},
         buttons:[
             {text:'Abrir',
                 icon:'images/icons/accept.png',
                 handler: function() {
                var form = this.up('form').getForm();
                var record = form.getRecord();
                 if (form.isValid()) {
                     storeRecord = Ext.data.StoreManager.lookup('storeCaixaOpen').getAt(0);
                     storeRecord.set('troco_inicial_const',fieldValueToFloat(Ext.getCmp('troco_inicial_const').getRawValue()));
                     storeRecord.set('usuario_id',id_usuario);
                     storeRecord.set('origem_loja_id',id_loja);
                     Ext.data.StoreManager.lookup('storeCaixaOpen').sync();
                     Ext.data.StoreManager.lookup('caixaStore').load();
                     Ext.data.StoreManager.lookup('storeCaixaClose').load();
                     Ext.data.StoreManager.lookup('storeCaixaOpen').load();
                     Ext.data.StoreManager.lookup('caixaStore').load();
                     Ext.getCmp('winCaixa').close();
                    }
                 }
            }],
            items:[{
                xtype:'field-money',
                fieldLabel	: 'Troco inicial presumido',
                name	: 'troco_inicial_pres',
                id	: 'troco_inicial_pres',
                labelWidth:80,
                width:180,
                allowBlank	: false,
                allowZero      : true,
                readOnly:true
                ,maxLength	: 100
            },{
                xtype:'field-money',
                fieldLabel	: 'Troco inicial constatado',
                name	: 'troco_inicial_const',
                id	: 'troco_inicial_const',
                labelWidth:80,
                width:180,
                allowBlank	: false,
                allowZero      : true,
                readOnly:false
                ,maxLength	: 100
            }]
        });

     Ext.define('CaixaSangriaForm',{
         extend:'Ext.form.Panel',
         id:'formCaixaSangria',
         bodyStyle: 'padding:15px;',
         border: false,
         width:280,
         height:187,
         autoScroll: true,
	 defaultType: 'textfield',
         defaults: {anchor: '-19'},
         buttons:[
             {text:'Gravar',
                 icon:'images/icons/accept.png',
                 handler: function() {
                var form = this.up('form').getForm();
                var record = form.getRecord();
                 if (form.isValid()) {
                     var model = Ext.ModelManager.getModel(Ext.data.StoreManager.lookup('storeCaixaSangria').model);
                     storeRecord =  new Ext.create(model);                     
                     storeRecord.set('retirada',fieldValueToFloat(Ext.getCmp('retirada').getRawValue()));
                     storeRecord.set('historico',(Ext.getCmp('historico').getValue()));
                     //Valores disponíveis globalmente
                     storeRecord.set('usuario_id',id_usuario);
                     storeRecord.set('origem_loja_id',id_loja);
                     Ext.data.StoreManager.lookup('storeCaixaSangria').add(storeRecord);
                     Ext.data.StoreManager.lookup('storeCaixaSangria').sync();
                     Ext.data.StoreManager.lookup('storeCaixaOpen').load();
                     Ext.data.StoreManager.lookup('storeCaixaClose').load();
                     Ext.data.StoreManager.lookup('caixaStore').load();
                     Ext.getCmp('winCaixa').close();
                    }
                 }

            }],
            items:[{
               xtype:'field-money',
                fieldLabel	: 'Total na gaveta',

                name	: 'gaveta',
                id	: 'gaveta',
                labelWidth:140,
                width:218,
                allowBlank	: false,
                allowZero      : true,
                readOnly    :true,
                maxLength	: 100
            },{
               xtype:'field-money',
                fieldLabel	: 'Valor retirado',
                name	: 'retirada',
                id	: 'retirada',
                labelWidth:140,
                width:218,
                allowBlank	: false,
                allowZero      : true,
                maxLength	: 100,
                listeners:{
                change:function( formField,novoValor,antValor,e ){
                        max=fieldValueToFloat(Ext.getCmp('gaveta').getRawValue());
                        curr=fieldValueToFloat(novoValor);
                        if (curr > max) {
                            formField.setValue(formatValue(max));
                        }
                        Ext.getCmp('troco_final').setValue(formatValue(max-curr));
                    }
                }
            },{
                xtype:'field-money',
                fieldLabel	: 'Troco Restante',
                name	: 'troco_final',
                id	: 'troco_final',
                labelWidth:140,
                width:210,
                allowBlank	: false,
                allowZero      : true,
                readOnly:true
                ,maxLength	: 100
            },{
                xtype:'textfield',
                fieldLabel	: 'Histórico',
                name	: 'historico',
                id	: 'historico',
                labelWidth:80,
                width:180,
                allowBlank	: false,
                allowZero      : true
                ,maxLength	: 100
            }]
        });

Ext.onReady(function(){

  

        // Add the additional 'advanced' VTypes
    Ext.apply(Ext.form.field.VTypes, {
        daterange: function(val, field) {
            var date = field.parseDate(val);

            if (!date) {
                return false;
            }
            if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                var start = field.up('form').down('#' + field.startDateField);
                start.setMaxValue(date);
                start.validate();
                this.dateRangeMax = date;
            }
            else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                var end = field.up('form').down('#' + field.endDateField);
                end.setMinValue(date);
                end.validate();
                this.dateRangeMin = date;
            }

            return true;
        },

        daterangeText: 'Data inicial deve ser menor que data final.'
         });

    //Store Creation


    var storeCaixa = Ext.create('CaixaStore');    
    //Adjust number of rows
    storeCaixa.pageSize=18;
    storeCaixa.load();
    var storeCaixaClose = Ext.create('CaixaStoreClose');
    storeCaixaClose.load();
    var storeCaixaOpen = Ext.create('CaixaStoreOpen');
    storeCaixaOpen.load();
    var storeCaixaOpen = Ext.create('CaixaStoreSangria');

    Ext.create('Ext.grid.Panel',{
        id:'gridCaixa',
        store:Ext.data.StoreManager.lookup('caixaStore'),
        renderTo:'main',
        border:false,
        title:'Operações Caixa',
        height:498,
        columns:[ 
            {header:'Operação',dataIndex:'operacao',flex:4},
            {header:'Data',dataIndex:'up_date',flex:2,renderer:Ext.util.Format.dateRenderer('d/m/Y G:i')},
            {header:'Valor',dataIndex:'valor',flex:2,renderer:function(v){return Ext.util.Format.currency(v, 'R$',2);}},
            {header:'Histórico',dataIndex:'historico',flex:4},
            {header:'Usuário',dataIndex:'usuario',flex:4}
        ],
        dockedItems:[
            {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            id:'pagingToolbarCaixa',
            emptyMsg:'Nenhum registro',
            displayMsg:'Exibindo {0} - {1} de {2} reg. localizados',
            store:Ext.data.StoreManager.lookup('caixaStore'),
            displayInfo: true
            },{
            xtype:'toolbar',
            dock:'top',
            id:'toolbarCaixa',
            items:[
                {
                 text : 'Abrir Caixa',
                 xtype:'button',
                 icon:'images/icons/user_add.png',
                 handler:function() {
                     //storeCaixaOpen
                     var openForm = Ext.create('CaixaOpenForm');
                     var winCaixa = Ext.create('WinCaixa');
                     var record = Ext.data.StoreManager.lookup('storeCaixaOpen').getAt(0);                    
                     var num = new Number(record.get('troco_final'));
                     record.set('troco_inicial_pres',num.toFixed(2));
                     record.set('troco_inicial_const',num.toFixed(2));
                     record.set('usuario_id',id_usuario);
                     record.set('origem_loja_id',id_loja);
                     record.set('troco_inicial',num.toFixed(2));
                     openForm.loadRecord(record);
                     winCaixa.setTitle('Abertura de Caixa');
                     winCaixa.add(openForm);
                     winCaixa.show();
                     Ext.getCmp('troco_inicial_pres').focus();
                     Ext.getCmp('troco_inicial_const').focus();
                    }
                },{
                 text : 'Fechar Caixa',
                 xtype:'button',
                 icon:'images/icons/user_add.png',
                 handler:function() {
                     var closeForm = Ext.create('CaixaCloseForm');
                     var winCaixa = Ext.create('WinCaixa');
                     var record = Ext.data.StoreManager.lookup('storeCaixaClose').getAt(0);
                     var num = new Number(record.get('entradas')+record.get('troco_inicial')+record.get('sangrias'));                     
                     record.set('gaveta',num.toFixed(2));
                     var num = new Number(record.get('sangrias'));
                     record.set('sangrias',num.toFixed(2));
                     closeForm.loadRecord(record);
                     winCaixa.setTitle('Fechamento de Caixa');
                     winCaixa.add(closeForm);
                     winCaixa.show();
                     Ext.getCmp('entradas').focus();
                     Ext.getCmp('troco_inicial').focus();
                     Ext.getCmp('sangrias').focus();
                     Ext.getCmp('gaveta').focus();
                     Ext.getCmp('falta').focus();
                     Ext.getCmp('sobra').focus();
                     Ext.getCmp('retirada').focus();                      
                    }
                },{
                 text : 'Depósitos e despesas',
                 xtype:'button',                 
                 icon:'images/icons/script_lightning.png',
                 handler:function() {
                     var sangForm = Ext.create('CaixaSangriaForm');
                     var winCaixa = Ext.create('WinCaixa');
                     winCaixa.setTitle('Retirada de valor do caixa');
                     winCaixa.add(sangForm);
                     var record = Ext.data.StoreManager.lookup('storeCaixaClose').getAt(0);
                     var num = new Number(record.get('entradas')+record.get('troco_inicial')+record.get('sangrias'));
                     Ext.getCmp('gaveta').setValue(num.toFixed(2));
                     Ext.getCmp('historico').setValue('Retirada-depósito');
                     winCaixa.show();
                     Ext.getCmp('troco_final').focus();
                     Ext.getCmp('gaveta').focus();
                     Ext.getCmp('retirada').focus();
                    
                    }
                }
            ]
            }
        ]
    });

});
