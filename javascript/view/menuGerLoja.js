

 Ext.define('MainMenu', {
           extend:'Ext.Panel',
           border:false,
            width    : 158,
            height   : 498,
            style    : 'margin:0px',
            bodyStyle: 'padding:0px',
            autoScroll: true,
             lbar: [{
                        text:'Clientes',
                        width: 157,
                        menu: [
                            {text: 'Gerenciar',width: 157,
                             handler: function() { window.location = "index.php?action=Clientes" }
                            },
                            {text: 'Débitos',width: 157}
                            ]
                    },'-',{
                        text:'Movimentos',
                        menu: [                            
                            {text: 'Outras saídas',width: 157}
                            ]
                    },'-',{
                        text:'Perfil',
                        menu: [
                            {text: 'Alterar senha',width: 157},
                            {text: 'Comissões',width: 157},
                            {text: 'Fazer logoff',width: 157,
                                 handler: function() {
                                     Ext.Msg.confirm('Fazer Logoff','Deseja realmente sair do sistema?',
                                     function(btn, text){
                                            if (btn == 'yes'){
                                            window.location = "php/ajax/logout.php"
                                            }
                                            if (btn == 'no'){

                                            }
                                        })
                                 }
                             }
                        ]
                    },'-',{
                        text:'Caixa',
                        menu: [
                            {text: 'Gerenciar',width: 157,
                            handler: function() { window.location = "index.php?action=GerCaixa" }},
                            {text: 'Recebimentos',width: 157,
                            handler: function() { window.location = "index.php?action=SitCaixa" }},
                            ]
                    },'-',{
                        text:'Relatórios',
                        menu: [
                            {text: 'Vendas',width: 157}
                            ]
                    }]

       });

        Ext.onReady(function() {
            Ext.util.Format.thousandSeparator = '.';
            Ext.util.Format.decimalSeparator = ',';

           var menu = Ext.create('MainMenu');
           menu.render(Ext.getDom('menu'));
            });

           