

Ext.define('WinCatProduto',{
        extend:'Ext.Window',
        ID : 0,
        id:'winCatProduto',
        modal:true,
        //width:400,
        //height:200,
        title:'Nova Categoria',
        layout:'fit',
        closeAction:'destroy',

        setID:function(id){
            this.ID = id;
        }
    });



    Ext.define('CatProdutoForm',{
         extend:'Ext.form.Panel',
         bodyStyle: 'padding:15px;',
         border: false,
         width:460,
         height:90,
         autoScroll: true,
	 defaultType: 'textfield',
         defaults: {anchor: '-19'},
         buttons:[
             {text:'Salvar',
                 icon:'images/icons/accept.png',
                 handler: function() {
                // The getForm() method returns the Ext.form.Basic instance:
                var form = this.up('form').getForm();
                var record = form.getRecord();
                 if (form.isValid()) { // Incluir verificação se o record é novo ou não
                // Updates the store
                     form.updateRecord(record);
                     // Verifica se o registro é novo

                     var regexpObj = new RegExp(/ext/);
                     if (regexpObj.test(record.id)) {
                        Ext.data.StoreManager.lookup('catProdutoStore').add(record);
                     }
                     Ext.data.StoreManager.lookup('catProdutoStore').sync();
                     Ext.data.StoreManager.lookup('catProdutoStore').load();
                     Ext.getCmp('winCatProduto').close();
                    }
                 }

         }],
        items:[{
                xtype:'textfield'
                ,fieldLabel	: 'Descrição:'
                ,name	: 'descricao'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
            }
            ]

        });






Ext.onReady(function(){   

    //Store Creation
    var storeCatproduto = Ext.create('CatProdutoStore');
    //Adjust number of rows
    storeCatproduto.pageSize=19;
     //storeCliente.load();
    

    Ext.create('Ext.grid.Panel',{
        id:'gridCatProduto',
        store:Ext.data.StoreManager.lookup('catProdutoStore'),
        renderTo:'main',
        border:false,
        title:'Categorias de Produtos',
        height:498,
        columns:[
            {header:'Descrição',dataIndex:'descricao',flex:1}
        ],
        viewConfig:{
            listeners:{
                itemdblclick: function(dataview, record, item, index, e) {
                    var winCatProduto = Ext.create('WinCatProduto');
                    winCatProduto.title='Atualizar Cadastro';
                    var catProdutoForm = Ext.create('CatProdutoForm');
                    catProdutoForm.loadRecord(record);
                    winCatProduto.add(catProdutoForm);
                    winCatProduto.show();
                }
            }
        },
        dockedItems:[
            {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            id:'pagingToolbarCatProduto',
            emptyMsg:'Nenhum registro',
            displayMsg:'Exibindo {0} - {1} de {2} reg. localizados',
            store:Ext.data.StoreManager.lookup('catProdutoStore'),
            displayInfo: true
            },{
            xtype:'toolbar',
            dock:'top',
            id:'toolbarCatProduto',
            items:[
                {
                 text : 'Nova Categoria',
                 xtype:'button',
                 icon:'images/icons/user_add.png',
                 handler:function() {
                      var record = Ext.create('CatProduto');
                      var winCatProduto = Ext.create('WinCatProduto');
                      var catProdutoForm = Ext.create('CatProdutoForm');
                      catProdutoForm.loadRecord(record);
                      winCatProduto.add(catProdutoForm);
                      winCatProduto.show();
                    }
                },{
                 text : 'Atualizar Categoria',
                 xtype:'button',
                 icon:'images/icons/application_form_edit.png',
                 handler:function() {
                     if (Ext.getCmp('gridCatProduto').getSelectionModel().hasSelection()) {
                            var record =Ext.getCmp('gridCatProduto').getSelectionModel().getSelection()[0];
                            var winCatProduto = Ext.create('WinCatProduto');
                            winCatProduto.title='Atualizar Categoria';
                            var catProdutoForm = Ext.create('CatProdutoForm');
                            catProdutoForm.loadRecord(record);
                            winCatProduto.add(catProdutoForm);
                            winCatProduto.show();
                        }
                    }
                }
               ]
            },{
            xtype:'toolbar',
            dock:'top',
            id:'toolbarSearchCatProduto',
            items:[
                {
                style:{marginLeft:'25px',marginRight:'125px'},
                xtype:"textfield",
                flex:4,
                labelWidth:75,
                fieldLabel:"Buscar por",
                name:"textBusca",
                id:"textBusca",
                enableKeyEvents:true,
                listeners: {     // Fires the filter on 3th character
                       keyup: function(campo) {
                           if (campo.getValue().length > 2) {

                                var store = Ext.getCmp('gridCatProduto').store;
                                store.clearFilter(true);
                                store.filter('descricao',campo.getValue());

                           }

                       }

                    }
                },{
                 text : 'Listar',
                 xtype:'button',
                 style:{marginLeft:'25px',marginRight:'80px'},
                 icon:'images/icons/script_lightning.png',
                 handler:function() {
                    var store = Ext.getCmp('gridCatProduto').store
                    store.clearFilter(true);
                    store.pageSize=17;
                    store.load();
                    }
                }
                ]
            }
        ]
    });



});

