
function fieldValueToFloat(Strvalue) {

    var Strvalue=Strvalue.replace("R$","");
    var Strvalue=Strvalue.replace(" ","");
    //var Strvalue=Strvalue.replace(".","");
    var Strvalue=Strvalue.replace(",",".");
    return parseFloat(Strvalue);
}

function formatValue(value) {
    return Ext.util.Format.currency(value, 'R$',2);
}

function applyFieldsFormat() {
    Ext.getCmp('preco_normal').focus();
    Ext.getCmp('preco_promo').focus();
    Ext.getCmp('custo').focus();
    Ext.getCmp('descricao').focus();
}


Ext.define('WinEntrada',{
        extend:'Ext.Window',
        ID : 0,
        id:'winEntrada',
        modal:true,
        //width:460,
        //height:300,
        title:"Entrada de mercadorias",
        layout:'fit',
        closeAction:'destroy',

        setID:function(id){
            this.ID = id;
        }
    });

Ext.define('FormEntrada',{
    extend:'Ext.form.Panel',
    renderTo:'main',
    id:'formEntrada',
    bodyStyle:"padding:15px",   
    width:780,
    border: false,
    buttons:[
        { text : 'Gravar Movimento',
            xtype:'button',
            scope:this,
            id:'saveMovButton',
            icon:'images/icons/tick.png',
            disabled:true,            
            handler:function() {
                var store = Ext.getCmp('resEntradaGrid').getStore();
                store.each(function(record){
                   Ext.data.StoreManager.lookup('entradaStore').add(record);
                });
                Ext.data.StoreManager.lookup('entradaStore').sync();
                store.removeAll();
            }
        }

    ],
    items:[{
        xtype:"combobox",
        fieldLabel:"Loja:",
        name:"loja_id",
        id:'comboLoja',
        querymode:'remote',
        valueField:'id',
        displayField:'descricao',
        width:360,
        minWidth:360,
        maxWidth:360,
        listeners:{
            change:function() {    // Ativa combo do produto e atualiza lista para o fornecedor
            }
        }
  },{
    xtype:"combobox",
    fieldLabel:"Fornecedor:",
    name:"fornecedor_id",
    id:'comboFornecedor',
    querymode:'remote',
    valueField:'id',
    displayField:'descricao',
    width:360,
    minWidth:360,
    maxWidth:360,
    listeners:{
        change:function() {    // Ativa combo do produto e atualiza lista para o fornecedor
           Ext.getCmp('comboProduto').reset();
           Ext.getCmp('textReferencia').setValue('');
           Ext.getCmp('textReferencia').disable();
           Ext.getCmp('textNotaEntrada').setValue('');
           Ext.getCmp('textNotaEntrada').disable();
           Ext.getCmp('textQuantidade').setValue('');
           Ext.getCmp('textQuantidade').disable();
           Ext.getCmp('textValor').setValue('');
           Ext.getCmp('textValor').disable();           
           Ext.data.StoreManager.lookup('produtoStore').clearFilter(true);
           Ext.data.StoreManager.lookup('produtoStore').filter('fornecedor_id',Ext.getCmp('comboFornecedor').getValue());
           Ext.getCmp('comboProduto').enable(); 
        }
    }
  },{
    xtype:"combobox",
    fieldLabel:"Produto",
    id:'comboProduto',
    name:"produto_id",
    valueField:'id',
    displayField:'descricao',
    width:360,
    minWidth:360,
    maxWidth:360,
    disabled:true,
    listeners:{
        select:function()  {
            var id_produto = Ext.getCmp('comboProduto').getValue();
            var referencia=Ext.getCmp('comboProduto').store.findRecord('id', id_produto).get('referencia');
            Ext.getCmp('textReferencia').setValue(referencia);
            Ext.getCmp('textReferencia').enable();           
            Ext.getCmp('textNotaEntrada').enable();
            Ext.getCmp('textQuantidade').setValue('');
            Ext.getCmp('textQuantidade').enable();
            Ext.getCmp('textValor').setValue('');
            Ext.getCmp('textValor').enable();            
        }
    }

  },{
    xtype:"textfield",
    fieldLabel:"Referência:",
    name:"referencia",
    id:'textReferencia',
    width:260,
    minWidth:260,
    maxWidth:260,
    disabled:true,
    readOnly:true
  },{
    xtype:"textfield",
    fieldLabel:"Nota de Entrada:",
    name:"numNota",
    id:'textNotaEntrada',
    width:260,
    minWidth:260,
    maxWidth:260,
    maxLength:128,
    disabled:true
  },{
    xtype:"numberfield",
    fieldLabel:"Quantidade",
    name:"quantidade",
    id:'textQuantidade',
    value:'0',
    width:260,
    minWidth:260,
    maxWidth:260,
    disabled:true,
     listeners:{
         change:function(){             
             if (Ext.getCmp('textQuantidade').value > 0 && Ext.getCmp('textValor').value > 0) {
                Ext.getCmp('novoItemButton').enable();
             }else {
                Ext.getCmp('novoItemButton').disable(); 
             }
         }
     }
  },{
    xtype:"numberfield",
    fieldLabel:"Valor unitário R$",
    name:"valor",
    id:'textValor',
    value:'0',
    width:260,
    maxWidth:260,
    minWidth:260,
    disabled:true,
     listeners:{
         change:function(){             
             if (Ext.getCmp('textValor').value > 0 && Ext.getCmp('textQuantidade').value > 0) {
                Ext.getCmp('novoItemButton').enable();
             }else {
                Ext.getCmp('novoItemButton').disable(); 
             }
         }
     }
  },{
    tbar: [{
            text : 'Novo Item',
            xtype:'button',
            scope:this,
            id:'novoItemButton',
            icon:'images/icons/add.png',
            disabled:true,
            handler:function() { //Cria record do grid completa e acrescenta
                var record = Ext.create('Entrada');
                record.set('fornecedor_desc',Ext.getCmp('comboFornecedor').getDisplayValue());
                record.set('tipo_movimento_id',1);
                record.set('destino_estoque_id',Ext.getCmp('comboLoja').getValue());
                record.set('nota',Ext.getCmp('textNotaEntrada').getValue());
                record.set('produto_referencia',Ext.getCmp('textReferencia').getValue());
                record.set('produto_id',Ext.getCmp('comboProduto').getValue());
                record.set('produto_descricao',Ext.getCmp('comboProduto').getDisplayValue());
                record.set('quantidade',Ext.getCmp('textQuantidade').getValue()); 
                record.set('valor_unit',Ext.getCmp('textValor').getValue());
                record.set('entregue','Sim');
                //Calculo do total do item
                var qtd_item = new Number(Ext.getCmp('textQuantidade').getValue());
                var val_unit = new Number(Ext.getCmp('textValor').getValue());
                var total_item = qtd_item*val_unit;
                record.set('valor_total_item',total_item);
                //Adiciona ao store
                Ext.getCmp('resEntradaGrid').getStore().add(record);
                
               //Item incluído, zera os campos
                Ext.getCmp('textReferencia').setValue('');
                Ext.getCmp('textReferencia').disable();
                Ext.getCmp('textQuantidade').setValue('');
                Ext.getCmp('textQuantidade').disable();
                Ext.getCmp('textValor').setValue('');
                Ext.getCmp('textValor').disable(); 
                Ext.getCmp('comboProduto').reset();
                
                //Remove marcação de remoção de registro
                Ext.getCmp('resEntradaGrid').getSelectionModel().deselectAll();
                Ext.getCmp('removeItemButton').disable();

                var qtd = Ext.getCmp('resEntradaGrid').getStore().getNewRecords().length;
                    if (qtd > 0) {
                Ext.getCmp('saveMovButton').enable();
                 }else {
                    Ext.getCmp('saveMovButton').disable();
                 }

                
                
            }
       },{ text : 'Remover Item',
            xtype:'button',
            scope:this,
            icon:'images/icons/delete.png',
            disabled:true,
            id:'removeItemButton',
            handler:function() {
                var store = Ext.getCmp('resEntradaGrid').getStore();
                var selModel = Ext.getCmp('resEntradaGrid').getSelectionModel();
                store.remove(selModel.getSelection());
                Ext.getCmp('removeItemButton').disable();
                var qtd = Ext.getCmp('resEntradaGrid').getStore().getNewRecords().length;
                    if (qtd > 0) {
                Ext.getCmp('saveMovButton').enable();
                 }else {
                    Ext.getCmp('saveMovButton').disable();
                 }
                
            }
       
       }]
      
  },{
    xtype:'grid',
    id:'resEntradaGrid',
    store:Ext.data.StoreManager.lookup('entradaStore'),
    height: 200,
    columns:[
        {text: 'Produto',flex:1,dataIndex: 'produto_descricao'},
        {text: 'Valor Unit.',dataIndex: 'valor_unit'},
        {text: 'Qtd',dataIndex: 'quantidade'},
        {text: 'Total Item',dataIndex: 'valor_total_item'},
        {text: 'Nota',dataIndex: 'nota'},
        {text: 'Referência',dataIndex: 'produto_referencia'},
        {text: 'Fornecedor',dataIndex: 'fornecedor_desc'}

    ],viewConfig: {
            listeners: {
                itemdblclick: function(dataview, record, item, index, e) {
                },
                itemclick:function(dataview, record, item, index, e){
                 Ext.getCmp('removeItemButton').enable();
                }
            }
        }
  }]
});

Ext.onReady(function(){


Ext.util.Format.thousandSeparator = '.';
Ext.util.Format.decimalSeparator = ',';

    var produtoStore = Ext.create('ProdutoStore');
    var lojaStore = Ext.create('LojaStore');
    var fornecedorStore = Ext.create('FornecedorStore');
    var entradaStore = Ext.create('EntradaStore');    
   
    fornecedorStore.pageSize=2000;
    produtoStore.pageSize=2000;
    lojaStore.pageSize=2000;

    fornecedorStore.load();
    //produtoStore.load();
    lojaStore.load();


    Ext.create ('Ext.grid.Panel',{
        id:'gridEntradas',
        store:Ext.data.StoreManager.lookup('entradaStore'),
        border:false,
        renderTo:'main',
        width: 840,
        height: 498,
        title: 'Lista de Entradas',
         columns: [
            {text: 'Data', dataIndex: 'up_date', renderer: Ext.util.Format.dateRenderer('d/m/Y') },
            {text: 'Loja',dataIndex: 'destino_estoque_desc'},
            {text: 'Qtd Itens',dataIndex: 'quantidade_itens'},
            {text: 'Custo Total',dataIndex: 'valor_total',renderer:function(v){return Ext.util.Format.currency(v, 'R$',2);}},
            //{text: 'Venda Potencial',dataIndex: 'valor_total',renderer:function(v){return Ext.util.Format.currency(v, 'R$',2);}},
            {text: 'Usuário',dataIndex: 'usuario_nome'},
            {text: 'Nota nº',dataIndex: 'nota'},
        ],
        viewConfig: {
            listeners: {
                itemdblclick: function(dataview, record, item, index, e) {
                   
                }
            }
        },
        dockedItems:[
            {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            id:'pagingToolbarProduto',
            emptyMsg:'Nenhum registro',
            displayMsg:'Exibindo {0} - {1} de {2} reg. localizados',
            store:Ext.data.StoreManager.lookup('entradaStore'),
            displayInfo: true
            },{
            xtype:'toolbar',
            dock:'top',
            id:'toolbarEntrada',
            items:[{
                 text : 'Nova Entrada',
                 xtype:'button',
                 icon:'images/icons/user_add.png',
                 handler:function() {
                          var record = Ext.create('Entrada');
                          var winEntrada = Ext.create('WinEntrada');
                          var formEntrada = Ext.create('FormEntrada');
                          // Associa os stores com os combos.
                          Ext.getCmp('comboLoja').store = Ext.data.StoreManager.lookup('lojaStore');
                          Ext.getCmp('comboFornecedor').store = Ext.data.StoreManager.lookup('fornecedorStore');
                          Ext.getCmp('comboProduto').store = Ext.data.StoreManager.lookup('produtoStore');                          
                          formEntrada.loadRecord(record);
                          winEntrada.add(formEntrada);
                          winEntrada.show();
                        }
                    }
                ]
            },{
            xtype:'toolbar',
            dock:'top',
            id:'toolbarSearchEntrada',
            items:[
                {
                style:{marginLeft:'25px',marginRight:'125px'},
                xtype:"textfield",
                flex:4,
                labelWidth:75,
                fieldLabel:"Buscar por",
                name:"textBusca",
                id:"textBusca",
                enableKeyEvents:true,
                listeners: {     // Fires the filter on 3th character
                       keyup: function(campo) {
                           if (campo.getValue().length > 2) {

                                var store = Ext.getCmp('gridEntradas').store;
                                store.clearFilter(true);
                                store.filter('loja',campo.getValue());
                           }
                       }
                    }
                },{
                 text : 'Listar',
                 xtype:'button',
                 style:{marginLeft:'25px',marginRight:'80px'},
                 icon:'images/icons/script_lightning.png',
                 handler:function() {
                    var store = Ext.getCmp('gridEntradas').store
                    store.clearFilter(true);
                    store.pageSize=17;
                    store.load();
                    }
                }
                ]
            }
        ]

        });

       
   
    });
   