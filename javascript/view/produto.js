
var recordProd = null;

//Campos e grid de busca de produtos

Ext.define('StoreCamposBusca',{
    extend:'Ext.data.Store',
    storeId:'storeCamposBusca',
    fields:['valor','texto'],
    data:[
        {'valor':'id','texto':'Código'},
        {'valor':'descricao','texto':'Descrição'}
    ]
});

function fieldValueToFloat(Strvalue) {

    var Strvalue=Strvalue.replace("R$","");
    var Strvalue=Strvalue.replace(" ","");
    //var Strvalue=Strvalue.replace(".","");
    var Strvalue=Strvalue.replace(",",".");
    return parseFloat(Strvalue);
}

function formatValue(value) {
    return Ext.util.Format.currency(value, 'R$',2);
}

function applyFieldsFormat() {
    Ext.getCmp('preco_normal').focus();
    Ext.getCmp('preco_promo').focus();
    Ext.getCmp('custo').focus();
    Ext.getCmp('descricao').focus();
}

Ext.define('AtivoStore',{
    extend:'Ext.data.Store',
    fields:['valor','texto'],
    storeId:'ativoStore',
    data:[
        {"valor":"Sim","texto":"Sim"},
        {"valor":"Não","texto":"Não"}
    ]

});

Ext.define('WinEntrada',{
        extend:'Ext.Window',
        ID : 0,
        id:'winEntrada',
        modal:true,
        //width:460,
        //height:300,
        title:"Entrada de mercadoria",
        layout:'fit',
        closeAction:'destroy',

        setID:function(id){
            this.ID = id;
        }
    });

Ext.define('FormEntrada',{
    extend:'Ext.form.Panel',
    renderTo:'main',
    id:'formEntrada',
    bodyStyle:"padding:15px",
    width:390,
    border: false,
    buttons:[
        { text : 'Gravar Entrada',
            xtype:'button',
            scope:this,
            id:'saveMovButton',
            icon:'images/icons/tick.png',
            disabled:true,
            handler:function() {
                var entradaRecord = Ext.create('Entrada');
                entradaRecord.set('destino_estoque_id',Ext.getCmp('comboLoja').getValue());
                entradaRecord.set('produto_id',recordProd.get('id'));
                entradaRecord.set('produto_referencia',recordProd.get('referencia'));
                entradaRecord.set('nota',Ext.getCmp('textNotaEntrada').value);
                entradaRecord.set('quantidade',Ext.getCmp('textQuantidade').value);
                entradaRecord.set('valor_unit',fieldValueToFloat(Ext.getCmp('custo').getRawValue()));
                Ext.data.StoreManager.lookup('entradaStore').add(entradaRecord);
                Ext.data.StoreManager.lookup('entradaStore').sync();
                Ext.data.StoreManager.lookup('produtoStore').load();
                Ext.getCmp('winEntrada').close();
            }
        }

    ],
    items:[{
        xtype:"combobox",
        fieldLabel:"Loja:",
        name:"loja_id",
        id:'comboLoja',
        querymode:'remote',
        valueField:'id',
        displayField:'descricao',
        width:360,
        minWidth:360,
        maxWidth:360,
        listeners:{
            change:function() {    // Ativa combo do produto e atualiza lista para o fornecedor
            }
        }
  },{
    xtype:"textfield",
    fieldLabel:"Fornecedor:",
    name:"fornecedor_descricao",
    id:'fornecedor_descricao',
    width:360,
    minWidth:360,
    maxWidth:360,   
    readOnly:true
  },{
    xtype:"textfield",
    fieldLabel:"Produto",
    id:'descricao',
    name:"descricao",
    width:360,
    minWidth:360,
    maxWidth:360,   
    readOnly:true
  },{
    xtype:"textfield",
    fieldLabel:"Referência:",
    name:"referencia",
    id:'referencia',
    width:260,
    minWidth:260,
    maxWidth:260,    
    readOnly:true
  },{
    xtype:"textfield",
    fieldLabel:"Nota de Entrada:",
    name:"numNota",
    id:'textNotaEntrada',
    width:260,
    minWidth:260,
    maxWidth:260,
    maxLength:128,
    emptyText:'Sem número'
  },{
    xtype:"numberfield",
    fieldLabel:"Quantidade",
    name:"quantidade",
    id:'textQuantidade',
    value:'0',
    width:260,
    minWidth:260,
    maxWidth:260,    
     listeners:{
         change:function(){
             if (Ext.getCmp('textQuantidade').value > 0 && fieldValueToFloat(Ext.getCmp('custo').getRawValue()) > 0) {
                Ext.getCmp('saveMovButton').enable();
             }else {
                Ext.getCmp('saveMovButton').disable();
             }
         }
     }
  },{
    xtype: 'field-money',
    fieldLabel:"Custo",
    name:"custo",
    id:'custo',
    value:'0',
    width:260,
    maxWidth:260,
    minWidth:260,    
     listeners:{
         change:function(){
             if (fieldValueToFloat(Ext.getCmp('custo').getRawValue()) > 0 && Ext.getCmp('textQuantidade').value > 0) {
                Ext.getCmp('saveMovButton').enable();
             }else {
                Ext.getCmp('saveMovButton').disable();
             }
         }
     }
  }]
});

Ext.define('WinProduto',{
        extend:'Ext.Window',
        ID : 0,
        id:'winProduto',       
        modal:true,
        //width:460,
        //height:300,
        title:'Produto',
        layout:'fit',
        closeAction:'destroy',

        setID:function(id){
            this.ID = id;
        }
    });

Ext.define('ProdutoForm',{
         extend:'Ext.form.Panel',              
         bodyStyle: 'padding:15px;',
         width:560,
         border: false,
         autoScroll: true,
	 defaultType: 'textfield',
         defaults: {anchor: '-19'},
         buttons:[             
             {text:'Salvar',
                 icon:'images/icons/accept.png',
                 handler: function() {
                // The getForm() method returns the Ext.form.Basic instance:
                var form = this.up('form').getForm();
                var record = form.getRecord();
                 if (form.isValid()) { // Incluir verificação se o record é novo ou não
                // Updates the store 
                     form.updateRecord(record);                    
                     record.set('preco_normal',fieldValueToFloat(Ext.getCmp('preco_normal').getRawValue()));
                     record.set('preco_promo',fieldValueToFloat(Ext.getCmp('preco_promo').getRawValue()));
                     record.set('custo',fieldValueToFloat(Ext.getCmp('custo').getRawValue()));
                     var regexpObj = new RegExp(/ext/);
                     if (regexpObj.test(record.id)) {
                        Ext.data.StoreManager.lookup('produtoStore').add(record);
                     }
                     Ext.data.StoreManager.lookup('produtoStore').sync();
                     Ext.data.StoreManager.lookup('produtoStore').load();
                     Ext.getCmp('winProduto').close();
                    }
                 }
             }],

        items:[{
                xtype:'textfield'
                ,fieldLabel	: 'Descrição'
                ,name	: 'descricao'
                ,id	: 'descricao'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
                ,labelWidth:130
            },{
                xtype:'textfield'
                ,fieldLabel	: 'Referência'
                ,name	: 'referencia'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
                ,labelWidth:130
            },{
                xtype: 'field-money',
                fieldLabel	: 'Preço Normal:'
                ,name	: 'preco_normal'
                ,id	: 'preco_normal'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
                ,labelWidth:130
            },{
                xtype: 'field-money',
                fieldLabel	: 'Preço Promocional:'
                ,name	: 'preco_promo'
                ,id     : 'preco_promo'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
                ,labelWidth:130
                ,allowZero:true
            },{
                xtype:'textfield'
                ,fieldLabel	: 'Desconto Máximo %'
                ,name	: 'desconto_maximo'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
                ,labelWidth:130
            },{
                xtype: 'field-money',
                fieldLabel	: 'Custo:'
                ,name	: 'custo'
                ,id     : 'custo'
                ,value: 0
                ,minValue: 0
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
                ,labelWidth:130
            },{
                xtype:'textfield'
                ,fieldLabel	: 'Qtd. em estoque'
                ,name	: 'qtd_total'
                ,id	: 'qtd_total'
                ,readOnly:true
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
                ,labelWidth:130
            },{
                xtype:'combobox'
                ,fieldLabel	: 'Categoria Produto'
                ,name	: 'categoria_produto_id'
                ,id:'comboCatProduto'
                ,querymode:'remote'
                ,valueField:'id'
                ,displayField:'descricao'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
                ,labelWidth:130
            },{
                xtype:'combobox'
                ,fieldLabel	: 'Fornecedor'
                ,name	: 'fornecedor_id'
                ,id:'comboFornecedor'
                ,querymode:'remote'
                ,valueField:'id'
                ,displayField:'descricao'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
                ,labelWidth:130
            },{
                xtype:'combobox'
                ,fieldLabel	: 'Ativo'
                ,name	: 'ativo'
                ,id :'comboAtivo'
                ,querymode:'local'
                ,valueField:'valor'
                ,displayField:'texto'
                ,valueNotFoudText:'Sim'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
                ,labelWidth:130
            }]

        });
       





Ext.onReady(function(){

    

    Ext.util.Format.thousandSeparator = '.';
    Ext.util.Format.decimalSeparator = ',';
   
    var lojaStore = Ext.create('LojaStore');
    var storeProduto = Ext.create('ProdutoStore');
    var storeCatProduto = Ext.create('CatProdutoStore');
    var storeFornecedor = Ext.create('FornecedorStore');
    var ativoStore = Ext.create('AtivoStore');
    var entradaStore = Ext.create('EntradaStore');
       
    storeFornecedor.pageSize=2000;
    storeCatProduto.pageSize=2000;
    lojaStore.pageSize=2000;


     lojaStore.load();
     storeFornecedor.load();
     storeCatProduto.load();
     ativoStore.load();
     

   Ext.create ('Ext.grid.Panel',{
        id:'gridProduto',
        store:Ext.data.StoreManager.lookup('produtoStore'),
        border:false,
        renderTo:'main',       
        width: 840,
        height: 498,
        title: 'Lista de Produtos',
         columns: [
            {text: 'Código', flex: 1, dataIndex: 'id'},
            {text: 'Descrição', flex: 2.2, dataIndex: 'descricao'},
            {text: 'Preço Normal',flex: 1.2,dataIndex: 'preco_normal',renderer:function(v){return Ext.util.Format.currency(v, 'R$',2);}},
            {text: 'Preço Promo',flex: 1.1,dataIndex: 'preco_promo',renderer:function(v){return Ext.util.Format.currency(v, 'R$',2);}},
            {text: 'Desc. Máximo',flex:1,dataIndex: 'desconto_maximo'},
            {text: 'Custo',flex: 0.9,dataIndex: 'custo',renderer:function(v){return Ext.util.Format.currency(v, 'R$',2);}},
            {text: 'Qtd. Total', flex: 1, dataIndex: 'qtd_total'},
            //{text: 'Categoria',flex: 1,dataIndex: 'categoria_produto_id'},
            {text: 'Categoria',flex: 1,dataIndex: 'categoria_produto_descricao'},
            {text: 'Ativo',flex: 0.6,dataIndex: 'ativo'},
            //{text: 'Fornecedor',flex: 1,dataIndex: 'fornecedor_id'},
            {text: 'Fornecedor',flex: 1.1,dataIndex: 'fornecedor_descricao'},
            {text: 'Referência',flex: 1,dataIndex: 'referencia'},

        ],
        viewConfig: {
            listeners: {
                itemdblclick: function(dataview, record, item, index, e) {
                    var winProduto= Ext.create('WinProduto');
                    winProduto.title='Atualizar Produto';
                    var produtoForm = Ext.create('ProdutoForm');                   
                    Ext.getCmp('comboCatProduto').store= Ext.data.StoreManager.lookup('catProdutoStore');
                    Ext.getCmp('comboFornecedor').store= Ext.data.StoreManager.lookup('fornecedorStore');
                    Ext.getCmp('comboAtivo').store = Ext.data.StoreManager.lookup('ativoStore');
                    Ext.getCmp('comboCatProduto').setValue('record.categoria_produto_id');
                    Ext.getCmp('comboFornecedor').setValue('record.fornecedor_id');
                    Ext.getCmp('comboAtivo').setValue('record.ativo');
                    produtoForm.loadRecord(record);
                    winProduto.add(produtoForm);
                    winProduto.show();
                    applyFieldsFormat();
                },
                itemclick: function(dataview, record, item, index, e) {
                    Ext.getCmp('updateCadastro').enable();
                    Ext.getCmp('gerarEtiquetas').enable();
                    Ext.getCmp('novaEntrada').enable();
                }
            }
        },
        dockedItems:[
            {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            id:'pagingToolbarProduto',
            emptyMsg:'Nenhum registro',
            displayMsg:'Exibindo {0} - {1} de {2} reg. localizados',
            store:Ext.data.StoreManager.lookup('produtoStore'),
            displayInfo: true
            },{
            xtype:'toolbar',
            dock:'top',
            id:'toolbarProduto',
            items:[{
                 text : 'Novo Produto',
                 xtype:'button',
                 icon:'images/icons/user_add.png',
                 handler:function() {
                          var record = Ext.create('Produto');
                          var winProduto = Ext.create('WinProduto');
                          var produtoForm = Ext.create('ProdutoForm');
                          Ext.getCmp('comboCatProduto').store= Ext.data.StoreManager.lookup('catProdutoStore');                          
                          Ext.getCmp('comboFornecedor').store= Ext.data.StoreManager.lookup('fornecedorStore');
                          Ext.getCmp('comboAtivo').store = Ext.data.StoreManager.lookup('ativoStore');                         
                          produtoForm.loadRecord(record);
                          winProduto.add(produtoForm);
                          winProduto.show();
                          applyFieldsFormat();
                        }
                    },{
                 text : 'Atualizar Produto',
                 xtype:'button',
                 icon:'images/icons/application_form_edit.png',
                 id:'updateCadastro',
                 disabled:true,
                 handler:function() {
                     if (Ext.getCmp('gridProduto').getSelectionModel().hasSelection()) {
                            var record =Ext.getCmp('gridProduto').getSelectionModel().getSelection()[0];
                            var winProduto = Ext.create('WinProduto');
                            winProduto.title='Atualizar Produto';
                            var produtoForm = Ext.create('ProdutoForm');
                            Ext.getCmp('comboCatProduto').store= Ext.data.StoreManager.lookup('catProdutoStore');
                            Ext.getCmp('comboFornecedor').store= Ext.data.StoreManager.lookup('fornecedorStore');
                            Ext.getCmp('comboAtivo').store = Ext.data.StoreManager.lookup('ativoStore');
                            produtoForm.loadRecord(record);
                            winProduto.add(produtoForm);
                            winProduto.show();
                            applyFieldsFormat();

                        }
                    }
                },{
                 text : 'Gerar etiquetas',
                 xtype:'button',
                 disabled:true,
                 icon:'images/icons/printer.png',
                 id:'gerarEtiquetas',
                 handler:function() {
                           if (Ext.getCmp('gridProduto').getSelectionModel().hasSelection()) {
                            var record =Ext.getCmp('gridProduto').getSelectionModel().getSelection()[0];
                            window.open("php/database/LabelGenerator.php?cod=" + record.get('id') + "","_blank");
                           
                           }
                        }
                  },{
                 text : 'Nova Entrada',
                 xtype:'button',
                 id:'novaEntrada',
                 icon:'images/icons/user_add.png',
                 disabled:true,

                 handler:function() {
                          var record = Ext.create('Entrada');
                          var winEntrada = Ext.create('WinEntrada');
                          var formEntrada = Ext.create('FormEntrada');
                          Ext.getCmp('comboLoja').store = Ext.data.StoreManager.lookup('lojaStore');
                          recordProd =Ext.getCmp('gridProduto').getSelectionModel().getSelection()[0];
                          formEntrada.loadRecord(recordProd);
                          Ext.getCmp('custo').focus();                          
                          Ext.getCmp('textNotaEntrada').focus();
                          winEntrada.add(formEntrada);
                          winEntrada.show();
                        }
                    },{
                 text : 'Transferir Produto',
                 xtype:'button',
                 id:'buttonProdTransf',
                 icon:'images/icons/user_add.png',
                 disabled:false,
                 handler:function() {
                         if (Ext.getCmp('gridProduto').getSelectionModel().hasSelection()) {
                            var record =Ext.getCmp('gridProduto').getSelectionModel().getSelection()[0];
                            showTransferencia(record);
                            }                         
                        }
                    }
                ]
            },{
                xtype:'toolbar',
                id:'searchToolBar',
                renderTo:'main',
                border:'false',
                items:[{
                    xtype:'combobox',
                    style:{marginLeft:'25px'},
                    width:190,
                    labelWidth:80,
                    id:'propertyCombo',
                    store:Ext.data.StoreManager.lookup('storeMovCarteira'),
                    fieldLabel :'Pesquisar por:',
                    align:"right",
                    valueField:'valor',
                    displayField:'texto',
                    listeners:{
                        select:function(combo) {
                                if (combo.getValue()=='id') {
                                Ext.getCmp('valueField').searchLenght = 5
                                }
                                if (combo.getValue()=='descricao') {
                                 Ext.getCmp('valueField').searchLenght = 2
                                }
                            }
                        }
                    },{
                    xtype:"textfield",
                    fieldLabel:"Busca:",
                    style:{marginLeft:'45px'},
                    width:240,
                    labelWidth:45,
                    id:'valueField',
                    searchLenght:5,                    
                    enableKeyEvents:true,

                    listeners: {     // Fires the filter on 3th character
                        keyup: function(campo) {
                            if (campo.getValue().length > campo.searchLenght) {
                                var store = Ext.getCmp('gridProduto').store;
                                store.clearFilter(true);
                                store.filter(Ext.getCmp('propertyCombo').getValue(),campo.getValue());
                                }
                            },
                            specialkey: function(f,e){
                                if(e.getKey()==e.ENTER){                                    
                                    var store = Ext.getCmp('gridProduto').store;
                                    store.clearFilter(true);
                                    store.filter(Ext.getCmp('propertyCombo').getValue(),Ext.getCmp('valueField').getValue());
                                }
                            }
                        }
                    },{
                     text : 'Buscar',
                     xtype:'button',
                     style:{marginLeft:'20px'},
                     icon:'images/icons/find.png',
                     handler:function() {
                            var store = Ext.getCmp('gridProduto').store;
                            store.clearFilter(true);
                            store.filter(Ext.getCmp('propertyCombo').getValue(),Ext.getCmp('valueField').getValue());

                        }
                    },{
                     text : 'Listar',
                     xtype:'button',
                     style:{marginLeft:'85px'},
                     icon:'images/icons/script_lightning.png',
                     handler:function() {
                        var store = Ext.getCmp('gridProduto').store
                        store.clearFilter(true);
                        store.pageSize=17;
                        store.load();
                        Ext.getCmp('updateCadastro').disable();
                        Ext.getCmp('gerarEtiquetas').disable();
                        Ext.getCmp('novaEntrada').disable();

                        }
                    }
                ]}
        ]       
        
        });
    var storeCamposBusca = Ext.create('StoreCamposBusca');
    storeCamposBusca.load();
    var comboBusca = Ext.getCmp('propertyCombo');
    comboBusca.store = storeCamposBusca;
    comboBusca.setValue('id');

    });











