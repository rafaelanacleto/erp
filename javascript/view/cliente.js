
Ext.define('RecebeEmailStore',{
    extend:'Ext.data.Store',
    fields:['valor','texto'],
    storeId:'recebeEmailStore',
    data:[
        {"valor":"Sim","texto":"Sim"},
        {"valor":"Não","texto":"Não"}
        ]
    });



Ext.define('WinCliente',{
        extend:'Ext.Window',
        ID : 0,
        id:'winCliente',
        modal:true,
        //width:400,
        //height:200,
        title:'Novo Cliente',
        layout:'fit',
        closeAction:'destroy',

        setID:function(id){
            this.ID = id;
        }
    });


Ext.define('WinDebitosCliente',{
        extend:'Ext.Window',
        ID : 0,
        id:'winDebitosCliente',
        modal:true,
        //width:400,
        //height:200,
        title:'Débitos do Cliente',
        layout:'fit',
        closeAction:'destroy',

        setID:function(id){
            this.ID = id;
        }
    });

    Ext.define('WinComprasCliente',{
        extend:'Ext.Window',
        ID : 0,
        id:'winComprasCliente',
        modal:true,
        //width:400,
        //height:200,
        title:'Histórico de compras do Cliente',
        layout:'fit',
        closeAction:'destroy',

        setID:function(id){
            this.ID = id;
        }
    });


    Ext.define('ClienteForm',{
         extend:'Ext.form.Panel',
         bodyStyle: 'padding:15px;',
         border: false,
         width:460,
         height:240,
         autoScroll: true,
	 defaultType: 'textfield',
         defaults: {anchor: '-19'},
         buttons:[
             {text:'Salvar',
                 icon:'images/icons/accept.png',
                 handler: function() {
                // The getForm() method returns the Ext.form.Basic instance:
                var form = this.up('form').getForm();
                var record = form.getRecord();
                 if (form.isValid()) { // Incluir verificação se o record é novo ou não
                // Updates the store
                     form.updateRecord(record);
                     // Verifica se o registro é novo

                     var regexpObj = new RegExp(/ext/);
                     if (regexpObj.test(record.id)) {
                        Ext.data.StoreManager.lookup('clienteStore').add(record);
                     }
                     Ext.data.StoreManager.lookup('clienteStore').sync();
                     Ext.data.StoreManager.lookup('clienteStore').load();
                     Ext.getCmp('winCliente').close();
                    }
                 }

         }],
        items:[{
                xtype:'textfield'
                ,fieldLabel	: 'Nome:'
                ,name	: 'nome'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
            },{
                xtype:'datepicker'
                ,fieldLabel	: 'Nascimento:'
                ,name	: 'nascimento'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
                ,minDate: new Date()
                ,hidden:true
                ,handler: function(picker, date) {
                        // do something with the selected date
                }
            },{
                xtype:'textfield'
                ,fieldLabel	: 'Telefone'
                ,name	: 'fone'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
            },{
                xtype:'textfield'
                ,fieldLabel	: 'E-mail:'
                ,name	: 'email'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
            },{
                xtype:'textfield'
                ,fieldLabel	: 'Facebook:'
                ,name	: 'facebook'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
            },{
                xtype:'datefield'
                ,fieldLabel	: 'Nascimento:'
                ,name	: 'nascimento',
                anchor: '100%',
                submitFormat:'Y-m-d',
                submitValue:true,
                maxValue: new Date(),
                width:100
                ,allowBlank	: false
                ,maxLength	: 100
            },{
                xtype:'combobox'
                ,fieldLabel	: 'Recebe E-mail:'
                ,name	: 'recebe_email'
                ,id :'comboRecebe_email'
                ,querymode:'local'
                ,valueField:'valor'
                ,displayField:'texto'
                ,valueNotFoudText:'Não recebe'
                ,width:220
                ,allowBlank	: false
                ,maxLength	:200
            }]

        });


Ext.onReady(function(){

    var dateMenu = Ext.create('Ext.menu.DatePicker', {
        handler: function (dp, date) {
            Ext.Msg.alert('Date Selected', 'You selected ' + Ext.Date.format(date, 'M j, Y'));
        }
    });

    //Store Creation
    var storeCliente = Ext.create('clienteStore');
    //Adjust number of rows
    storeCliente.pageSize=17;
     storeCliente.load();
     
     var storeRecebeEmail = Ext.create('RecebeEmailStore');
     storeRecebeEmail.load();


    Ext.create('Ext.grid.Panel',{
        id:'gridCliente',
        store:Ext.data.StoreManager.lookup('clienteStore'),
        renderTo:'main',
        border:false,
        title:'Clientes',
        height:498,
        columns:[
            {header:'Nome',dataIndex:'nome',flex:4},
            {header:'Data Nasc.',dataIndex:'nascimento', renderer: Ext.util.Format.dateRenderer('d/m/Y') },
            {header:'Telefone',dataIndex:'fone',flex:2},
            {header:'E-mail',dataIndex:'email',flex:2},
            {header:'facebook',dataIndex:'facebook'},
            {header:'Recebe E-mail',dataIndex:'recebe_email'}
        ],
        viewConfig:{
            listeners:{
                itemclick: function(dataview, record, item, index, e) {
                    Ext.getCmp('updateCadastro').enable();
                    //Ext.getCmp('histCompras').enable();
                    //Ext.getCmp('verDebitos').enable();
                    Ext.getCmp('vendaCliente').enable();
                },

                itemdblclick: function(dataview, record, item, index, e) {
                    var winCliente = Ext.create('WinCliente');
                    winCliente.title='Atualizar Cadastro';
                    var clienteForm = Ext.create('ClienteForm');
                    Ext.getCmp('comboRecebe_email').store= Ext.data.StoreManager.lookup('recebeEmailStore');
                    clienteForm.loadRecord(record);
                    winCliente.add(clienteForm);
                    winCliente.show();
                }
            }
        },
        dockedItems:[
            {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            id:'pagingToolbarCliente',
            emptyMsg:'Nenhum registro',
            displayMsg:'Exibindo {0} - {1} de {2} reg. localizados',
            store:Ext.data.StoreManager.lookup('clienteStore'),
            displayInfo: true
            },{
            xtype:'toolbar',
            dock:'top',
            id:'toolbarCliente',
            items:[
                {
                 text : 'Novo Cliente',
                 xtype:'button',
                 icon:'images/icons/user_add.png',
                 handler:function() {
                      var record = Ext.create('Cliente');                      
                      var winCliente = Ext.create('WinCliente');
                      var clienteForm = Ext.create('ClienteForm');
                      Ext.getCmp('comboRecebe_email').store= Ext.data.StoreManager.lookup('recebeEmailStore');
                      clienteForm.loadRecord(record);
                      winCliente.add(clienteForm);
                      winCliente.show();
                    }
                },{
                 text : 'Realizar Venda',
                 xtype:'button',
                 icon:'images/icons/basket.png',
                 id:'vendaCliente',
                 disabled:true,
                 handler:function() {
                        if (Ext.getCmp('gridCliente').getSelectionModel().hasSelection()) {
                            var record =Ext.getCmp('gridCliente').getSelectionModel().getSelection()[0];
                            var clienteId = record.get('id');
                            var clienteNome = record.get('nome');
                            var teste = "index.php?action=Venda&cliente="+clienteId;
                            teste += "&nome="+clienteNome;
                            window.location = teste;
                        }
                 }
                   
                },{
                 text : 'Ver débitos',
                 xtype:'button',
                 icon:'images/icons/report_magnify.png',
                 id:'verDebitos',
                 disabled:true,
                 handler:function() {
                      var record = Ext.create('Cliente');
                      var winCliente = Ext.create('WinDebitosCliente');
                      var clienteForm = Ext.create('ClienteForm');
                      Ext.getCmp('comboRecebe_email').store= Ext.data.StoreManager.lookup('recebeEmailStore');
                      clienteForm.loadRecord(record);
                      winCliente.add(clienteForm);
                      winCliente.show();
                    }
                },{
                 text : 'Histórico de compras',
                 xtype:'button',
                 icon:'images/icons/hourglass.png',
                 disabled:true,
                 id:'histCompras',
                 handler:function() {
                      var record = Ext.create('Cliente');
                      var winCliente = Ext.create('WinComprasCliente');
                      var clienteForm = Ext.create('ClienteForm');
                      Ext.getCmp('comboRecebe_email').store= Ext.data.StoreManager.lookup('recebeEmailStore');
                      clienteForm.loadRecord(record);
                      winCliente.add(clienteForm);
                      winCliente.show();
                    }
                },{
                 text : 'Atualizar Cadastro',
                 xtype:'button',
                 icon:'images/icons/application_form_edit.png',
                 id:'updateCadastro',
                 disabled:true,
                 handler:function() {
                     if (Ext.getCmp('gridCliente').getSelectionModel().hasSelection()) {
                            var record =Ext.getCmp('gridCliente').getSelectionModel().getSelection()[0];
                            var winCliente = Ext.create('WinCliente');
                            winCliente.title='Atualizar Cadastro';
                            var clienteForm = Ext.create('ClienteForm');
                            Ext.getCmp('comboRecebe_email').store= Ext.data.StoreManager.lookup('recebeEmailStore');
                            clienteForm.loadRecord(record);
                            winCliente.add(clienteForm);
                            winCliente.show();
                        }
                    }
                }
               ]
            },{
            xtype:'toolbar',
            dock:'top',
            id:'toolbarSearchCliente',
            items:[
                {
                style:{marginLeft:'25px',marginRight:'125px'},
                xtype:"textfield",
                flex:4,
                labelWidth:75,
                fieldLabel:"Buscar por",
                name:"textBusca",
                id:"textBusca",
                enableKeyEvents:true,
                listeners: {     // Fires the filter on 3th character
                       keyup: function(campo) {
                           if (campo.getValue().length > 2) {

                                var store = Ext.getCmp('gridCliente').store;
                                store.clearFilter(true);
                                store.filter('nome',campo.getValue());

                           }

                       }

                    }
                },{
                 text : 'Listar',
                 xtype:'button',
                 style:{marginLeft:'25px',marginRight:'80px'},
                 icon:'images/icons/script_lightning.png',
                 handler:function() {
                    var store = Ext.getCmp('gridCliente').store
                    store.clearFilter(true);
                    store.pageSize=17;
                    store.load();
                    Ext.getCmp('updateCadastro').disable();
                    Ext.getCmp('histCompras').disable();
                    Ext.getCmp('verDebitos').disable();
                    Ext.getCmp('vendaCliente').disable();
                    }
                }
                ]
            }
        ]
    });

    

});

