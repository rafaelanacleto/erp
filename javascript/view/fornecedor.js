


Ext.define('AtivoStore',{
    extend:'Ext.data.Store',
    fields:['valor','texto'],
    storeId:'ativoStore',
    data:[
        {"valor":"Sim","texto":"Sim"},
        {"valor":"Não","texto":"Não"}
        ]
    });



Ext.define('WinFornecedor',{
        extend:'Ext.Window',
        ID : 0,
        id:'winFornecedor',
        modal:true,
        //width:400,
        //height:200,
        title:'Novo Fornecedor',
        layout:'fit',
        closeAction:'destroy',

        setID:function(id){
            this.ID = id;
        }
    });


    Ext.define('FornecedorForm',{
         extend:'Ext.form.Panel',
         bodyStyle: 'padding:15px;',
         border: false,
         width:460,
         height:240,
         autoScroll: true,
	 defaultType: 'textfield',
         defaults: {anchor: '-19'},
         buttons:[
             {text:'Salvar',
                 icon:'images/icons/accept.png',
                 handler: function() {
                // The getForm() method returns the Ext.form.Basic instance:
                var form = this.up('form').getForm();
                var record = form.getRecord();
                 if (form.isValid()) { // Incluir verificação se o record é novo ou não
                // Updates the store
                     form.updateRecord(record);
                     // Verifica se o registro é novo

                     var regexpObj = new RegExp(/ext/);
                     if (regexpObj.test(record.id)) {
                        Ext.data.StoreManager.lookup('fornecedorStore').add(record);
                     }
                     Ext.data.StoreManager.lookup('fornecedorStore').sync();
                     Ext.data.StoreManager.lookup('fornecedorStore').load();
                     Ext.getCmp('winFornecedor').close();
                    }
                 }

         }],
        items:[{
                xtype:'textfield'
                ,fieldLabel	: 'Descricao:'
                ,name	: 'descricao'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
            },{
                xtype:'textfield'
                ,fieldLabel	: 'Telefone'
                ,name	: 'fone'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
            },{
                xtype:'textfield'
                ,fieldLabel	: 'E-mail:'
                ,name	: 'email'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
            },{
                xtype:'textfield'
                ,fieldLabel	: 'Localização:'
                ,name	: 'localizacao'
                ,width:220
                ,allowBlank	: false
                ,maxLength	: 100
            },{
                xtype:'combobox'
                ,fieldLabel	: 'Ativo:'
                ,name	: 'ativo'
                ,id :'comboAtivo'
                ,querymode:'local'
                ,valueField:'valor'
                ,displayField:'texto'
                ,valueNotFoudText:'Sim'
                ,width:220
                ,allowBlank	: false
                ,maxLength	:200
            }]

        });






Ext.onReady(function(){
    var ativoStore = Ext.create('AtivoStore');
   
    //Store Creation
    var storeFornecedor = Ext.create('FornecedorStore');
    //Adjust number of rows
    storeFornecedor.pageSize=19;
     //storeCliente.load();

    Ext.create('Ext.grid.Panel',{
        id:'gridFornecedor',
        store:Ext.data.StoreManager.lookup('fornecedorStore'),
        renderTo:'main',
        border:false,
        title:'Fornecedores',
        height:498,
        columns:[
            {header:'Descrição',dataIndex:'descricao',flex:3},
            {header:'Telefone',dataIndex:'fone',flex:2},
            {header:'E-mail',dataIndex:'email',flex:2},
            {header:'localizacao',dataIndex:'localizacao',flex:2},
            {header:'Ativo',dataIndex:'ativo'}
        ],
        viewConfig:{
            listeners:{
                itemdblclick: function(dataview, record, item, index, e) {
                    var winFornecedor = Ext.create('WinFornecedor');
                    winFornecedor.title='Atualizar Cadastro';
                    var fornecedorForm = Ext.create('FornecedorForm');
                    Ext.getCmp('comboAtivo').store= Ext.data.StoreManager.lookup('ativoStore');
                    fornecedorForm.loadRecord(record);
                     winFornecedor.add(fornecedorForm);
                     winFornecedor.show();
                }
            }
        },
        dockedItems:[
            {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            id:'pagingToolbarFornecedor',
            emptyMsg:'Nenhum registro',
            displayMsg:'Exibindo {0} - {1} de {2} reg. localizados',
            store:Ext.data.StoreManager.lookup('fornecedorStore'),
            displayInfo: true
            },{
            xtype:'toolbar',
            dock:'top',
            id:'toolbarfornecedor',
            items:[
                {
                 text : 'Novo Fornecedor',
                 xtype:'button',
                 icon:'images/icons/user_add.png',
                 handler:function() {
                      var record = Ext.create('Fornecedor');
                      var winFornecedor = Ext.create('WinFornecedor');
                      var fornecedorForm = Ext.create('FornecedorForm');
                       Ext.getCmp('comboAtivo').store= Ext.data.StoreManager.lookup('ativoStore');
                      fornecedorForm.loadRecord(record);
                      winFornecedor.add(fornecedorForm);
                      winFornecedor.show();
                    }
                },{
                 text : 'Histórico de fornecimento',
                 xtype:'button',
                 icon:'images/icons/hourglass.png',
                 handler:function() {
                      var record = Ext.create('Fornecedor');
                      var winFornecedor = Ext.create('WinFornecedor');
                      var fornecedorForm = Ext.create('FornecedorForm');
                      fornecedorForm.loadRecord(record);
                      winFornecedor.add(fornecedorForm);
                      winFornecedor.show();
                    }
                },{
                 text : 'Atualizar Cadastro',
                 xtype:'button',
                 icon:'images/icons/application_form_edit.png',
                 handler:function() {
                     if (Ext.getCmp('gridFornecedor').getSelectionModel().hasSelection()) {
                            var record =Ext.getCmp('gridFornecedor').getSelectionModel().getSelection()[0];
                            var winFornecedor = Ext.create('WinFornecedor');
                            winFornecedor.title='Atualizar Cadastro';
                            var fornecedorForm = Ext.create('FornecedorForm');
                             Ext.getCmp('comboAtivo').store= Ext.data.StoreManager.lookup('ativoStore');
                            fornecedorForm.loadRecord(record);
                            winFornecedor.add(fornecedorForm);
                            winFornecedor.show();
                        }
                    }
                }
               ]
            },{
            xtype:'toolbar',
            dock:'top',
            id:'toolbarSearchFornecedor',
            items:[
                {
                style:{marginLeft:'25px',marginRight:'25px'},
                xtype:"textfield",
                flex:4,
                labelWidth:75,
                fieldLabel:"Buscar por",
                name:"textBusca",
                id:"textBusca",
                enableKeyEvents:true,
                listeners: {     // Fires the filter on 3th character
                       keyup: function(campo) {
                           if (campo.getValue().length > 2) {

                                var store = Ext.getCmp('gridFornecedor').store;
                                store.clearFilter(true);
                                store.filter('descricao',campo.getValue());

                           }

                       }

                    }
                },{
                 text : 'Listar',
                 xtype:'button',
                 style:{marginLeft:'25px',marginRight:'80px'},
                 icon:'images/icons/script_lightning.png',
                 handler:function() {
                    var store = Ext.getCmp('gridFornecedor').store
                    store.clearFilter(true);
                    store.pageSize=17;
                    store.load();
                    }
                }

                ]
            }
        ]
    });



});

