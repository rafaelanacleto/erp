function showTransferencia(record) {


    //Configure proxy string
    strUrl = 'php/ajax/estoque.php?action=getProductPosition&prod_id='+record.get('id');
    var storeEstoque = Ext.create('EstoqueStore');
    storeEstoque.load({url:strUrl});
    
    var winTransfer = 
    Ext.create('Ext.Window',{
        extend:'',
        ID : 0,
        id:'winTransfer',
        modal:true,
        //width:400,
        //height:200,
        title:'Transferência de mercadoria',
        layout:'fit',
        closeAction:'destroy',

        setID:function(id){
            this.ID = id;
        }
    });
    var formTransfer =
    Ext.create('Ext.form.Panel',{
        id:'formTransfer',
        bodyStyle:"padding:15px",
        buttons:[{
        text : 'Transferir',
        xtype:'button',
        scope:this,
        id:'transferButton',
        icon:'images/icons/tick.png',
        disabled:true,
        listeners:{
            validitychange:function( form, valid, eOpts ) {
                    console.log('ta validado!');
                }
        },
        handler:function() {
            var form = Ext.getCmp('formFinalizaVenda');
            form.submit({
                    url:'php/ajax/venda.php?action=insert',
                    params: {'venda':Ext.JSON.encode(resumoVenda)},
                    success: function (form, action) {
                        Ext.Msg.show({
                               title:'Sucesso na operação',
                               msg: 'Operação Efetuada com sucesso.',
                               buttons: Ext.Msg.OK,
                               fn: function(btn){
                                    if (btn == 'ok'){
                                        window.location = "index.php?action=Clientes"
                                    }
                                },
                               animEl: 'elId'
                            });
                    },
                    failure: function (responseform, action) {
                        Ext.Msg.alert('Falha na operação','Não foi possível efetuar a operação.');
                    }
                });
            }
        }],
        items:[{
            xtype:"textfield",
            fieldLabel:"Produto",
            name:"descricao",
            id:"descricao",
            width:290,
            value:record.get('descricao'),
            allowBlank:false
          },{
            xtype:"combo",
            store:Ext.data.StoreManager.lookup('lojaStore'),
            fieldLabel:"Loja Origem",
            name:"comboLojaOrigem",
            id:"comboLojaOrigem",
            hiddenName:"combovalue",
            querymode:'remote',
            valueField:'id',
            displayField:'descricao',
            allowBlank:false,
            width:290,           
            listeners:{
                select:function(combo) {
                    Ext.getCmp('qtd_transf').setValue(0);
                    result = storeEstoque.query('loja_id',combo.getValue());
                    console.log(result.getAt(0).data.quantidade);
                    Ext.getCmp('qtd_disp').setValue(result.getAt(0).data.quantidade);
                    Ext.getCmp('qtd_transf').setMaxValue(result.getAt(0).data.quantidade);
                }
            }
          
          },{
            xtype:"combo",
            store:Ext.data.StoreManager.lookup('lojaStore'),
            fieldLabel:"Loja Destino",
            name:"comboLojaDestino",
            id:"comboLojaDestino",
            hiddenName:"combovalue",
            querymode:'remote',
            valueField:'id',
            displayField:'descricao',
            width:290,
            allowBlank:false,
            listeners:{
                select:function(combo) {
                    if (combo.getValue() == Ext.getCmp('comboLojaOrigem').getValue()) {
                        combo.setValue('');
                    }
                    
                }
            }
          },{
            xtype:"numberfield",           
            fieldLabel:"Qtd disponível",
            name:"qtd_disp",
            id:"qtd_disp",
            increment:5,
            value:"0",
            readOnly:true,
            width:290,
            allowBlank:false
          },{
            xtype:"numberfield",
            name:'qtd_transf',
            id:'qtd_transf',
            fieldLabel:"Qtd transferida",
            name:"numbervalue",
            width:290,
            allowBlank:false,
            listeners:{
                change:function(campo, newValue, oldValue, eOpts) {
                    if (newValue > campo.maxValue) {
                        campo.setValue(campo.maxValue);
                    }
                }
            }
          }]
});


    winTransfer.add(formTransfer);
    winTransfer.show();





}




