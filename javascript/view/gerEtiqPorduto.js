



Ext.onReady(function(){
    Ext.create('Ext.data.Store',{
    fields:['valor','texto'],
    storeId:'TipoBuscaStore',
    data:[
        {"valor":"id","texto":"Código"},
        {"valor":"descricao_produto","texto":"Descrição"},
        {"valor":"referencia","texto":"Referência"}
    ]
});

Ext.define('WinGerEtiqProduto',{
        extend:'Ext.Window',
        ID : 0,
        id:'winGerEtiqProduto',
        modal:true,
        width:460,
        height:180,
        title:'Gerar etiquetas por produto',
        layout:'fit',
        closeAction:'destroy',

        setID:function(id){
            this.ID = id;
        }
    });

Ext.define('FormGerEtiqProduto',{
         extend:'Ext.form.Panel',
         bodyStyle: 'padding:15px;',
         border: false,
         autoScroll: true,
         defaultType: 'textfield',
         defaults: {anchor: '-19'},
         buttons:[
             {text:'Cancelar',
                 id:'excluirProduto',
                 icon:'images/icons/cancel.png',
              handler: function() {
                 this.up('window').close();
              }},
             {text:'Gerar',
                 icon:'images/icons/script_go.png',
                 handler: function() {
                     window.open('php/database/gerEtiquetas.php');
                 }
             }],
          items:[{
                xtype:'textfield'
                ,fieldLabel	: 'Descrição'
                ,name	: 'descricao_produto'
                ,width:220
                ,allowBlank	: false
                ,readOnly:true
                ,maxLength	: 100
            },{
                xtype:'textfield'
                ,fieldLabel	: 'Código'
                ,name	: 'id'
                ,width:60
                ,allowBlank	: false
                ,readOnly:true
                ,maxLength	: 100
            },{
                xtype:'numberfield'
                ,fieldLabel	: 'Nº de etiquetas'
                ,name	: 'quantLabels'
                ,width:60
                ,allowBlank	: false
                ,value:1
                ,maxLength	: 100
            }]
});

    var storeProdutoBusca = Ext.create('ProdutoBuscaStore');
    storeProdutoBusca.pageSize=19;   
        
Ext.create('Ext.grid.Panel',{
    store:Ext.data.StoreManager.lookup('ProdutoBuscaStore'),
    id:'resBuscaGrid',
    renderTo:'main',
    border:false,
    title:'Pesquisa de Produtos',
    height:498,
    columns: [
        {header: 'Código',  dataIndex: 'id'},
        {header: 'Referência',  dataIndex: 'referencia'},
        {header: 'Descrição', dataIndex: 'descricao_produto', flex: 1},
        {header: 'Preço', dataIndex: 'preco', renderer:Ext.util.Format.usMoney},
        {header: 'Quantidade', dataIndex: 'quantidade'},
        {header: 'Fornecedor', dataIndex: 'descricao_fornecedor'}
    ],
    viewConfig: {
        listeners: {
            itemdblclick: function(dataview, record, item, index, e) {
                var winEtiqProduto= Ext.create('WinGerEtiqProduto');
                var produtoForm = Ext.create('FormGerEtiqProduto');
                produtoForm.loadRecord(record);
                winEtiqProduto.add(produtoForm);
                winEtiqProduto.show();
            }
        }
    },
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            id:'pagingToolbarBusca',
            emptyMsg:'Nenhum registro',
            displayMsg:'Exibindo {0} - {1} de {2} reg. localizados',
            store:Ext.data.StoreManager.lookup('ProdutoBuscaStore'),
            displayInfo: true
            },{
            xtype: 'toolbar',
            dock: 'top',
            items:[{
                xtype:"combobox",
                flex:2,
                labelWidth:60,
                store:Ext.data.StoreManager.lookup('TipoBuscaStore'),
                fieldLabel:"Critério",
                name:"comboCriterio",
                id:'comboCriterio',
                querymode:"local",
                valueField:"valor",
                displayField:"texto",
                value:'descricao_produto',
                originalValue:"descricao_produto"
            },{
                style:{marginLeft:'25px'},
                xtype:"textfield",
                flex:4,
                labelWidth:75,
                fieldLabel:"Buscar por",
                name:"textBusca",
                id:"textBusca"
            },{
                style:{marginLeft:'25px'},
                xtype:"button",
                text:'Pesquisar',
                id:'buttonPesquisar',
                icon:'images/icons/find.png',
                handler: function() {
                    var store = Ext.getCmp('resBuscaGrid').store;
                    store.clearFilter();
                    var propertyName = Ext.getCmp('comboCriterio').getValue();
                    var filtervalue = Ext.getCmp('textBusca').getValue();
                    Ext.getCmp('pagingToolbarBusca').moveFirst();
                    store.filter(propertyName,filtervalue);
                }
            }]
        }]
    });

});


