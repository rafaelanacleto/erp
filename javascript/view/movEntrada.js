


Ext.define('MovEntradaForm',{
 extend:'Ext.form.Panel',
 renderTo:'main',
 id:'movEntradaForm',
title:"Movimento - Entrada",
bodyStyle:"padding:15px",
height:498,
width:840,
items:[{
    xtype:"combobox",
    fieldLabel:"Fornecedor",
    name:"id_fornecedor",
    id:'comboFornecedor',
    querymode:'remote',
    valueField:'id',
    displayField:'descricao_fornecedor',
    width:360,
    minWidth:360,
    maxWidth:360,
    listeners:{
        change:function() {    // Ativa combo do produto e atualiza lista para o fornecedor
           Ext.getCmp('comboProduto').reset();
           Ext.getCmp('textReferencia').setValue('');
           Ext.getCmp('textReferencia').disable();
           Ext.getCmp('textNotaEntrada').setValue('');
           Ext.getCmp('textNotaEntrada').disable();
           Ext.getCmp('textQuantidade').setValue('');
           Ext.getCmp('textQuantidade').disable();
           Ext.getCmp('textValor').setValue('');
           Ext.getCmp('textValor').disable();           
           Ext.data.StoreManager.lookup('ProdutoStore').clearFilter();
           Ext.data.StoreManager.lookup('ProdutoStore').filter('id_fornecedor',Ext.getCmp('comboFornecedor').getValue());
           Ext.getCmp('comboProduto').enable();           
        }
    }
  },{
    xtype:"combobox",
    fieldLabel:"Produto",
    id:'comboProduto',
    name:"id_produto",
    valueField:'id',
    displayField:'descricao_produto',
    width:360,
    minWidth:360,
    maxWidth:360,
    disabled:true,
    listeners:{
        select:function()  {
        var id_produto = Ext.getCmp('comboProduto').getValue();           
        var referencia=Ext.getCmp('comboProduto').store.findRecord('id', id_produto).get('referencia');
        Ext.getCmp('textReferencia').setValue(referencia);
        Ext.getCmp('textReferencia').enable();
        //Ext.getCmp('textNotaEntrada').setValue(''); Desabilitado, quando troca produto não troca nota
        Ext.getCmp('textNotaEntrada').enable();
        Ext.getCmp('textQuantidade').setValue('');
        Ext.getCmp('textQuantidade').enable();
        Ext.getCmp('textValor').setValue('');
        Ext.getCmp('textValor').enable();
        }
    }

  },{
    xtype:"textfield",
    fieldLabel:"Referência",
    name:"referenciaProduto",
    id:'textReferencia',
    width:260,
    minWidth:260,
    maxWidth:260,
    disabled:true,
    readOnly:true
  },{
    xtype:"textfield",
    fieldLabel:"Nota de Entrada",
    name:"numNota",
    id:'textNotaEntrada',
    width:260,
    minWidth:260,
    maxWidth:260,
    maxLength:128,
    disabled:true
  },{
    xtype:"numberfield",
    fieldLabel:"Quantidade",
    name:"quantidade",
    id:'textQuantidade',
    value:'0',
    width:260,
    minWidth:260,
    maxWidth:260,
    disabled:true,
     listeners:{
         change:function(){             
             if (Ext.getCmp('textQuantidade').value > 0 && Ext.getCmp('textValor').value > 0) {
                Ext.getCmp('novoItemButton').enable();
             }else {
                Ext.getCmp('novoItemButton').disable(); 
             }
         }
     }
  },{
    xtype:"numberfield",
    fieldLabel:"Valor Item R$",
    name:"valor",
    id:'textValor',
    value:'0',
    width:260,
    maxWidth:260,
    minWidth:260,
    disabled:true,
     listeners:{
         change:function(){             
             if (Ext.getCmp('textValor').value > 0 && Ext.getCmp('textQuantidade').value > 0) {
                Ext.getCmp('novoItemButton').enable();
             }else {
                Ext.getCmp('novoItemButton').disable(); 
             }
         }
     }
  },{
    tbar: [{
            text : 'Novo Item',
            xtype:'button',
            scope:this,
            id:'novoItemButton',
            icon:'images/icons/add.png',
            disabled:true,
            handler:function() { //Cria record do grid completa e acrescenta  
                var record = Ext.create('MovEntradaModel');
                record.set('idFornecedor',Ext.getCmp('comboFornecedor').getValue());               
                record.set('idProduto',Ext.getCmp('comboProduto').getValue());                
                record.set('descricaoFornecedor',Ext.getCmp('comboFornecedor').getRawValue()); 
                record.set('descricaoProduto',Ext.getCmp('comboProduto').getRawValue()); 
                record.set('numNota',Ext.getCmp('textNotaEntrada').getValue()); 
                record.set('quantidade',Ext.getCmp('textQuantidade').getValue()); 
                record.set('referenciaProduto',Ext.getCmp('textReferencia').getValue()); 
                record.set('valor',Ext.getCmp('textValor').getValue()); 
                
                //Ext.data.StoreManager.lookup('MovEntradaStore').add(record); 
                var gridStore = Ext.getCmp('resMovGrid').getStore();
                gridStore.add(record);
                
                //Item incluído, zera os campos 
                Ext.getCmp('textReferencia').setValue('');
                Ext.getCmp('textReferencia').disable();
                Ext.getCmp('textQuantidade').setValue('');
                Ext.getCmp('textQuantidade').disable();
                Ext.getCmp('textValor').setValue('');
                Ext.getCmp('textValor').disable(); 
                Ext.getCmp('comboProduto').reset();
                
                //Remove marcação de remoção de registro
                Ext.getCmp('resMovGrid').getSelectionModel().deselectAll();
                Ext.getCmp('removeItemButton').disable();
            }
       },{ text : 'Remover Item',
            xtype:'button',
            scope:this,
            icon:'images/icons/delete.png',
            disabled:true,
            id:'removeItemButton',
            handler:function() {
                var store = Ext.getCmp('resMovGrid').getStore();
                var selModel = Ext.getCmp('resMovGrid').getSelectionModel();
                store.remove(selModel.getSelection());
                Ext.getCmp('removeItemButton').disable();
            }
       },{ text : 'Gravar Movimento',
            xtype:'button',
            scope:this,
            icon:'images/icons/disk.png',
            disabled:true,
            id:'saveMovButton',
            handler:function() {
                Ext.getCmp('removeItemButton').disable();
                Ext.getCmp('textNotaEntrada').setValue('');
                Ext.getCmp('comboFornecedor').reset();
                Ext.getCmp('comboProduto').reset();
                var store = Ext.getCmp('resMovGrid').getStore();
                store.sync();
                Ext.Msg.alert('Gravar movimento', 'Movimento de entrada registrado com sucesso.');
                Ext.data.StoreManager.lookup('movEntradaStore').removeAll();

            }
       }]
      
  }]
});





Ext.onReady(function(){


    //cria e configura store para o combo de fornecedores
    var fornecedorStore = Ext.create('FornecedorStore');
    fornecedorStore.pageSize=9999;
    
    
     //cria e configura store para o combo de produtos
     var produtoStore = Ext.create('ProdutoStore');
     produtoStore.pageSize=9999;

    //cria e configura store para grid de itens do movimento
    var resMovStore = Ext.create('MovEntradaStore');
    
    //Adiciona listener para quantidade de registros no grid
    Ext.data.StoreManager.lookup('movEntradaStore').addListener('datachanged', function(){
        //Controle botão salvar movimento
            var qtd = Ext.data.StoreManager.lookup('movEntradaStore').getNewRecords().length;
            if (qtd > 0) {
                Ext.getCmp('saveMovButton').enable();
            }else {
                Ext.getCmp('saveMovButton').disable();               
            } }, this );

    //Instancia o painel
    var movEntradaPanel = Ext.create('MovEntradaForm');

     //Instancia o grid
    var resMovGrid=Ext.create('Ext.grid.Panel',{
        width: 807,
        height: 250,
        title: 'Itens deste movimento',
        id:'resMovGrid',
        store:Ext.data.StoreManager.lookup('movEntradaStore'),
        columns: [
            {text: 'Referência', width: 100, dataIndex: 'referenciaProduto' },
            {text: 'Descrição', flex: 1, dataIndex: 'descricaoProduto' },
            {text: 'Número Nota', flex: 1, dataIndex: 'numNota' },
            {text: 'Quantidade',flex: 1, dataIndex: 'quantidade'},
            {text: 'Valor R$',flex: 1,dataIndex: 'valor'},
            {text: 'Fornecedor',flex: 1,dataIndex: 'descricaoFornecedor'}
        ],
         viewConfig: {
            listeners: {
                itemdblclick: function(dataview, record, item, index, e) {
                },
                itemclick:function(dataview, record, item, index, e){
                 Ext.getCmp('removeItemButton').enable();
                }
            }
        }
    });

   

     //Adiciona o grid ao painel
    movEntradaPanel.add(resMovGrid);
    Ext.getCmp('comboFornecedor').store = Ext.data.StoreManager.lookup('FornecedorStore');
    Ext.getCmp('comboProduto').store = Ext.data.StoreManager.lookup('ProdutoStore');
    movEntradaPanel.show();
    Ext.data.StoreManager.lookup('movEntradaStore').removeAll(); // fix the phantom row issue
    
   
    });