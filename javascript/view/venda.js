

//Campos e grid de busca de produtos
Ext.define('StoreCamposBusca',{
    extend:'Ext.data.Store',
    storeId:'storeCamposBusca',
    fields:['valor','texto'],
    data:[
        {'valor':'id','texto':'Código'},
        {'valor':'descricao','texto':'Descrição'}
    ]
});

Ext.define('StoreComboPreco',{
    extend:'Ext.data.Store',
    storeId:'storeComboPreco',
    fields:['valor','texto'],
    data:[
        {'valor':'preco_normal','texto':'Normal'},
        {'valor':'preco_promo','texto':'Promocional'}
    ]
});

Ext.define('ModelMovCarteira',{
     extend:'Ext.data.Model',
     fields: [
           {name:'id',type:'int'},
           {name:'descricao',type:'string'},
           {name:'movimento_id',type:'int'},
           {name:'valor',type:'float'},
           {name:'valorNum',type:'float'},
           {name:'update',type:'string'}
       ]
   });

Ext.define('StoreMovCarteira',{
    extend:'Ext.data.Store',
    storeId:'storeMovCarteira',
    model:'ModelMovCarteira'
});



Ext.onReady(function(){

objCliente = Ext.Object.fromQueryString(document.location.search); // returns {foo: '1', bar: '2'}
var clienteId = Ext.Object.getValues(objCliente)[1];
var clienteNome = Ext.Object.getValues(objCliente)[2];

function updateValorCompra() {
    var store = Ext.getCmp('basketGrid').store
    var soma = store.sum('valor_total_item');
    Ext.getCmp('numberValorTotal').setValue(Number(soma));
    Ext.getCmp('textValorTotal').setValue(Ext.util.Format.currency(soma, 'R$',2));
}

function fieldValueToFloat(Strvalue) {

    var Strvalue=Strvalue.replace("R$","");
    var Strvalue=Strvalue.replace(" ","");
    var Strvalue=Strvalue.replace(".","");
    var Strvalue=Strvalue.replace(",",".");
    return parseFloat(Strvalue);
}

function formatValue(value) {
    return Ext.util.Format.currency(value, 'R$',2);
}

function verifyPayment() {



}

function addToBasket(record){

    if(record.get('qtd_loja') > 0) {
       

        var basketStore = Ext.getCmp('basketGrid').store
        var basketRecord =  Ext.create(basketStore.model);

        basketRecord.set('produto_id',record.get('id'));
        basketRecord.set('custo',record.get('custo'));
        basketRecord.set('produto_descricao',record.get('descricao'));
        basketRecord.set('valor_unit',record.get('preco_normal'));
        var preco = record.get('preco_normal');
        basketRecord.set('preco_normal',record.get('preco_normal'));
        basketRecord.set('preco_promo',record.get('preco_promo'));
        basketRecord.set('desconto_maximo',record.get('desconto_maximo'));

        basketRecord.set('desconto_concedido',0);
        basketRecord.set('preco_final',record.get('preco_normal'));
        basketRecord.set('quantidade',Ext.getCmp('textQuantVenda').getValue());
        var qtd = Ext.getCmp('textQuantVenda').getValue();
        basketRecord.set('valor_total_item',(preco*qtd));

        var totalVenda = Number(Ext.getCmp('textValorTotal').getRawValue());
        totalVenda = (totalVenda+ Number(preco*qtd));      
        basketStore.add(basketRecord);
        updateValorCompra();
        Ext.getCmp('textQuantVenda').setValue(1);
        Ext.getCmp('valueField').setValue('');
        Ext.getCmp('propertyCombo').setValue('id');
        Ext.getCmp('buttonBasketPut').disable();
        Ext.getCmp('buttonNegocia').disable();
        Ext.getCmp('buttonBasketRemove').disable();
        Ext.getCmp('searchResultGrid').getSelectionModel().deselectAll();
        Ext.getCmp('basketGrid').getSelectionModel().deselectAll();
    }   
}

function mostraWinVenda() {
    var insertSent = false;
    movCartStore.removeAll();
    
    Ext.create('Ext.window.Window', {
    title: 'Informações de Pagamento',
    height: 380,
    width: 270,
    resizable:false,
    layout: 'fit',
    closeAction:'destroy',
    items: [{
         xtype:'form',
         bodyStyle: 'padding:15px;',
         id:'formFinalizaVenda',
         border: false,
         autoScroll: true,
	 defaultType: 'textfield',
         items:[{
                xtype:'combobox',
                width: 230,
                 style: 'margin-top:8px;'
                ,fieldLabel	: 'Modalidade:'
                ,name	: 'f_pgto'
                ,id :'comboModalidade'
                ,querymode:'local'
                ,valueField:'id'
                ,displayField:'descricao'
                ,valueNotFoudText:'Dinheiro'
                ,editable:false                
                ,allowBlank	: false                
                },{
                 xtype: 'field-money',
                 width: 230,
                readOnly: false,
                id:'valor'
                ,name: 'valor'                
                ,fieldLabel: 'Valor:'
                ,value: 0
                },{  
                xtype: 'grid',
                border: false,
                store:Ext.data.StoreManager.lookup('storeMovCarteira'),
                style: 'margin-top:8px;',
                width: 230,
                id:'gridContasVenda',
                border:true,
                height:120,

                tbar:[{
                        xtype:'button',
                        scope:this,
                        id:'novoItemButton',
                        icon:'images/icons/add.png',                        
                        handler:function() {

                            var inserir = false;                           
                            var pago = 0;
                            var troco = 0;
                            var valCompra = fieldValueToFloat(Ext.getCmp('apagar').getRawValue());

                             //Setup record fields
                            var record = Ext.create(movCartStore.model);
                            var value = (Ext.getCmp('comboModalidade').getValue());
                            var tipo = Ext.getCmp('comboModalidade').findRecordByValue(value).get('descricao');
                            record.set('id',Ext.getCmp('comboModalidade').getValue());
                            record.set('descricao',tipo);
                            record.set('valor',fieldValueToFloat(Ext.getCmp('valor').getValue()));
                            record.set('valorNum',fieldValueToFloat(Ext.getCmp('valor').getValue()));

                             var result = movCartStore.find('descricao',tipo);

                             if (result == -1) {
                                 if (tipo == 'Dinheiro') {                                   
                                    pago = fieldValueToFloat(Ext.getCmp('pago').getRawValue());
                                    troco = ( (pago+parseFloat(record.get('valorNum')) ) -valCompra);
                                    
                                     if (troco > -1 ) {
                                        Ext.getCmp('troco').setFieldLabel('Troco:');
                                        Ext.getCmp('troco').setValue(formatValue(troco));
                                        Ext.getCmp('btnFinalizar').enable();
                                        inserir = true;
                                    } else {
                                        Ext.getCmp('troco').setFieldLabel('Em haver:');
                                        Ext.getCmp('troco').setValue(formatValue(troco*-1));
                                        inserir = true;                                        
                                    }

                                 } else {
                                    valCompra = fieldValueToFloat(Ext.getCmp('apagar').getRawValue());
                                    pago = fieldValueToFloat(Ext.getCmp('pago').getRawValue());
                                    troco = ( (pago+parseFloat(record.get('valorNum')) ) -valCompra);
                                   
                                     if (troco > 0 ) {
                                        Ext.Msg.alert('Pagamento inconsistente','Troco disponível apenas para dnheiro');
                                    } else {
                                        Ext.getCmp('troco').setFieldLabel('Troco:');
                                        Ext.getCmp('troco').setValue(formatValue(troco));
                                        Ext.getCmp('troco').setFieldLabel('Em haver:');
                                        Ext.getCmp('troco').setValue(formatValue(troco*-1));
                                        inserir = true;
                                        inserir = true;
                                    }
                                 }

                                 if (inserir) {
                                     movCartStore.add(record);
                                     inserir = false;
                                     //Libera encerramento quando pago >= a apagar
                                     if (movCartStore.sum('valorNum') >= fieldValueToFloat(Ext.getCmp('apagar').getRawValue())) {
                                        Ext.getCmp('novoItemButton').disable();
                                        Ext.getCmp('btnFinalizar').enable();
                                     }
                                 }

                             } else {
                                  Ext.Msg.alert('Modalidade Duplicada','Esta modalidade já está sendo utilizada');
                             }

                             Ext.getCmp('pago').setValue(formatValue(movCartStore.sum('valorNum')));
                        }
                   }],
                    columns: [
                        {text: 'Pagamento em',  dataIndex: 'descricao', flex: 1},
                        {text: 'Valor', dataIndex: 'valor',renderer:function(v){return Ext.util.Format.currency(v, 'R$',2);}}
                    ]               
            },{
                xtype:'textfield',
                width:230,
                 style: 'margin-top:8px;',
                 id:'apagar'
                ,readOnly: true
                ,name: 'apagar'
                ,fieldLabel: 'Total a pagar:'
                ,value:0
            },{
                xtype:'textfield',
                width:230,
                style: 'margin-top:8px;'
                 ,name: 'pago'
                 ,id: 'pago'
                ,fieldLabel: 'Pago:'
                ,readOnly: true
                ,value:0
            },{
                xtype:'textfield',
                width:230,
                style: 'margin-top:8px;'
                ,readOnly: false
                ,name: 'troco'
                ,id: 'troco'
                ,fieldLabel: 'Troco:'
                ,value:0
            }],
         buttons:[{
                 text:'OK',
                 id:'btnFinalizar',
                 icon:'images/icons/accept.png',
                 disabled:true,                 
                 handler: function() {

                     if (!insertSent) {

                    //Controls the double sent inserts
                    insertSent = true;                  

                  //create Resumo da venda
                     var resumoVenda = new ResumoVenda();
                     resumoVenda.clienteId = clienteId;
                     resumoVenda.usuarioId = id_usuario;
                     resumoVenda.lojaId = id_loja;
                     resumoVenda.troco = fieldValueToFloat(Ext.getCmp('troco').getRawValue());
                    
                     vendaStore.each(function(record){
                         movProduto = new MovProduto();
                         movProduto.prodId = record.get('produto_id');
                         movProduto.desconto = record.get('desconto_concedido');
                         movProduto.quantidade = record.get('quantidade');
                         movProduto.precoFinal = record.get('preco_final');
                         movProduto.precoNormal = record.get('preco_normal');
                         movProduto.precoPromo = record.get('preco_promo');
                         movProduto.valorTotal = record.get('valor_total_item');
                         movProduto.custo = record.get('custo');
                         resumoVenda.addMovProduto(movProduto);
                     });

                     //Para cada item no CartGrid
                     movCartStore.each(function(record){
                        movCarteira = new MovCarteira();
                        movCarteira.contaId = record.get('id');
                        if (record.get('id') == 1) { //diminui o troco da entrada no caixa
                            var bruto = parseFloat(record.get('valorNum'));
                            movCarteira.valor = bruto-resumoVenda.troco;
                        }else{
                             movCarteira.valor = record.get('valorNum');
                        }
                        resumoVenda.addMovCarteira(movCarteira);
                     });

                var form = Ext.getCmp('formFinalizaVenda');

                form.submit({
                        url:'php/ajax/venda.php?action=insert',
                        params: {'venda':Ext.JSON.encode(resumoVenda)},
                        success: function (form, action) {

                            Ext.Msg.show({
                                   title:'Sucesso na operação',
                                   msg: 'Operação Efetuada com sucesso.',
                                   buttons: Ext.Msg.OK,
                                   fn: function(btn){
                                        if (btn == 'ok'){
                                            window.location = "index.php?action=Clientes"
                                        }
                                    },
                                   animEl: 'elId'
                                })
                        },
                        failure: function (responseform, action) {
                            Ext.Msg.alert('Falha na operação','Não foi possível efetuar a operação.');
                        }
                    });


                    }
         }
                }]
            }]
    }).show();
    Ext.getCmp('comboModalidade').store = Ext.data.StoreManager.lookup('contaStore');
    Ext.getCmp('comboModalidade').setValue(1);
    Ext.getCmp('apagar').setValue(Ext.getCmp('textValorTotal').getValue());    
    Ext.getCmp('valor').focus();
    
 
}

function ResumoVenda() {

    this.clienteId = 0;
    this.usuarioId = 0;
    this.lojaId = 0;
    this.listaMovCarteira = Array();
    this.listaMovProduto = Array();
    this.troco = 0;
    
    this.addMovCarteira = function(movCart) {        
        this.listaMovCarteira.push(movCart);
    }
    
    this.addMovProduto = function(movProd) {
        this.listaMovProduto.push(movProd);
    }
}

function MovCarteira() {    
    this.contaId = 0;
    this.valor = 0;
   
}

function MovProduto() {
    this.prodId = 0;
    this.desconto = 0;
    this.precoFinal = 0;
    this.quantidade = 0;
    this.valorTotal = 0;
    this.custo = 0;
    this.precoNormal = 0;
    this.precoPromo = 0;
}

function mostraWinNegocia() {

     Ext.create('Ext.window.Window', {
        ID : 0,
        id:'winNegocia',
        modal:true,
        title:'Negociação',
        layout:'fit',
        closeAction:'destroy',        
        items: [{
            xtype:'form',
            bodyStyle: 'padding:15px;',
            buttons:[{
                 text:'OK',
                 id:'btnOK',
                 icon:'images/icons/accept.png',
                 handler: function() {
                     var record =  Ext.getCmp('basketGrid').getSelectionModel().getLastSelected();
                     var preco_final = fieldValueToFloat(Ext.getCmp('preco_com_desconto').getRawValue());
                     var qtd = Number(record.get('quantidade'));
                     record.set('preco_final',fieldValueToFloat(Ext.getCmp('preco_com_desconto').getRawValue()));
                     record.set('desconto_concedido',Ext.getCmp('desconto').getValue());
                     record.set('valor_total_item',(preco_final*qtd));
                     updateValorCompra();
                     Ext.getCmp('winNegocia').close();



                 }
            }],
            items:[{
                xtype:"textfield",
                fieldLabel:"Produto",
                name:"descricao",
                id:"descricao",
                width:290,
                labelWidth:120
              },{
                xtype:"combo",
                fieldLabel:"Aplicar preço:",
                name:"select_preco",
                id:"select_preco",
                querymode:'local',
                valueField:'valor',
                displayField:'texto',
                hiddenName:"combovalue",
                editable:false,
                width:290,
                labelWidth:120,
                store:[['preco_normal','Normal'],['preco_promo','Promocional']],
                listeners:{select:function(combo) {
                                    if (combo.getValue()=='preco_normal') {
                                        Ext.getCmp('preco_com_desconto').setValue(formatValue(record.get('preco_normal')));
                                        Ext.getCmp('desconto').enable();
                                    }
                                    if (combo.getValue()=='preco_promo') {
                                         Ext.getCmp('preco_com_desconto').setValue(formatValue(record.get('preco_promo')));
                                         Ext.getCmp('desconto').setValue(0);
                                         Ext.getCmp('desconto').disable();
                                    }
                                }
                }
              },{
                xtype:"numberfield",
                fieldLabel:"Aplicar desconto %",
                name:"desconto",
                id:"desconto",
                value:0,
                width:290,
                labelWidth:120,
                listeners:{
                    change:function(combo, newValue, oldValue, eOpts ) {
                        if (newValue > 0) {
                            var pNorm = fieldValueToFloat(Ext.getCmp('preco_sem_desconto').getRawValue());
                            var descont = Number((pNorm*newValue)/100);
                            var pNeg = Number(pNorm-descont);
                            Ext.getCmp('preco_com_desconto').setValue(formatValue(pNeg));                            
                        }

                    }
                }
              },{
                xtype:"field-money",
                fieldLabel:"Preço normal",
                name:"preco_sem_desconto",
                id:"preco_sem_desconto",
                readOnly:true,
                value:0,
                width:290,
                labelWidth:120
              },{
                xtype:"field-money",
                fieldLabel:"Preço Negociado",
                name:"preco_com_desconto",
                id:"preco_com_desconto",
                readOnly:true,
                value:0,
                width:290,
                labelWidth:120
            }]
        }]
    }).show();

    Ext.getCmp('select_preco').setValue('preco_normal');
    var record =  Ext.getCmp('basketGrid').getSelectionModel().getLastSelected();    
    //Tranfere os valores do record do produto para o form
    Ext.getCmp('descricao').setValue(record.get('produto_descricao'));
    Ext.getCmp('preco_sem_desconto').setValue(formatValue(record.get('preco_normal')));
    Ext.getCmp('preco_com_desconto').setValue(formatValue(record.get('preco_normal')));
    Ext.getCmp('desconto').setMaxValue(record.get('desconto_maximo'));
    
}

   //Stores:
   var produtoStore = Ext.create('ProdutoStore');
   var vendaStore = Ext.create('VendaStore');
   var contaStore = Ext.create('ContaStore');
   var movCartStore = Ext.create('StoreMovCarteira');
   var storeCamposBusca = Ext.create('StoreCamposBusca');
   var storeComboPreco = Ext.create('StoreComboPreco');
   
   contaStore.load();

   produtoStore.addListener('datachanged', //Verifica método de busca e insere item x quantidade no movimento
        function(store, e){
            if ( (store.getCount() == 1) && (Ext.getCmp('propertyCombo').getValue() == 'id') ) {               
                record = store.getAt(0);               
                addToBasket(record);                             
            }
        });

   vendaStore.addListener('datachanged',
        function(dataview, record, item, index, e) {
            if (Ext.data.StoreManager.lookup('vendaStore').count() > 0) {
                    Ext.getCmp('buttonBasketOrder').enable();
            } else {
                Ext.getCmp('buttonBasketOrder').disable();
            }
        });

    //SearchResultGrid

    Ext.create('Ext.grid.Panel',{
    id:'searchResultGrid',
    title:'Venda para Cliente - '+ clienteNome,
    store:Ext.data.StoreManager.lookup('produtoStore'),
    border:false,
    renderTo:'main',
    height:160,
    columns:[
        {header:'Descrição',dataIndex:'descricao',flex:1},
        {header:'Preço Normal',dataIndex:'preco_normal',renderer:function(v){return Ext.util.Format.currency(v, 'R$',2);}},
        {header:'Na Promoção',dataIndex:'preco_promo',renderer:function(v){return Ext.util.Format.currency(v, 'R$',2);}},
        {header:'Desconto Máx',dataIndex:'desconto_maximo'},
        {header:'Qtd no local',dataIndex:'qtd_loja'},
        {header:'Qtd total',dataIndex:'qtd_total'}
    ], listeners:{
            itemclick:function(dataview, record, item, index, e){
                Ext.getCmp('buttonBasketPut').enable();
                },
            itemdblclick:function(dataview, record, item, index, e) {                           
                Ext.getCmp('buttonBasketPut').disable();
                addToBasket(record);   
                }
     },
    dockedItems:[{
        xtype:'toolbar',
        id:'searchToolBar',
        renderTo:'main',
        border:'false',
        items:[{
            xtype:'combobox',
            id:'propertyCombo',
            fieldLabel :'Pesquisar por:',
            align:"right",
            valueField:'valor',
            displayField:'texto',
            listeners:{
                select:function(combo) {
                        if (combo.getValue()=='id') {
                        Ext.getCmp('valueField').searchLenght = 5
                        }
                        if (combo.getValue()=='descricao') {
                         Ext.getCmp('valueField').searchLenght = 2
                        }
                    }
                }
            },{
            xtype:"textfield",
            fieldLabel:"Busca:",
            id:'valueField',
            searchLenght:5,
            style:{marginLeft:'45px'},
            labelWidth:45,
            enableKeyEvents:true,
            listeners: {     // Fires the filter on 3th character
                keyup: function(campo) {
                    if (campo.getValue().length > campo.searchLenght) {
                        var store = Ext.getCmp('searchResultGrid').store;
                        store.clearFilter(true);
                        store.filter(Ext.getCmp('propertyCombo').getValue(),campo.getValue());
                        }
                    },
                            specialkey: function(f,e){
                                if(e.getKey()==e.ENTER){
                                    var store = Ext.getCmp('searchResultGrid').store;
                                    store.clearFilter(true);
                                    store.filter(Ext.getCmp('propertyCombo').getValue(),Ext.getCmp('valueField').getValue());
                                }
                            }
                }
            },{
                     text : 'Buscar',
                     xtype:'button',
                     style:{marginLeft:'20px'},
                     icon:'images/icons/find.png',
                     handler:function() {
                            var store = Ext.getCmp('searchResultGrid').store;
                            store.clearFilter(true);
                            store.filter(Ext.getCmp('propertyCombo').getValue(),Ext.getCmp('valueField').getValue());

                        }
                    }
        ]}
    ]

    });


Ext.create('Ext.toolbar.Toolbar',{
    renderTo:'main',
    id:'basketControlBar',
     border:false,    
    items:[{
        xtype:'button',
        id:'buttonBasketPut',
        text : 'Adicionar ao cesto',
        icon:'images/icons/basket_put.png',
        disabled:true,
        handler:function(){
            var record =  Ext.getCmp('searchResultGrid').getSelectionModel().getLastSelected();
            addToBasket(record);
            Ext.getCmp('searchResultGrid').getSelectionModel().deselectAll();         
            }
       
    },{
        xtype:'button',
        id:'buttonBasketRemove',
        text : 'Remover do cesto',
        icon:'images/icons/basket_remove.png',
        disabled:true,
        handler:function() {
            var record =  Ext.getCmp('basketGrid').getSelectionModel().getLastSelected( );
            var gridStore = Ext.getCmp('basketGrid').getStore().remove(record);

             updateValorCompra();

            this.disable();
            Ext.getCmp('buttonBasketPut').disable();
            Ext.getCmp('buttonNegocia').disable();
            Ext.getCmp('searchResultGrid').getSelectionModel().deselectAll();
        }

    },{
        xtype:"numberfield",
        style:{marginLeft:'25px'},
        fieldLabel:"Unidades:",
        name:"textQuantVenda",
        id:'textQuantVenda',
        width:105,
        labelWidth:45,
        value:1,
        minValue:1,
        listeners:{
            change:function( formField,novoValor,antValor,e ){
                if (novoValor > this.maxValue) {
                    this.setValue(this.maxValue);
                }
            }
        }
    },{
        xtype:'button',
        id:'buttonNegocia',
        text : 'Aplicar desconto',
        icon:'images/icons/basket_remove.png',
        style:{marginLeft:'25px'},
        disabled:true,
        handler:function() {
            mostraWinNegocia();
           
        }

    }]
});

 Ext.create('Ext.grid.Panel',{
    id:'basketGrid',
    renderTo:'main',
    height:268,
    border:false,
    store:Ext.data.StoreManager.lookup('vendaStore'),
    title:'Cesto de compras',
    columns: [
        {header: 'Código',  dataIndex: 'produto_id'},
        {header: 'Descrição', dataIndex: 'produto_descricao', flex: 1},
        {header: 'Preço unit.', dataIndex: 'valor_unit',renderer:function(v){return Ext.util.Format.currency(v, 'R$',2);}},
        {header: 'Desconto %', dataIndex: 'desconto_concedido'},
        {header: 'Preço final', dataIndex: 'preco_final',renderer:function(v){return Ext.util.Format.currency(v, 'R$',2);}},
        {header: 'Quantidade', dataIndex: 'quantidade'},
        {header: 'Total', dataIndex: 'valor_total_item',renderer:function(v){return Ext.util.Format.currency(v, 'R$',2);}}
    ],
     listeners:{
            itemclick:function(dataview, record, item, index, e){
                Ext.getCmp('buttonBasketRemove').enable();
                Ext.getCmp('buttonNegocia').enable();
                Ext.getCmp('buttonBasketPut').disable();
                },          
            itemdblclick:function(dataview, record, item, index, e) {               
                var gridStore = Ext.getCmp('basketGrid').getStore().remove(record);                         
                Ext.getCmp('buttonBasketPut').disable();
                Ext.getCmp('buttonBasketRemove').disable();
                Ext.getCmp('buttonNegocia').disable();
                Ext.getCmp('searchResultGrid').getSelectionModel().deselectAll();           
                }                
     }      
            
 });

 Ext.create('Ext.toolbar.Toolbar',{
    id:'payingControlBar',
    renderTo:'main',
    border:false,
    items:[{
            xtype:"textfield",
            fieldLabel:"Valor Total",
            fieldStyle: 'color:#C00;font-size:28px;width:200px;height:40px;text-align:right',
            labelStyle: 'color:#C00;font-size:22px;padding-top:15px',
            labelWidth:125,
            name:"textValorTotal",
            id:'textValorTotal',
            height:40,
            readOnly:true,
            margin:"0 0 0 280",
            width:325

         },{
            xtype:"numberfield",
            name:"numberValorTotal",
            id:'numberValorTotal',
            hidden:true

         },{
        xtype:'button',
        id:'buttonBasketOrder',
        text : 'Finalizar compra',
        icon:'images/icons/tick-24.png',
        align:"right",
        margin:"0 0 0 100",
        disabled:true,
        scale:'large',
        handler:function() {
            mostraWinVenda();
            
            }
        }
    ]
});
    
    //Associations
    var comboBusca = Ext.getCmp('propertyCombo');
    comboBusca.store = storeCamposBusca;
    comboBusca.setValue('id');
    
});