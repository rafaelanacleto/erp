
Ext.create('Ext.data.Store',{               
                fields:['valor','texto'],
                storeId:'TipoBuscaStore',
                data:[
                    {"valor":"id","texto":"Código"},
                    {"valor":"descricao_produto","texto":"Descrição"},
                    {"valor":"referencia","texto":"Referência"}
                ]
            });

Ext.define('ResBuscaGrid',{
    extend:'Ext.grid.Panel',
    id:'resBuscaGrid',
    renderTo:'main',
    height:189,
    border:false,    
    title:'Pesquisa de Produtos',
    columns: [
        {header: 'Código',  dataIndex: 'id'},
        {header: 'Referência',  dataIndex: 'referencia'},
        {header: 'Descrição', dataIndex: 'descricao_produto', flex: 1},
        {header: 'Preço', dataIndex: 'preco', renderer:Ext.util.Format.usMoney},
        {header: 'Quantidade', dataIndex: 'quantidade'},
        {header: 'Fornecedor', dataIndex: 'descricao_fornecedor'}
    ]});

//  Products on basket display grid
            
Ext.define('BasketGrid',{
    extend:"Ext.grid.Panel",
    id:'basketGrid',
    renderTo:'main',
    height:200,
    border:false,
    store:Ext.data.StoreManager.lookup('MovVendaStore'),
    title:'Cesto de compras',
    columns: [
        {header: 'Código',  dataIndex: 'idProduto'},
        {header: 'Referência',  dataIndex: 'referenciaProduto'},
        {header: 'Descrição', dataIndex: 'descricaoProduto', flex: 1},
        {header: 'Preço unit.', dataIndex: 'valor'},
        {header: 'Quantidade', dataIndex: 'quantidade'},
        {header: 'Valor', dataIndex: 'total_item'},
        {header: 'Fornecedor', dataIndex: 'descricaoFornecedor'}
    ]   
    
});

//Item's move buttons


//   Paying informations form

// Paying control bar
Ext.define('PayingControlBar',{
    extend:'Ext.toolbar.Toolbar',
    id:'payingControlBar',
    renderTo:'main',
    border:false,
    items:[
        {
            xtype:"combobox",
            store:Ext.data.StoreManager.lookup('TipoFinanceiroStore'),
            fieldLabel:"Pagamento em",
            name:"combotipofin",
            id:'combotipofin',           
            valueField:"id",
            displayField:"descricao_fin",
            value:'Dinheiro',
            originalValue:"Dinheiro",
            labelWidth:86,
            width:200,
            listeners:{
                change:function() {    // Ativa combo do produto e atualiza lista para o fornecedor                                      
                  
                }
            }
        },{
            xtype:"textfield",
            fieldLabel:"Valor Total",            
            fieldStyle: 'color:#C00;font-size:28px;width:200px;height:40px;text-align:right',
            labelStyle: 'color:#C00;font-size:22px;padding-top:15px',
            labelWidth:125,
            name:"textValorTotal",
            id:'textValorTotal',            
            height:40,
            readOnly:true,
            margin:"0 0 0 280",
            width:325,
            value:'R$0,00' 
         }        
    ]
});

Ext.define('PayingAuxControlBar',{
    extend:'Ext.toolbar.Toolbar',
    id:'payingAuxControlBar',
    renderTo:'main',
     border:false,
     height:39,
    items:[{
        xtype:'button',
        id:'buttonBasketOrder',
        text : 'Finalizar compra',
        icon:'images/icons/tick-24.png',
        align:"right",
        margin:"0 0 0 680",
        disabled:true,
        scale:'medium',
        handler:function() {
                var store = Ext.getCmp('basketGrid').getStore();
                var tipo_fin_value = Ext.getCmp('combotipofin').getValue();
                store.getProxy().extraParams= {tipo_mov: "1",tipo_fin_mov:tipo_fin_value,nominata:"Cliente Varejo"}
                store.sync();               
                store.removeAll();
                // reset screen data and options except the search criteria
                Ext.getCmp('textBusca').setValue('');
                var store2 = Ext.getCmp('resBuscaGrid').getStore();
                store2.load();
                Ext.Msg.alert('Gravar movimento', 'Venda registrada com sucesso.');
            }
        }]
});
              
       
Ext.onReady(function(){

    //Creates the needed stores
    var storeProduto = Ext.create('ProdutoBuscaStore');    
    var storeMovVenda = Ext.create('MovVendaStore');
    var storeTipoFinanceiro = Ext.create('TipoFinanceiroStore');

    //customization of ResBuscaGrid
   
   var resBuscaGrid = Ext.create('ResBuscaGrid',{
       store:Ext.data.StoreManager.lookup('ProdutoBuscaStore'),
       id:'resBuscaGrid',      
       listeners:{
            selectionchange: function(dataview, record, item, index, e) {  // Disables put on basket button when are no selected items
                   
                  if (this.getSelectionModel().getLastSelected( ).get('quantidade') == 0) { //produto indisponivel
                       Ext.getCmp('buttonBasketPut').disable();
                       Ext.getCmp('textQuantVenda').setValue(1);
                       Ext.getCmp('textQuantVenda').disable();
                      
                  } else {      // Produto disponivel 
                       
                        var result = Ext.getCmp('basketGrid').getStore().find('idProduto',this.getSelectionModel().getLastSelected( ).get('id'));
                        
                        if (result == -1) { //Produto não existente no cesto
                            Ext.getCmp('buttonBasketPut').enable();
                            Ext.getCmp('textQuantVenda').setValue(1);
                            Ext.getCmp('textQuantVenda').enable();
                            Ext.getCmp('textQuantVenda').maxValue = this.getSelectionModel().getLastSelected( ).get('quantidade');                            
                        } else {
                            Ext.getCmp('buttonBasketPut').disable();
                       Ext.getCmp('textQuantVenda').setValue(1);
                       Ext.getCmp('textQuantVenda').disable();
                        } 
                  }
            }
       },
       viewConfig: {
            listeners: {
                itemdblclick: function(dataview, record, item, index, e) { // Adds one unit of the double-clicked item to the basket
                    var arrSelection = this.getSelectionModel().getLastSelected( );
                    if (arrSelection.get('quantidade') != 0) {
                        
                        var result = Ext.getCmp('basketGrid').getStore().find('idProduto',this.getSelectionModel().getLastSelected( ).get('id'));                        
                        if (result == -1) { //Produto não existente no cesto
                            var record=Ext.create('MovVendaModel');
                            record.set('idProduto',arrSelection.get('id'));
                            record.set('referenciaProduto',arrSelection.get('referencia'));
                            record.set('descricaoProduto',arrSelection.get('descricao_produto'));
                            record.set('valor',arrSelection.get('preco'));
                            record.set('idFornecedor',arrSelection.get('id_fornecedor'));
                            record.set('descricaoFornecedor',arrSelection.get('descricao_fornecedor'));
                            record.set('quantidade',Ext.getCmp('textQuantVenda').value);
                            var valor_calculado = (Ext.getCmp('textQuantVenda').getValue()*arrSelection.get('preco'));
                            record.set('total_item',valor_calculado);
                            var gridStore = Ext.getCmp('basketGrid').getStore();
                            gridStore.add(record);
                            Ext.getCmp('resBuscaGrid').getSelectionModel().deselectAll();
                        }else{
                            
                        }
                    } else {
                        Ext.Msg.alert('Indisponibilidade','Produto indisponível no estoque.');
                        Ext.getCmp('resBuscaGrid').getSelectionModel().deselectAll();
                        Ext.getCmp('textQuantVenda').setValue(1);
                    }          

                },               
                itemclick:function(dataview, record, item, index, e){
                    
                }
            }
        },
        dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            id:'pagingToolbarBusca',
            emptyMsg:'Nenhum registro',
            displayMsg:'Exibindo {0} - {1} de {2} reg. localizados',
            store:Ext.data.StoreManager.lookup('ProdutoBuscaStore'),
            displayInfo: true
            },{
            xtype: 'toolbar',
            dock: 'top',           
            items:[{
                xtype:"combobox",
                flex:2,
                labelWidth:60,
                store:Ext.data.StoreManager.lookup('TipoBuscaStore'),
                fieldLabel:"Critério",
                name:"comboCriterio",
                id:'comboCriterio',
                querymode:"local",
                valueField:"valor",
                displayField:"texto",
                value:'descricao_produto',
                originalValue:"descricao_produto"
            },{
                style:{marginLeft:'25px'},
                xtype:"textfield",
                flex:4,
                labelWidth:75,
                fieldLabel:"Buscar por",
                name:"textBusca",
                id:"textBusca"
            },{
                style:{marginLeft:'25px'},
                xtype:"button",
                text:'Pesquisar',
                id:'buttonPesquisar',
                icon:'images/icons/find.png',
                handler: function() {                    
                    var store = Ext.getCmp('resBuscaGrid').store;
                    store.clearFilter();
                    var propertyName = Ext.getCmp('comboCriterio').getValue();
                    var filtervalue = Ext.getCmp('textBusca').getValue();
                    Ext.getCmp('pagingToolbarBusca').moveFirst();
                    store.filter(propertyName,filtervalue);
                }

        }]            
            }]      
   });
   var basketControlBar = Ext.create('BasketControlBar');

   var basketGrid = Ext.create('BasketGrid',{
        store:Ext.data.StoreManager.lookup('movVendaStore'),
        id:'basketGrid',
        emptyText:'Nenhum produto no cesto.',
        viewConfig: {
            listeners: {
                itemdblclick: function(dataview, record, item, index, e) {                     
                },
                itemclick:function(dataview, record, item, index, e){
                    Ext.getCmp('buttonBasketRemove').enable();
                }
               
            }  
        }
    });

    Ext.getCmp('basketGrid').store.removeAll(); // fix the phantom row issue
       
   //var finalizaVendaForm = Ext.create('FinalizaVendaForm');
   var payingToolBar = Ext.create('PayingControlBar');
   var payingAuxToolBar = Ext.create('PayingAuxControlBar');
   Ext.getCmp('combotipofin').store = Ext.data.StoreManager.lookup('TipoFinanceiroStore');

   Ext.data.StoreManager.lookup('movVendaStore').addListener('datachanged', function(dataview, record, item, index, e){

          if (Ext.data.StoreManager.lookup('movVendaStore').count() > 0) {
          var soma = Ext.data.StoreManager.lookup('movVendaStore').sum('total_item');
            Ext.getCmp('textValorTotal').setValue(Ext.util.Format.currency(soma,'',2,false));
            Ext.getCmp('buttonBasketOrder').enable();
          } else {
            Ext.getCmp('buttonBasketOrder').disable();
            Ext.getCmp('textValorTotal').setValue('R$0,00');
          }
      }, this );

});