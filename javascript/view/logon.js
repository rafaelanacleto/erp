Ext.define('WinLogon',{
        extend:'Ext.Window',
        ID : 0,
        id:'winLogon',       
        modal:true, 
        closable:false,
        title:'Fazer Logon no Sistema',
        layout:'fit',
        closeAction:'destroy',
        rederTo:'central',
        setID:function(id){
            this.ID = id;
        }
    });
    

    Ext.define('LogonForm',{
        extend:'Ext.form.Panel',              
        bodyStyle: 'padding:55px;', 
        id:'logonForm',
        border: false,
        autoScroll: true,
        standardSubmit: false,
        defaultType: 'textfield',
        defaults: {anchor: '-19'},
        buttons:[{            
            text:'Logon',            
            icon:'images/icons/accept.png',
            handler: function() {

                var form = Ext.getCmp('logonForm');
                //problema no isValid, colocado true para debug - form.isValid()
                if (true) {                   
                    form.submit({
                        url:'php/ajax/logon.php',                       
                        success: function (form, action) {
                          window.location="index.php?action=Clientes";
                        },
                        failure: function (responseform, action) {
                            Ext.Msg.alert('Erro de logon',action.result.errors.erro)                           
                        }
                    });
                }               
            }            
        }],        
        items:[{
                xtype:'textfield'
                ,fieldLabel	: 'Usuário'
                ,name	: 'user_id'
                ,width:330
                ,allowBlank	: false
                ,maxLength	: 100
                ,labelWidth:60
            },{
                xtype:'textfield',
                inputType:'password'                
                ,fieldLabel: 'Senha'
                ,name	: 'pwd'
                ,width:330
                ,allowBlank	: false
                ,maxLength	: 100
                ,labelWidth:60
            },{
                xtype:'combobox'
                ,fieldLabel	: 'Loja'
                ,name	: 'loja'
                ,id:'comboLoja'
                ,querymode:'remote'
                ,valueField:'id'
                ,displayField:'descricao'
                ,width:330
                ,allowBlank	: false
                ,maxLength	: 100
                ,labelWidth:60
            }]
        
        
    });
        
Ext.onReady(function(){
    
    var lojaStore = Ext.create('LojaStore');
    lojaStore.load();
    var winLogon = Ext.create(WinLogon);
    var logonForm = Ext.create('LogonForm'); 
    Ext.getCmp('comboLoja').store = Ext.data.StoreManager.lookup('lojaStore');
    
    winLogon.add(logonForm);
    winLogon.show();
    
    
});
