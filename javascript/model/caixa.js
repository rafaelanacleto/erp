

Ext.require('Ext.data.*');

Ext.define('Caixa', {
    extend:'Ext.data.Model',
    fields: [
           {name:'id',type:'int'},
           {name:'origem_loja_id',type:'int'},
           {name:'operacao',type:'string'},
           {name:'valor',type:'float'},
           {name:'up_date',type:'date'},
           {name:'usuario',type:'string'},
           {name:'usuario_id',type:'int'},
           {name:'entDinheiro',type:'float'},
           {name:'entradas',type:'float'},
           {name:'gaveta',type:'float'},
           {name:'retirada',type:'float'},
           {name:'trocoInicio',type:'float'},
           {name:'troco_inicial',type:'float'},
           {name:'troco_inicial_pres',type:'float'},
           {name:'troco_inicial_const',type:'float'},
           {name:'troco_final',type:'float'},
           {name:'sobra',type:'float'},
           {name:'falta',type:'float'},
           {name:'sangrias',type:'float'},
           {name:'historico',type:'string'}
       ]
   });


    Ext.define('CaixaStore',{
    extend:'Ext.data.Store',
    model:'Caixa',
    storeId:'caixaStore',
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:19,
    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/caixa.php?action=insert',
            read:'php/ajax/caixa.php?action=listAll',
            update:'php/ajax/caixa.php?action=update',
            destroy:''
        },

        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },

        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
});


    Ext.define('CaixaStoreClose',{
    extend:'Ext.data.Store',
    model:'Caixa',
    storeId:'storeCaixaClose',
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:19,
    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/caixa.php?action=insert',
            read:'php/ajax/caixa.php?action=getCloseInfo',
            update:'php/ajax/caixa.php?action=close',
            destroy:''
        },
        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },
        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
    });

    Ext.define('CaixaStoreOpen',{
    extend:'Ext.data.Store',
    model:'Caixa',
    storeId:'storeCaixaOpen',
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:19,
    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/caixa.php?action=insert',
            read:'php/ajax/caixa.php?action=getOpenInfo',
            update:'php/ajax/caixa.php?action=open',
            destroy:''
        },
        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },
        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
    });

    Ext.define('CaixaStoreSangria',{
    extend:'Ext.data.Store',
    model:'Caixa',
    storeId:'storeCaixaSangria',
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:19,
    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/caixa.php?action=drain',
            read:'php/ajax/caixa.php?action=getOpenInfo',
            update:'php/ajax/caixa.php?action=sangria',
            destroy:''
        },
        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },
        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
    });