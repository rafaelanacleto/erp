


Ext.require('Ext.data.*');

Ext.define('Cliente', {
    extend:'Ext.data.Model',
    fields: [
           {name:'id',type:'int'},
           {name:'nome',type:'string'},
           {name:'nascimento',type: 'date',dateFormat:'Y-m-d'},
           {name:'fone',type:'String'},
           {name:'email',type:'String'},
           {name:'facebook',type:'String'},
           {name:'recebe_email',type:'String'}
       ]
   });


    Ext.define('clienteStore',{
    extend:'Ext.data.Store',
    model:'Cliente',
    storeId:'clienteStore',    
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:19,
    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/cliente.php?action=insert',
            read:'php/ajax/cliente.php?action=listCliente',
            update:'php/ajax/cliente.php?action=update',
            destroy:''
        },

        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },

        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
});