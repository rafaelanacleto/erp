/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.require('Ext.data.*');

Ext.define('TipoFinanceiro', {
    extend:'Ext.data.Model',
    fields: [
           {name:'id',type:'int'},
           {name:'descricao_fin',type:'string'},
           {name:'prazo',type:'int'},
       ]}
   );


 Ext.define('TipoFinanceiroStore',{
    extend:'Ext.data.Store',
    model:'TipoFinanceiro',
    storeId:'TipoFinanceiroStore',
    pageSize:9999,
    autoLoad: true,
    remoteFilter:true,
    remoteSorter:true,
    sorters:[{
        property:'descricao_fin',
        direction:'ASC'}
    ],

    proxy: {
        type:'ajax',
        url:'php/ajax/tipoFinanceiro.php?action=listTipoFinanceiro',
        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        }
    }
});


