

Ext.require('Ext.data.*');

Ext.define('Movimento', {
    extend:'Ext.data.Model',
    fields: [
           {name:'id',type:'int'},
           {name:'tipo_movimento',type:'int'},
           {name:'cliente_id',type:'int'},
           {name:'destino_estoque_id',type:'int'},
           {name:'valor',type:'float'},
           {name:'usuario_id',type:'int'},
           {name:'update',type:'date'},
           {name:'entregue',type:'string'},
           {name:'origem_estoque_id',type:'int'},
           {name:'nota',type:'string'},
       ]
   });

   Ext.define('MovimentoStore',{
    extend:'Ext.data.Store',
    model:'Movimento',
    storeId:'movimentoStore',
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:19,

    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/movimento.php?action=insert',
            read:'php/ajax/movimento.php?action=listMovimento',
            update:'php/ajax/movimento.php?action=update',
            destroy:''
        },

        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },

        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
});



