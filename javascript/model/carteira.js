

Ext.require('Ext.data.*');

Ext.define('Carteira', {
    extend:'Ext.data.Model',
    fields: [
           {name:'id',type:'int'},
           {name:'conta_id',type:'int'},
           {name:'movimento_id',type:'int'},
           {name:'descricao',type:'string'},
           {name:'valor',type:'float'},
           {name:'update',type:'string'}

       ]
   });


    Ext.define('CarteiraStore',{
    extend:'Ext.data.Store',
    model:'Carteira',
    storeId:'carteiraStore',
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:19,
    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/carteira.php?action=insert',
            read:'php/ajax/carteira.php?action=listCarteira',
            update:'php/ajax/carteira.php?action=update',
            destroy:''
        },

        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },

        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
});