

Ext.require('Ext.data.*');

Ext.define('Estoque', {
    extend:'Ext.data.Model',
    fields: [
           {name:'id',type:'int'},
           {name:'produto_id',type:'int'},
           {name:'loja_id',type:'int'},
           {name:'quantidade',type:'int'},
           {name:'update',type:'date'}
       ]
   });

   Ext.define('EstoqueStore',{
    extend:'Ext.data.Store',
    model:'Estoque',
    storeId:'estoqueStore',
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:19,

    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/estoque.php?action=insert',
            read:'php/ajax/estoque.php?action=listMovimento',
            update:'php/ajax/estoque.php?action=update',
            destroy:''
        },

        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },

        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
});



