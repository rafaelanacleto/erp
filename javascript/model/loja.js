Ext.require('Ext.data.*');

Ext.define('Loja', {
    extend:'Ext.data.Model',
    fields: [
           {name:'id',type:'int'},
           {name:'descricao',type:'string'},
           {name:'localizacao',type:'string'}
       ]
   });


    Ext.define('LojaStore',{
    extend:'Ext.data.Store',
    model:'Loja',
    storeId:'lojaStore',
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:19,
    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/loja.php?action=insert',
            read:'php/ajax/loja.php?action=listLoja',
            update:'php/ajax/loja.php?action=update',
            destroy:''
        },

        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },

        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
});