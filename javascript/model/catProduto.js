Ext.require('Ext.data.*');

Ext.define('CatProduto', {
    extend:'Ext.data.Model',
    fields: [
           {name:'id',type:'int'},
           {name:'descricao',type:'string'}
       ]
   });


    Ext.define('CatProdutoStore',{
    extend:'Ext.data.Store',
    model:'CatProduto',
    storeId:'catProdutoStore',
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:19,
    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/catProduto.php?action=insert',
            read:'php/ajax/catProduto.php?action=listCatProduto',
            update:'php/ajax/catProduto.php?action=update',
            destroy:''
        },

        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },

        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
});