Ext.require('Ext.data.*');

Ext.define('Conta', {
    extend:'Ext.data.Model',
    fields: [
           {name:'id',type:'int'},
           {name:'descricao',type:'string'},
           {name:'prazo',type:'int'}
       ]
   });


    Ext.define('ContaStore',{
    extend:'Ext.data.Store',
    model:'Conta',
    storeId:'contaStore',
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:19,
    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/conta.php?action=insert',
            read:'php/ajax/conta.php?action=listConta',
            update:'php/ajax/conta.php?action=update',
            destroy:''
        },

        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },

        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
});