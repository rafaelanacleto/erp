/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.require('Ext.data.*');

Ext.define('MovEntradaModel', {
    extend:'Ext.data.Model',
    fields: [
           {name:'idProduto',type:'int'}, 
           {name:'referenciaProduto',type:'string'},
           {name:'descricaoProduto',type:'string'},
           {name:'numNota',type:'string'},
           {name:'quantidade',type:'int'},
           {name:'valor',type:'float'},
           {name:'idFornecedor',type:'string'},
           {name:'descricaoFornecedor',type:'string'},           
       ]}
   );
   
   
Ext.define('MovEntradaStore',{
    extend:'Ext.data.Store',
    model:'MovEntradaModel',
     storeId:'movEntradaStore',
    autoLoad: false,
    proxy:{
        type: 'ajax',
         api: {
            create:'php/ajax/movimento.php?action=createMovimentoEntrada',
            read:'',
            update:'php/ajax/fornecedor.php?action=createMovimentoEntrada',
            destroy:''
        },
        reader: {
            type: 'json',
            root: 'items'
            },
        writer: {
            type:'json',
            root: 'items',
            encode:true
            }
        },
    data:{}
});


