

Ext.require('Ext.data.*');

Ext.define('Item', {
    extend:'Ext.data.Model',
    fields: [
           {name:'id',type:'int'},
           {name:'movimento_id',type:'int'},
           {name:'estoque_id',type:'int'},
           {name:'valor_unit',type:'float'},
           {name:'desconto_concedido',type:'int'},
           {name:'quantidade',type:'int'},
           {name:'custo',type:'float'},
           {name:'valor_total',type:'float'},
           {name:'update',type:'date'},
       ]
   });

   Ext.define('ItemStore',{
    extend:'Ext.data.Store',
    model:'Item',
    storeId:'itemStore',
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:19,

    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/item.php?action=insert',
            read:'php/ajax/item.php?action=listMovimento',
            update:'php/ajax/item.php?action=update',
            destroy:''
        },

        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },

        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
});



