


Ext.require('Ext.data.*');

Ext.define('Fornecedor', {
    extend:'Ext.data.Model',
    fields: [
           {name:'id',type:'int'},
           {name:'descricao',type:'string'},
           {name:'fone',type:'String'},
           {name:'email',type:'String'},
           {name:'localizacao',type:'String'},
           {name:'ativo',type:'String'}
       ]
   });


    Ext.define('FornecedorStore',{
    extend:'Ext.data.Store',
    model:'Fornecedor',
    storeId:'fornecedorStore',
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:19,
    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/fornecedor.php?action=insert',
            read:'php/ajax/fornecedor.php?action=listFornecedor',
            update:'php/ajax/fornecedor.php?action=update',
            destroy:''
        },

        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },

        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
});