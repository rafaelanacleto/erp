/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.require('Ext.data.*');

Ext.define('MovVendaModel', {
    extend:'Ext.data.Model',
    fields: [
           {name:'idProduto',type:'int'},
           {name:'referenciaProduto',type:'string'},
           {name:'descricaoProduto',type:'string'},           
           {name:'quantidade',type:'int'},
           {name:'valor',type:'float'},
           {name:'total_item',type:'float'},
           {name:'idFornecedor',type:'string'},
           {name:'descricaoFornecedor',type:'string'},
       ]}
   );


Ext.define('MovVendaStore',{
    extend:'Ext.data.Store',
    model:'MovVendaModel',
     storeId:'movVendaStore',
    autoLoad: false,
    proxy:{
        type: 'ajax',
         api: {
            create:'php/ajax/movimento.php?action=createMovimentoVenda',
            read:'',
            update:'php/ajax/fornecedor.php?action=createMovimentoVenda',
            destroy:''
        },
        reader: {
            type: 'json',
            root: 'items'
            },
        writer: {
            type:'json',
            root: 'items',
            encode:true
            }
        },
    data:{}
});


