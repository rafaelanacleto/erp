
Ext.require('Ext.data.*');

Ext.define('Produto', {
    extend:'Ext.data.Model',
    fields: [
           {name:'id',type:'int'},
           {name:'fornecedor_id',type:'int'},
           {name:'descricao',type:'String'},           
           {name:'preco_normal',type:'float'},
           {name:'preco_promo',type:'float'},
           {name:'desconto_maximo',type:'int'},
           {name:'categoria_produto_id',type:'int'},
           {name:'custo',type:'float'},
           {name:'ativo',type:'string'},          
           {name:'referencia',type:'string'},
           {name:'fornecedor_descricao',type:'string'},
           {name:'categoria_produto_descricao',type:'string'},
           {name:'qtd_loja',type:'int'},
           {name:'qtd_total',type:'int'},
       ]   
   });

   Ext.define('ProdutoStore',{
    extend:'Ext.data.Store',
    model:'Produto',
    storeId:'produtoStore',
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:17,
   
    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/produto.php?action=insert',
            read:'php/ajax/produto.php?action=listProduto',
            update:'php/ajax/produto.php?action=update',
            destroy:''
        },

        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },

        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
});
