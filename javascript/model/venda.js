
Ext.require('Ext.data.*');

Ext.define('Venda', {
    extend:'Ext.data.Model',
    fields: [
           {name:'id',type:'int'},
           {name:'tipo_movimento_id',type:'int'},
           {name:'destino_estoque_id',type:'int'},
           {name:'destino_estoque_desc',type:'string'},
           {name:'produto_id',type:'int'},
           {name:'produto_referencia',type:'string'},
           {name:'produto_descricao',type:'string'},
           {name:'fornecedor_desc',type:'string'},
           {name:'nota',type:'string'},
           {name:'usuario_id',type:'int'},
           {name:'usuario_nome',type:'string'},
           {name:'up_date',type:'date'},
           {name:'entregue',type:'string'},
           {name:'origem_estoque_id',type:'int'},
           {name:'quantidade',type:'int'},
           {name:'quantidade_itens',type:'int'},
           {name:'valor_unit',type:'float'},
           {name:'custo',type:'float'},
           {name:'desconto_concedido',type:'int'},
           {name:'desconto_maximo',type:'int'},
           {name:'preco_final',type:'float'},
           {name:'preco_normal',type:'float'},
           {name:'preco_promo',type:'float'},
           {name:'valor_total',type:'float'},
           {name:'valor_total_item',type:'float'},
       ]
   });

   Ext.define('VendaStore',{
    extend:'Ext.data.Store',
    model:'Venda',
    storeId:'vendaStore',
    autoLoad: false,
    remoteSort:true,
    remoteFilter:true,
    pageSize:19,

    proxy: {
        type:'ajax',
        api: {
            create:'php/ajax/venda.php?action=insert',
            read:'php/ajax/venda.php?action=listVenda',
            update:'php/ajax/venda.php?action=update',
            destroy:''
        },

        reader: {
            type:'json',
            root:'rows',
            totalProperty:'totalCount'
        },

        writer: {
            type:'json',
            root:'rows',
            writeAllFields:true,
            encode: true,
            allowSingle: true
        }
    }
});

