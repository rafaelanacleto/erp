<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 * Adicionar controle de execução do código de acordo com a modalidade de acesso.
 */


if(!isset($_GET['action'])) {
    echo '<script type="text/javascript" src="javascript/view/logon.js"></script>
        <script type="text/javascript" src="javascript/model/loja.js"></script>';
    return;
} else {
    //tratar qual menu vai ser visivel
    if ($_SESSION['acesso'] == 1) {
        echo '<script type="text/javascript" src="javascript/view/menu.js"></script>';
    }

    if ($_SESSION['acesso'] == 2) {
         echo '<script type="text/javascript" src="javascript/view/menuSupervisor.js"></script>';
    }

    if ($_SESSION['acesso'] == 3) {
         echo '<script type="text/javascript" src="javascript/view/menuGerLoja.js"></script>';
    }

    if ($_SESSION['acesso'] == 4) {
         echo '<script type="text/javascript" src="javascript/view/menuVendedor.js"></script>';
    }

}

if($_GET['action'] == 'Fornecedores') {
    echo '<script type="text/javascript" src="javascript/view/fornecedor.js"></script>
    <script type="text/javascript" src="javascript/model/fornecedor.js"></script>';
}

if($_GET['action'] == 'Produto') {
    echo '<script type="text/javascript" src="javascript/view/produto.js"></script>
    <script type="text/javascript" src="javascript/view/transferProduct.js"></script>
    <script type="text/javascript" src="javascript/model/produto.js"></script>
    <script type="text/javascript" src="javascript/model/estoque.js"></script>
    <script type="text/javascript" src="javascript/model/fornecedor.js"></script>
    <script type="text/javascript" src="javascript/library/fieldMoney.js"></script>
    <script type="text/javascript" src="javascript/model/catProduto.js"></script>
    <script type="text/javascript" src="javascript/model/entrada.js"></script>
    <script type="text/javascript" src="javascript/model/loja.js"></script>';
}

if($_GET['action'] == 'Entrada') {
    echo '<script type="text/javascript" src="javascript/view/entrada.js"></script>
    <script type="text/javascript" src="javascript/model/produto.js"></script>
    <script type="text/javascript" src="javascript/model/fornecedor.js"></script>
    <script type="text/javascript" src="javascript/model/loja.js"></script>
    <script type="text/javascript" src="javascript/model/entrada.js"></script>';
}

if($_GET['action'] == 'Venda') {
    echo '<script type="text/javascript" src="javascript/view/venda.js"></script>
    <script type="text/javascript" src="javascript/model/produto.js"></script>
    <script type="text/javascript" src="javascript/model/cliente.js"></script>
    <script type="text/javascript" src="javascript/model/venda.js"></script>
    <script type="text/javascript" src="javascript/view/negociacao.js"></script>
    <script type="text/javascript" src="javascript/model/conta.js"></script>
    <script type="text/javascript" src="javascript/library/fieldMoney.js"></script>';
}

if($_GET['action'] == 'Clientes') {
    echo '<script type="text/javascript" src="javascript/view/cliente.js"></script>
    <script type="text/javascript" src="javascript/model/cliente.js"></script>';
}

if($_GET['action'] == 'CatProduto') {
    echo '<script type="text/javascript" src="javascript/view/catProduto.js"></script>
    <script type="text/javascript" src="javascript/model/catProduto.js"></script>';
}

if($_GET['action'] == 'Loja') {
    echo '<script type="text/javascript" src="javascript/view/loja.js"></script>
    <script type="text/javascript" src="javascript/model/loja.js"></script>';
}

if($_GET['action'] == 'SitCaixa') {
    echo '<script type="text/javascript" src="javascript/view/carteira.js"></script>
    <script type="text/javascript" src="javascript/model/carteira.js"></script>';
}

if($_GET['action'] == 'GerCaixa') {
    echo '<script type="text/javascript" src="javascript/view/caixa.js"></script>
        <script type="text/javascript" src="javascript/library/fieldMoney.js"></script>
    <script type="text/javascript" src="javascript/model/caixa.js"></script>';
}



?>