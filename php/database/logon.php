<?php
require_once('../database/database.php');

class Logon {

    //Attributes
    private $id = 0;
    private $pwd = '';
    private $user_id = '';
    private $nome = '';
    private $tempPwd = 'N';
    private $error_message = '';
    private $permissao = 0;

    //Constructor
    public function Logon() {
       $this->pdo_conn=getPdoConnection();
    }

    //Acessors
    public function getNome() {
        return $this->nome;
    }

    public function setId($id) {
        $this->id = intVal($id);
        $_SESSION['user_id'] = $this->id;
    }

    public function setUserId($userId) {
        $this->user_id = $userId; //Username
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function setPwd($pwd) {
        $this->pwd = md5($pwd);
    }

    public function setPermissao($permissao) {
        $this->permissao = $permissao;
    }

    public function getPermissao() {
        return $this->permissao;
    }

    public function setNome($nome) {
        $this->nome = $nome;
        $_SESSION['nome_usuario'] = $this->nome;
    }

    public function getId() {
        return $this->id;
    }

    public function getPwd() {
        return $this->pwd;
    }

    public function setTempPwd($temp) {
        $this->tempPwd = $temp;
    }
    
    public function setErrorMessage($message) {
        $this->error_message = $message;
    }
    
    public function getErrorMessage() {
        return $this->error_message;
    }

    public function searchUser() {
        $statment = "SELECT id FROM usuario WHERE user_id=?";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$this->getUserId(),PDO::PARAM_STR);
        $result = $stmt->execute();
        $return = $stmt->fetchAll(PDO::FETCH_NUM);
        if (isset($return[0][0])) {
           $this->setId($return[0][0]);
           return true;
        } else { 
            $this->setErrorMessage('Usuário não existe');
            return false;
        }
    }

    public function matchPassword() {
        $statment = "SELECT nome,senha_provisoria,permissoes_id FROM usuario,usuario_has_permissao WHERE id=? AND senha=? AND usuario_has_permissao.usuario_id=usuario.id";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
        $stmt->bindParam(2,$this->getPwd(),PDO::PARAM_STR);
        $result = $stmt->execute();
        $return = $stmt->fetchAll(PDO::FETCH_NUM);        
       
        if (isset($return[0][0])) {
           $this->setNome($return[0][0]);
           $this->setTempPwd($return[0][1]);
           $this->setPermissao($return[0][2]);
            return true;
        } else {
            $this->setErrorMessage('Senha incorreta');
            return false;
        }
    }

    public function IsTempPassword() {
        if ($this->tempPwd == 'S') {
            return true;
        }else{
            return false;
        }
    }

    public function authenticate() {
       
        if ($this->searchUser() === true) {            
            if ($this->matchPassword() === true) {
                return 'OK';                
            }else {
                return $this->getErrorMessage();                
            }            
        }else {
            return $this->getErrorMessage();
        }
    }
}


?>
