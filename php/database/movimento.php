<?php
class Movimento {


    //Attributes
    private $id = 0;
    private $tipo_movimento = 0;
    private $cliente_id = 0;
    private $destino_loja_id = 0;
    private $valor = 0;
    private $usuario_id = 0;
    private $update = '';
    private $entregue = '';
    private $origem_loja_id = 0;
    private $nota = '';

    //Constructor
    public function Movimento() {
         $this->pdo_conn=getPdoConnection();
    }

    //Acessors
    public function setId($id) {
        $this->id=$id;
    }

    public function getId() {
        return $this->id;
    }

    public function setTipoMovimento($tipo_movimento) {
        $this->tipo_movimento=$tipo_movimento;
    }

    public function setClienteId($cliente_id) {
        $this->cliente_id=intVal($cliente_id);
    }

    public function setDestinoLojaId($destino_loja_id) {
        $this->destino_loja_id=$destino_loja_id;
    }

    public function setValor($valor) {
        $this->valor=$valor;
    }

    public function setUsuarioId($usuario_id) {
        $this->usuario_id =  intval($usuario_id);
    }

    public function setUpdate($update) {
        $this->update=$update;
    }

    public function setOrigemLojaId($origem_loja_id) {
        $this->origem_loja_id=  intval($origem_loja_id);
    }

    public function setNota($nota) {
        $this->nota=$nota;
    }

    public function setEntregue($entregue) {
        $this->entregue=$entregue;
    }

     public function getEntregue() {
        return $this->entregue;
    }
    
    public function geTipoMovimento() {
        return $this->tipo_movimento;
    }
    
    public function getClienteId(){
        return $this->cliente_id;
    }
    
    public function getDestinoLojaId(){
         return $this->destino_loja_id;
     }
    
    public function getValor(){
         return $this->valor;
     }
    
    public function getUsuarioId(){
         return $this->usuario_id;
     }
    
    public function getUpdate(){
        return $this->update;
    }
    
    public function getOrigemLojaId(){
        return $this->origem_loja_id;
    }
    
    public function getNota(){
        return $this->nota;
    }
    
    //Methods

    public function insert() {
        $statment = "INSERT INTO movimento VALUES ";
        $statment .= "(NULL,?,?,?,CAST(? AS DECIMAL(6,2)),?,NOW(),?,?,?)";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$this->tipo_movimento,PDO::PARAM_INT);
        $stmt->bindParam(2,$this->cliente_id,PDO::PARAM_INT);
        $stmt->bindParam(3,$this->destino_loja_id,PDO::PARAM_INT);
        $stmt->bindParam(4,$this->valor,PDO::PARAM_STR);
        $stmt->bindParam(5,$this->usuario_id,PDO::PARAM_INT);
        $stmt->bindParam(6,$this->entregue,PDO::PARAM_STR);
        $stmt->bindParam(7,$this->origem_loja_id,PDO::PARAM_INT);
        $stmt->bindParam(8,$this->nota,PDO::PARAM_STR);

        $this->pdo_conn->beginTransaction();
        $result = $stmt->execute();
        $lastId =  $this->pdo_conn->lastInsertId();
        $this->pdo_conn->commit();
        $this->setId($lastId);
        return $lastId;
    }
    
    

    public function getMovimento() {

       
    }
}   

?>
