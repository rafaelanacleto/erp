<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

define ("DEV_SCHEMA" , "lojas_charme");
define ("DEV_DB_ADDRESS", "localhost");
define ("DEV_DB_USER","root");
define ("DEV_DB_PWD", "root");
define ("DEV_logon_redir", "index.php?action=Login");

define ("PRO_SCHEMA" , "h24ubasa_estoque");
define ("PRO_DB_ADDRESS", "localhost");
define ("PRO_DB_USER","h24ubasa");
define ("PRO_DB_PWD", "Qdh7AbaoRZ");
define ("PRO_logon_redir", "index.php?action=Login");

function db_connect() {

    $connection = null;
    $connection = mysql_connect(PRO_DB_ADDRESS,PRO_DB_USER,PRO_DB_PWD);
    if ($connection) {
        mysql_select_db(PRO_SCHEMA,$connection);
    } else {
        $connection = mysql_connect(DEV_DB_ADDRESS,DEV_DB_USERUSER,DEV_DB_PWD);
        if ($connection) {
            mysql_select_db(DEV_SCHEMA,$connection);
        }else{
            die("Erro de conexão, entre em contato com o suporte.");
        }
        
    }
    return $connection;
}

function user_authenticate($user,$pwd) {

    // Verifica se o form foi preenchido
    if (($user == '') || ($pwd == '')) {

        $_SESSION['error_message'] = 'Usuário ou senha em branco.';
        redir(logon_redir);
        return;
    }

    $id = null;

    $connection =  db_connect();

    // Testa existência de usuário
    $query_1 = "SELECT id FROM usuario WHERE username='".$user."'";

    $result_1 = mysql_query($query_1, $connection);

    if (mysql_num_rows($result_1) > 0) {
        $array = mysql_fetch_assoc($result_1);
        $id = $array['id'];

    } else {
        //Redireciona para tela de login com usuário inexistente.
        $_SESSION['error_message'] = 'Usuário inexistente';
        redir(logon_redir);
        return;
    }

    // Testa a validade da senha do usuário
    $query_2 = "SELECT nome,tipo,email,fone FROM usuario WHERE id=$id AND pwd='".$pwd."'";

    $result_2 = mysql_query($query_2, $connection);

    if (mysql_num_rows($result_2) > 0) { // Se a senha for válida preenche dados da session
        $array = mysql_fetch_assoc($result_2);
        $_SESSION['logado'] = true;
        $_SESSION['action']  = 'Login';
        $_SESSION['user_nome']  = $array['nome'];
        $_SESSION['user_tipo'] = $array['tipo'];
        $_SESSION['user_email'] = $array['email'];
        $_SESSION['user_fone'] = $array['fone'];
        $_SESSION['error_message'] = '';

        redir("index.php?action=Init");

    } else {
        //Redireciona para tela de login com senha incorreta.
        $_SESSION['error_message'] = 'Senha inválida';
        redir(logon_redir);
        return;
    }

}

function trataSQL($valor,$tipo,$isNullable) {

    if( $tipo === 'S') {
        $valor = addslashes(trim($valor));

        if($valor == '' && $isNullable) {
            $valor = 'null';
        }
        else {
            $valor = "'" .$valor. "'";
        }
    }
    elseif ( $tipo=== 'N') {
        $valor = doubleval($valor);

        if($valor == 0 && $isNullable) {
            $valor = 'null';
        }
    }

    return $valor;
}

function sql2json($query, $isSingle = false) {
    $data_sql = $query;
    $json_str = ""; //Init the JSON string.

    if(!$isSingle)
        $json_str .= "[\n";

    if($total = mysql_num_rows($data_sql)) { //See if there is anything in the query

        $row_count = 0;
        while($data = mysql_fetch_assoc($data_sql)) {
            if(count($data) > 1) $json_str .= "{\n";

            $count = 0;
            foreach($data as $key => $value) {
                //If it is an associative array we want it in the format of "key":"value"
                if(count($data) > 1) $json_str .= "\"$key\":\"$value\"";
                else $json_str .= "\"$value\"";

                //Make sure that the last item don't have a ',' (comma)
                $count++;
                if($count < count($data)) $json_str .= ",\n";
            }
            $row_count++;
            if(count($data) > 1) $json_str .= "}\n";

            //Make sure that the last item don't have a ',' (comma)
            if($row_count < $total) $json_str .= ",\n";
        }
    }

    if(!$isSingle)
        $json_str .= "]\n";

    //Replace the '\n's - make it faster - but at the price of bad redability.
    $json_str = str_replace("\n","",$json_str); //Comment this out when you are debugging the script

    //Finally, output the data
    return $json_str;
}

function getPdoConnection() {
  //  $pdo_conf_string="mysql:host=".PRO_DB_ADDRESS.";dbname=".PRO_SCHEMA;
    //new PDO("mysql:host=$dbHost;dbname=$dbName;charset=utf8", $dbUser, $dbPass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
             $pdo_conf_string="mysql:host=".DEV_DB_ADDRESS.";dbname=".DEV_SCHEMA;
             $pdo = new PDO($pdo_conf_string,DEV_DB_USER,DEV_DB_PWD,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
             $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   return $pdo;

}

?>