<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('../database/database.php');

class Fornecedor {

    //Attributes
    private $id = 0;
    private $descricao = '';
    private $fone = '';
    private $email = '';
    private $localizacao = '';
    private $ativo = '';
    private $pdo_conn = '';
    private $listDefaultLenght = 17;

    //Constructor
    public function Fornecedor() {

       $this->pdo_conn=getPdoConnection();

    }

    //Acessors
    public function setId($id) {
        $this->id = $id;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setFone($fone) {
        $this->fone = $fone;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setLocalizacao($localizacao) {
        $this->localizacao= $localizacao;
    }

    public function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    public function getId() {
        return $this->id;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getFone() {
        return $this->fone;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getLocalizacao() {
        return $this->localizacao;
    }

    public function getAtivo() {
        return $this->ativo;
    }

    public function get() {

    }

    public function getList($start, $limit)  {

        $start = intval($start);
	$limit = intval($limit);

        $sort = 'descricao';
        $dir = 'ASC';

        $statment = "SELECT fornecedor.id,fornecedor.descricao";
        $statment .= ",fornecedor.fone,fornecedor.email,fornecedor.localizacao,";
        $statment .= "fornecedor.ativo FROM fornecedor ORDER BY ";
        $statment .= "$sort $dir LIMIT ?,?";

        $stmt = $this->pdo_conn->prepare($statment);

        $stmt->bindParam(1,$start,PDO::PARAM_INT);
        $stmt->bindParam(2,$limit,PDO::PARAM_INT);

        $result = $stmt->execute();

        return  $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function insert() {
        $statment = "INSERT INTO fornecedor VALUES(null,?,?,?,?,?)";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$this->getDescricao(),PDO::PARAM_STR);
        $stmt->bindParam(2,$this->getFone(),PDO::PARAM_STR);
        $stmt->bindParam(3,$this->getEmail(),PDO::PARAM_STR);
        $stmt->bindParam(4,$this->getLocalizacao(),PDO::PARAM_STR);
        $stmt->bindParam(5,$this->getAtivo(),PDO::PARAM_STR);
        $result = $stmt->execute();
        return $result;


    }

    public function update() {
        $statement = "UPDATE fornecedor SET ";
        $statement .= "descricao=?,fone=?,email=?,localizacao=?,ativo=? ";
        $statement .= " WHERE id=?";

        $stmt = $this->pdo_conn->prepare($statement);
        $stmt->bindParam(1,$this->getDescricao(),PDO::PARAM_STR);
        $stmt->bindParam(2,$this->getFone(),PDO::PARAM_STR);
        $stmt->bindParam(3,$this->getEmail(),PDO::PARAM_STR);
        $stmt->bindParam(4,$this->getLocalizacao(),PDO::PARAM_STR);
        $stmt->bindParam(5,$this->getAtivo(),PDO::PARAM_STR);
        $stmt->bindParam(6,$this->getId(),PDO::PARAM_INT);
        $result = $stmt->execute();
        return $result;


    }

    public function search($start, $limit,$property,$value) {

         $value = '%'.$value.'%';

         $statment = "SELECT fornecedor.id,fornecedor.descricao";
         $statment .= ",fornecedor.fone,fornecedor.email,fornecedor.localizacao,";
         $statment .= "fornecedor.ativo FROM fornecedor ";
         $statment .= "WHERE descricao LIKE ? ORDER BY descricao ASC";
         $stmt = $this->pdo_conn->prepare($statment);
         $stmt->bindParam(1,$value,PDO::PARAM_STR);
         $result = $stmt->execute();
         return  $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function count() {

         $statment = "SELECT COUNT(id) from fornecedor";

          $stmt = $this->pdo_conn->prepare($statment);

         $result = $stmt->execute();

         $return = $stmt->fetchAll(PDO::FETCH_NUM);

         return  $return[0][0];

    }



}


?>
