<?php

require_once('../database/produto.php');

if (isset($_GET['cod'])) {
    
$objProduto = new Produto();
$objProduto->setId($_GET['cod']);
$objProduto->get();



$lg = new LabelGenerator();
$lg->setCodeString($_GET['cod']);
$lg->setFormattedPrice($objProduto->getPreco_normal());
$lg->constructCode();
$lg->openBody();
$lg->renderContainer(1);
$lg->closeBody();

}



class LabelGenerator {
    
    private $codeString;
    private $formattedCode;
    private $formattedPrice;
    private $narrow = 0.2;
    private $nmultiplier;
    private $valueList = Array();
    private $arrayBarras = Array();
    private $copias = 0;
    private $codeSequence = '';  // Sequencia de canvas gerados
    private $symbols = 
        Array(
            0=>Array('n','n','W','W','n'),
            1=>Array('W','n','n','n','W'),
            2=>Array('n','W','n','n','W'),
            3=>Array('W','W','n','n','n'),
            4=>Array('n','n','W','n','W'),
            5=>Array('W','n','W','n','n'),
            6=>Array('n','W','W','n','n'),
            7=>Array('n','n','n','W','W'),
            8=>Array('W','n','n','W','n'),
            9=>Array('n','W','n','W','n'),
            10=>Array('n','n','n','n'),
            11=>Array('W','n','n'),
            );
    
    public function LabelGenerator() {
        
    }   
        
    public function openBody() {
        
        echo '<!DOCTYPE html><html><head>
        <link href="../../styles/codeBarPrint.css" rel="stylesheet" type="text/css" media="screen,print" />

        <script type="text/javascript">
            function fillCanvas() {
                var arrCodeDivs = document.getElementsByClassName("codeDiv");                
                for (var idx=0; idx < arrCodeDivs.length; idx++) {
                    var canvasList = arrCodeDivs[idx].childNodes;
                    for (index=0; index < canvasList.length; index++) {
                        var contexto = canvasList[index].getContext(\'2d\');
                        if (index%2 == 0) {                    
                            contexto.fillStyle = "rgb(0,0,0)";
                            contexto.fillRect (0, 0, 2000, 2000);
                        } else {
                            contexto.fillStyle = "rgb(255,255,255)";
                            contexto.fillRect (0, 0,1000,1000);
                        }
                    }
                }
            }
        </script>
        </head>
        <body>';
    }
    
    public function renderContainer($copias) {
        
        for ($i=0; $i<$copias; $i++) {
            
            echo'<div id="conjunto" class="conjunto">            
                <div id="sepSup" class="sepSup"></div>
                <div id="labelDiv" class="labelDiv">
                    <div id="codeDiv" class="codeDiv">';
                    echo $this->codeSequence;
                    echo '</div>
                    <div id="numDiv" class="numDiv">';
                    echo $this->formattedPrice;
                    echo '<br>';
                    echo $this->formattedCode;
                    echo'</div>
                </div>
                <div id="sepCen" class="sepCen"></div>
                <div id="labelDiv" class="labelDiv">
                    <div id="codeDiv" class="codeDiv">';
                    echo $this->codeSequence;
                    echo '</div>
                    <div id="numDiv" class="numDiv">';
                    echo $this->formattedPrice;
                    echo '<br>';
                    echo $this->formattedCode;
                    echo'</div>
                </div>
                <div id="sepInf" class="sepInf"></div>        
            </div>';        
        }
        //Inserir quebra de página exceto na última
    }
    
    public function closeBody() {
        echo '<script>fillCanvas();</script></body></html>';        
    }
    
    public function constructCode() {
        
        $this->arrayBarras = array_merge($this->arrayBarras, $this->symbols[10]); //adds the header to the code
        //This loop adds to arrayBarras the codes for each value in valueList
        for ($i = 0; $i< (sizeof($this->valueList) -1); $i+=2) {
            for ($j = 0;$j<5;$j++) {
                array_push($this->arrayBarras, $this->symbols[$this->valueList[$i]][$j]);
                array_push($this->arrayBarras, $this->symbols[$this->valueList[$i+1]][$j]);
            }
        }
        $this->arrayBarras = array_merge($this->arrayBarras, $this->symbols[11]); //adds the trailler to the code        
        foreach ($this->arrayBarras as $value) {
            if ($value == 'n') {
                $this->codeSequence .= '<canvas class="narrow"></canvas>';
            } else {
                $this->codeSequence .= '<canvas class="wide"></canvas>';
            }           
        }
    }
    
    public function setCodeString($code) {      
        if ( strlen($code) < 6) {                                   
             $code = str_pad($code, 6, "0", STR_PAD_LEFT);
             $this->setFormattedCode($code);
             $this->setValueList(str_split($code,1));       
        }else {
             $this->setValueList(str_split($code,1));            
        }        
    }
    
    private function setValueList($list) {        
        $this->valueList=$list;
    }
    
    private function getBarcodeWidth($code) {        
        $totalWidth = 0;
        $wide = 2.5*$this->narrow;        
        $totalWidth += (20*$this->narrow);        
        $arrayOccurences = array_count_values($code);
        $totalWidth += ($arrayOccurences['n']*$this->narrow);
        $totalWidth += ($arrayOccurences['W']*$wide);
        return $totalWidth;
    }

    private function setFormattedCode($code) {
        $this->formattedCode = $code;
    }

    public function setFormattedPrice($price) {
        $strPrice = (string)$price;
        $strPrice = str_replace('.',',',$strPrice);
        $strPrice = "R$".$strPrice;
        $this->formattedPrice = $strPrice;
    }
    
    
}

?>


    
    
