<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('../database/database.php');

class Estoque {

    //Attributes
    private $id = 0;
    private $produto_id = 0;
    private $loja_id = 0;
    private $quantidade = 0;
    private $update = 0;
    private $baixa = 0;

    //Constructor
    public function Estoque() {

       $this->pdo_conn=getPdoConnection();

    }

    //Acessors
    public function setId($id) {
        $this->id = $id;
    }

    public function setProduto_id($produto_id) {
        $this->produto_id = $produto_id;
    }

    public function setLoja_id($loja_id) {
        $this->loja_id = $loja_id;
    }

    public function setQuantidade($quantidade) {
        $this->quantidade=$quantidade;
    }

    public function setBaixa($baixa) {
        $this->baixa=$baixa;
    }

    public function getId() {
        return $this->id;
    }

    public function getProduto_id() {
        return $this->produto_id;
    }

    public function getLoja_id() {
        return $this->loja_ido;
    }

    public function getQuantidade() {

        $statment = "SELECT SUM(quantidade) AS quantidade FROM estoque WHERE produto_id=?";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$this->produto_id,PDO::PARAM_INT);
        $result = $stmt->execute();
        $qtd = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $qtd = $qtd[0];

        if ($qtd['quantidade']==null) { //Não há produtos com o id referenciado.
           $qtd['quantidade']=0;
        }

        return $qtd['quantidade'];
       
    }

    public function getUpdate() {
        return $this->update;
    }

    public function getList($start, $limit)  {

    }

    public function insert() {
        $statment = "INSERT INTO estoque VALUES (NULL,?,?,?,NOW())";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$this->produto_id,PDO::PARAM_INT);
        $stmt->bindParam(2,$this->loja_id,PDO::PARAM_INT);
        $stmt->bindParam(3,$this->quantidade,PDO::PARAM_INT);
        $this->pdo_conn->beginTransaction();
        $result = $stmt->execute();
        $lastId =  $this->pdo_conn->lastInsertId();
        $this->pdo_conn->commit();
        return $lastId;
       
    }

    public function update() {
        $statment = "UPDATE estoque SET quantidade=quantidade+? WHERE id=? ";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$this->quantidade,PDO::PARAM_INT);
        $stmt->bindParam(2,$this->id,PDO::PARAM_INT);
        $result = $stmt->execute();
        return $result;

    }
    
    public function baixa() {
        $statment = "UPDATE estoque SET quantidade=quantidade-?,estoque.update=NOW() WHERE produto_id=? AND loja_id=?";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$this->baixa,PDO::PARAM_INT);
        $stmt->bindParam(2,$this->produto_id,PDO::PARAM_INT);
        $stmt->bindParam(3,$this->loja_id,PDO::PARAM_INT);
        $result = $stmt->execute();
        return $result;

    }

    public function sync() {    // decide se é insert ou update

        $statment = "SELECT id FROM estoque WHERE (produto_id=? AND loja_id=?)";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$this->produto_id,PDO::PARAM_INT);
        $stmt->bindParam(2,$this->loja_id,PDO::PARAM_INT);
        $result = $stmt->execute();
        $linha = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($linha != null) {
            $this->setId($linha[0]['id']);
            $this->update();
        } else {
            $this->setId($this->insert());
        }
        return  $this->getId();
      
    }

    public function search($start, $limit,$property,$value) {

         $value = '%'.$value.'%';

         $statment = "SELECT loja.id,loja.descricao,loja.localizacao";
         $statment .= " FROM loja WHERE descricao LIKE ? ORDER BY descricao ASC";
         $stmt = $this->pdo_conn->prepare($statment);
         $stmt->bindParam(1,$value,PDO::PARAM_STR);
         $result = $stmt->execute();
         return  $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function count() {
         $statment = "SELECT COUNT(id) from loja";
         $stmt = $this->pdo_conn->prepare($statment);
         $result = $stmt->execute();
         $return = $stmt->fetchAll(PDO::FETCH_NUM);
         return  $return[0][0];

    }

    public function retrieveId() {

        $statment = "SELECT id FROM estoque WHERE produto_id=? AND loja_id=?";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$this->produto_id,PDO::PARAM_INT);
        $stmt->bindParam(2,$this->loja_id,PDO::PARAM_INT);
        $result = $stmt->execute();
        $return = $stmt->fetchAll(PDO::FETCH_NUM);
        $this->setId($return[0][0]);
        return $return[0][0];


    }


    public function transfer($prod_id,$origem_id,$destino_id) {
        
    }

    public function getProductPosition($prod_id) {
        $prod_id = intval($prod_id);

        $statement = "SELECT id,produto_id,loja_id,quantidade,`update` FROM estoque WHERE produto_id=?";
        $stmt = $this->pdo_conn->prepare($statement);
        $stmt->bindParam(1,$prod_id,PDO::PARAM_INT);
        $result = $stmt->execute();
        return  $stmt->fetchAll(PDO::FETCH_ASSOC);


    }

}


?>
