<?php

/*
 * Opera abertua,fechamento e sangrias no caixa movimentando as tabelas:
 * Carteira,movimento
 */

require_once('../database/database.php');

class Caixa {
   

    //Constructor
    public function Caixa() {
       $this->pdo_conn=getPdoConnection();
    }

    public function listAll($start, $limit,$property,$direction,$loja)  { //Lista movimentos de sangria, abertura e fechamento

        $start = intval($start);
        $limit = intval($limit);

        $statment = "SELECT id,tipo_movimento_id,valor,usuario_id,up_date,nota AS historico,";
        $statment .= "(SELECT usuario.nome from usuario where id=usuario_id) as usuario,";
        $statment .= "(SELECT descricao from tipo_movimento where tipo_movimento.id=tipo_movimento_id) AS operacao";
        $statment .= " FROM movimento where tipo_movimento_id IN (8,9,10,11) AND destino_loja_id=? ORDER BY ";
        if ($property) {
            $statment .= "$property $direction LIMIT ?,?";
        }else{
            $statment .= "up_date DESC LIMIT ?,?";
        }

        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$loja,PDO::PARAM_INT);
        $stmt->bindParam(2,$start,PDO::PARAM_INT);
        $stmt->bindParam(3,$limit,PDO::PARAM_INT);
        $result = $stmt->execute();
        return  $stmt->fetchAll(PDO::FETCH_ASSOC);
    }    

    public function getLastOpen() { //Quando foi a ultima abertura e quanto tinha na gaveta
        /*
         * SELECT up_date AS last_open_time,id AS last_open_id, valor AS troco_inicial
FROM movimento where tipo_movimento_id=9 AND destino_loja_id=2
ORDER BY up_date DESC limit 1
         */
        $statment = "SELECT up_date AS last_open_time,id AS last_open_id, valor AS troco_inicial ";
        $statment .= "FROM movimento where tipo_movimento_id=9 AND destino_loja_id=2 ORDER BY up_date DESC limit 1";
        $stmt = $this->pdo_conn->prepare($statment);
        $result = $stmt->execute();
        return  $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getLastCloseInfo() { //Quanto sobrou na gaveta e quando foi o ultimo fechamento
        $statment = "SELECT up_date AS last_close_time,id AS last_close_id, valor AS troco_final ";
        $statment .= "FROM movimento where tipo_movimento_id=10 AND destino_loja_id=2 ORDER BY up_date DESC limit 1";
        $stmt = $this->pdo_conn->prepare($statment);
        $result = $stmt->execute();
        return  $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getLastOpenSangrias() { //Sangrias ocorridas desde a ultima abertura
        $statment = "SELECT cast(sum(valor) AS decimal(6,2)) AS sangrias FROM carteira WHERE conta_id=1 AND movimento_id ";
        $statment .="IN (SELECT id FROM movimento WHERE tipo_movimento_id=8 AND up_date >";
        $statment .="(SELECT max(up_date) AS last_open FROM movimento where tipo_movimento_id=9 AND destino_loja_id=2))";
        $stmt = $this->pdo_conn->prepare($statment);
        $result = $stmt->execute();
        return  $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getLastOpenEntradas() { //Entradas em dinheiro desde a ultima abertura
        $statment = "SELECT sum(valor) AS entradas FROM carteira WHERE conta_id=1 AND movimento_id ";
        $statment .="IN (SELECT id FROM movimento WHERE tipo_movimento_id=2 AND up_date >";
        $statment .="(SELECT max(up_date) AS last_open FROM movimento where tipo_movimento_id = 9))";
        $stmt = $this->pdo_conn->prepare($statment);
        $result = $stmt->execute();        
        return  $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function sangria($loja_id,$valor,$user_id,$historico) {
        //Registra um movimento código 8 com o valor informado e registra movimento na conta dinheiro
        $statment = "INSERT INTO movimento VALUES(null,8,0,$loja_id,CAST($valor AS DECIMAL(8,2)),$user_id,now(),'Sim',$loja_id,?)";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$historico,PDO::PARAM_STR);
        $result = $stmt->execute();
        $lastId = $this->pdo_conn->lastInsertId();

        //Inverte valor para inserção na conta.
        $valor = -1*$valor;

        $statment = "INSERT INTO carteira VALUES(null,?,1,CAST(? AS DECIMAL(8,2)),NOW())";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$lastId,PDO::PARAM_INT);
        $stmt->bindParam(2,$valor,PDO::PARAM_STR);
        $result = $stmt->execute();
        return $result;
    }

    function getEstado($loja_id) {
        //Verifica se o caixa está aberto ou fechado

    }

    function close($loja_id,$troco_final,$user_id,$retirada) {
        //Registra retirada do fechamento
        if ($retirada > 0) {
            
            $this->sangria($loja_id,$retirada,$user_id,"Retirada de fechamento");
        }

        //Registra o fechamento do caixa
        $historico = "fechamento de caixa";
        $statment = "INSERT INTO movimento VALUES(null,10,0,$loja_id,CAST(? AS DECIMAL(8,2)),$user_id,NOW(),'Sim',$loja_id,?)";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$troco_final,PDO::PARAM_STR);
        $stmt->bindParam(2,$historico,PDO::PARAM_STR);
        $result = $stmt->execute();
        $lastId = $this->pdo_conn->lastInsertId();
        return $lastId;
    }

    function open($loja_id,$troco_inicial,$user_id) {

        //Registra o abertura do caixa
        $historico = "Abertura de caixa";
        $statment = "INSERT INTO movimento VALUES(null,9,0,$loja_id,CAST(? AS DECIMAL(8,2)),$user_id,NOW(),'Sim',$loja_id,?)";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$troco_inicial,PDO::PARAM_STR);
        $stmt->bindParam(2,$historico,PDO::PARAM_STR);
        $result = $stmt->execute();
        $lastId = $this->pdo_conn->lastInsertId();
        return $lastId;
    }

    function ajuste($loja_id,$valor,$user_id,$historico) {

        //Registra ajuste no caixa        
        $statment = "INSERT INTO movimento VALUES(null,11,0,$loja_id,CAST(? AS DECIMAL(8,2)),$user_id,NOW(),'Sim',$loja_id,?)";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$valor,PDO::PARAM_STR);
        $stmt->bindParam(2,$historico,PDO::PARAM_STR);
        $result = $stmt->execute();
        $lastId = $this->pdo_conn->lastInsertId();
        return $lastId;
    }

    public function count() {
         $statment = "SELECT COUNT(id) from movimento WHERE tipo_movimento_id IN (8,9,10,11)";
          $stmt = $this->pdo_conn->prepare($statment);
         $result = $stmt->execute();
         $return = $stmt->fetchAll(PDO::FETCH_NUM);
         return  $return[0][0];

    }
}


?>
