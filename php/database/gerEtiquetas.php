<?php

/*
 * Arquivo que gera a página imprimível para as etiquetas 
 * com códigos de barras.
 */

$codigo_barras = new Barcode();

$codigo_barras->setCodeString('324324');

$codigo_barras->render();


class Barcode {
    
    /*On symbols 0 means narrow 1 means wide bar or space
     * Position 10 is the header of bar code
     * Position 11 is the trailler of bar code 
     */
    
    private $codeString;
    private $narrow = 0.2;
    private $nmultiplier;
    private $valueList = Array();
    private $symbols = 
        Array(
            0=>Array('n','n','W','W','n'),
            1=>Array('W','n','n','n','W'),
            2=>Array('n','W','n','n','W'),
            3=>Array('W','W','n','n','n'),
            4=>Array('n','n','W','n','W'),
            5=>Array('W','n','W','n','n'),
            6=>Array('n','W','W','n','n'),
            7=>Array('n','n','n','W','W'),
            8=>Array('W','n','n','W','n'),
            9=>Array('n','W','n','W','n'),
            10=>Array('n','n','n','n'),
            11=>Array('W','n','n'),
            );
    
            
    
    public function barcode() { //Constructor
        
        
    }
    
    public function render() {
       
        $arrayBarras = Array();
        $arrayBarras = array_merge($arrayBarras,$this->symbols[10]); //adds the header to the code
        
        //This loop adds to arrayBarras the codes for each value in valueList
        for ($i=0;$i< (sizeof($this->valueList) -1) ;$i+=2) {
            for ($j=0;$j<5;$j++) {
                array_push($arrayBarras,$this->symbols[$this->valueList[$i]][$j]);
                array_push($arrayBarras,$this->symbols[$this->valueList[$i+1]][$j]);
            }            
        }        
        $arrayBarras = array_merge($arrayBarras,$this->symbols[11]); //adds the trailler to the code

        echo '';

        echo '<!DOCTYPE html>
<html>
<head>
<link href="../../styles/codeBarPrint.css" rel="stylesheet" type="text/css" media="screen,print" />

<script type="text/javascript">

function fillCanvas() {


for (var index=0;index<('.sizeof($arrayBarras).');index+=2) {

   teste = document.getElementById(\'id\'+index).getContext(\'2d\');

    teste.fillStyle = "rgb(0,0,0)";
    teste.fillRect (0, 0, 2000, 2000);
    try {
        teste = document.getElementById(\'id\'+(index+1)).getContext(\'2d\');
    }catch(err){
        return;
    }
    teste.fillStyle = "rgb(255,255,255)";
    teste.fillRect (0, 0,1000,1000);
    }
    

}








</script>
</head>
<body>
<div id="labelDiv" class="labelDiv">

<div id="codeDiv" class="codeDiv">';

$control = 'b';
$number = 0;

foreach ($arrayBarras as $value) {
    if ($value == 'n') {
        echo '<canvas class="narrow" id="id'.$number.'"></canvas>';       
    } else {
         echo '<canvas class="wide"id="id'.$number.'"></canvas>';
    }
   $number++;
}



echo'
</div>';
echo $this->codeString;

echo'

</div>

<script>fillCanvas();</script>

</body>
</html>';

        
        
        
        echo '<br>'.  $this->getBarcodeWidth($arrayBarras);
        
        
        
       //echo 'array_barcode'.(var_dump($arrayBarras));
    }
    
    public function setCodeString($code) {
      
        if ( strlen($code) < 6) {                           //Add a constant or configure size of code on the fly?            
             $code = str_pad($code, 6, "0", STR_PAD_LEFT);  // adds heading zeroes to the string 
             $this->setValueList(str_split($code,1));       //Creates an array based on the string
        }else {
             $this->setValueList(str_split($code,1));            
        }        
    } 
    
    private function setValueList($list) {        
        $this->valueList=$list;
    }
    
    private function getBarcodeWidth($code) {        
        $totalWidth = 0;
        $wide = 2.5*$this->narrow;        
        $totalWidth += (20*$this->narrow);        
        $arrayOccurences = array_count_values($code);
        $totalWidth += ($arrayOccurences['n']*$this->narrow);
        $totalWidth += ($arrayOccurences['W']*$wide);
        return $totalWidth;
    }
    
   
    
}

class Label {
    
    
    
}

class page {
    
    
    
}




?>
