<?php
session_start();

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('../database/database.php');

class Produto {

    //Attributes
    private $id = 0;
    private $fornecedor_id = 0;
    private $descricao = '';
    private $preco_normal = 0;
    private $preco_promo = 0;
    private $desconto_maximo = 0;
    private $categoria_produto_id = 0;
    private $custo = 0;
    private $qtd_estoque = 0;
    private $ativo = '';
    private $referencia = '';
    private $listDefaultLenght = 17;
    private $countFoundStatement = '';
   

    //Constructor
    public function Produto() {
       $this->pdo_conn=getPdoConnection();
    }

    //Acessors
    public function setId($id) {
        $this->id = intVal($id);
    }

    public function setCountFoundStatement($statement) {
        $this->countFoundStatement=$statement;
    }
    
    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setFornecedor_id($fornecedor_id) {
        $this->fornecedor_id = $fornecedor_id;
    }

    public function setPreco_normal($preco_normal) {
        $this->preco_normal = $preco_normal;
    }

    public function setPreco_promo($preco_promo) {
        $this->preco_promo = $preco_promo;
    }

    public function setDesconto_maximo($desconto_maximo) {
        $this->desconto_maximo = $desconto_maximo;
    }

    public function setCategoria_produto_id($categoria_produto_id) {
        $this->categoria_produto_id = $categoria_produto_id;
    }

    public function setCusto($custo) {
        $this->custo = $custo;
    }

    public function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    public function setReferencia($referencia) {
        $this->referencia = $referencia;
    }

    public function getId() {
        return $this->id;
    }

    public function getFornecedor_id() {
        return $this->fornecedor_id;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getPreco_normal() {
        return $this->preco_normal;
    }

    public function getPreco_promo() {
        return $this->preco_promo;
    }

    public function getDesconto_maximo() {
        return $this->desconto_maximo;
    }

    public function getCategoria_produto_id() {
        return $this->categoria_produto_id;
    }

    public function getCusto() {
        return $this->custo;
    }

    public function getAtivo() {
        return $this->ativo;
    }

    public function getReferencia() {
        return $this->referencia;
    }

    public function get() {

        $statement = "SELECT id,descricao,preco_normal,fornecedor_id";
        $statement .= ",preco_promo,desconto_maximo,categoria_produto_id,";
        $statement .= "custo,referencia,ativo FROM produto ";
        $statement .= "WHERE id=?";
        $stmt = $this->pdo_conn->prepare($statement);
        $stmt->bindParam(1,$this->id,PDO::PARAM_INT);
        $result = $stmt->execute();
        $prod = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $prod = $prod[0];
        $this->setAtivo($prod['ativo']);
        $this->setCategoria_produto_id($prod['categoria_produto_id']);
        $this->setCusto($prod['custo']);
        $this->setDesconto_maximo($prod['desconto_maximo']);
        $this->setDescricao($prod['descricao']);
        $this->setFornecedor_id($prod['fornecedor_id']);
        $this->setPreco_normal($prod['preco_normal']);
        $this->setPreco_promo($prod['preco_promo']);
        $this->setReferencia($prod['referencia']);
        return true;
    }

    public function getList($start, $limit,$property,$direction)  {

        $start = intval($start);
        $limit = intval($limit);

        $sort = 'descricao';
        $dir = 'ASC';

        $statement = "SELECT produto.id,produto.descricao,produto.preco_normal,produto.fornecedor_id";
        $statement .= ",produto.preco_promo,produto.desconto_maximo,produto.categoria_produto_id,";
        $statement .= "produto.custo,produto.referencia,produto.ativo,categoria_produto.descricao AS categoria_produto_descricao,";
        $statement .= "(SELECT SUM(estoque.quantidade) FROM estoque WHERE estoque.produto_id=produto.id) AS qtd_total ";
        $statement .= ",fornecedor.descricao AS fornecedor_descricao FROM produto,categoria_produto,fornecedor ";
        $statement .= " WHERE produto.categoria_produto_id=categoria_produto.id AND fornecedor.id=produto.fornecedor_id  ORDER BY ";
        if ($property) {
            $statement .= "$property $direction LIMIT ?,?";
        }else{
            $statement .= "id ASC LIMIT ?,?";
        }

        $stmt = $this->pdo_conn->prepare($statement);
        $stmt->bindParam(1,$start,PDO::PARAM_INT);
        $stmt->bindParam(2,$limit,PDO::PARAM_INT);
        $result = $stmt->execute();
        return  $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function insert() {
        $statement = "INSERT INTO produto VALUES(null,?,?,CAST(? AS DECIMAL(6,2)),CAST(? AS DECIMAL(6,2)),?,?,CAST(? AS DECIMAL(6,2)),?,?)";
        $stmt = $this->pdo_conn->prepare($statement);
        $stmt->bindParam(1,$this->getFornecedor_id(),PDO::PARAM_INT);
        $stmt->bindParam(2,$this->getDescricao(),PDO::PARAM_STR);
        $stmt->bindParam(3,$this->getPreco_normal(),PDO::PARAM_STR);
        $stmt->bindParam(4,$this->getPreco_promo(),PDO::PARAM_STR);
        $stmt->bindParam(5,$this->getDesconto_maximo(),PDO::PARAM_INT);
        $stmt->bindParam(6,$this->getCategoria_produto_id(),PDO::PARAM_STR);
        $stmt->bindParam(7,$this->getCusto(),PDO::PARAM_STR);
        $stmt->bindParam(8,$this->getAtivo(),PDO::PARAM_STR);
        $stmt->bindParam(9,$this->getReferencia(),PDO::PARAM_STR);
        $result = $stmt->execute();
        return $result;

    }

    public function update() {
        $statement = "UPDATE produto SET ";
        $statement .= "fornecedor_id=?,descricao=?,preco_normal=CAST(? AS DECIMAL(6,2))";
        $statement .= ",preco_promo=CAST(? AS DECIMAL(6,2)),desconto_maximo=?,categoria_produto_id=?";
        $statement .= ",custo=CAST(? AS DECIMAL(6,2)),ativo=?,referencia=? ";
        $statement .= " WHERE id=?";

        $stmt = $this->pdo_conn->prepare($statement);
        $stmt->bindParam(1,$this->getFornecedor_id(),PDO::PARAM_INT);
        $stmt->bindParam(2,$this->getDescricao(),PDO::PARAM_STR);
        $stmt->bindParam(3,$this->getPreco_normal(),PDO::PARAM_STR);
        $stmt->bindParam(4,$this->getPreco_promo(),PDO::PARAM_STR);
        $stmt->bindParam(5,$this->getDesconto_maximo(),PDO::PARAM_INT);
        $stmt->bindParam(6,$this->getCategoria_produto_id(),PDO::PARAM_STR);
        $stmt->bindParam(7,$this->getCusto(),PDO::PARAM_STR);
        $stmt->bindParam(8,$this->getAtivo(),PDO::PARAM_STR);
        $stmt->bindParam(9,$this->getReferencia(),PDO::PARAM_STR);
        $stmt->bindParam(10,$this->getId(),PDO::PARAM_INT);
        $result = $stmt->execute();
        return $result;

    }

    public function search($start,$limit,$property,$value,$sort,$direction) {
        $start=intval($start);
        $limit=intval($limit);
       
        if ($property == 'fornecedor_id') {
             $statement = "SELECT produto.id,produto.descricao,preco_normal";
             $statement .= ",preco_promo,desconto_maximo,categoria_produto_id,";
             $statement .= "categoria_produto.descricao AS categoria_produto_descricao,";
             $statement .= "fornecedor.descricao AS fornecedor_descricao,fornecedor_id,custo,produto.ativo,referencia";
             $statement .= " FROM produto,categoria_produto,fornecedor ";
             $statement .= "WHERE produto.categoria_produto_id=categoria_produto.id AND produto.fornecedor_id=fornecedor.id AND ";
             $statement .= "produto.fornecedor_id=? ORDER BY descricao ASC";
             $stmt = $this->pdo_conn->prepare($statement);
             $stmt->bindParam(1,$value,PDO::PARAM_INT);
        }

        if ($property == 'descricao') {
             $value = '%'.$value.'%';
             $statement = "SELECT SQL_CALC_FOUND_ROWS produto.id,produto.descricao,preco_normal";
             $statement .= ",preco_promo,desconto_maximo,categoria_produto_id,";
             $statement .= "categoria_produto.descricao AS categoria_produto_descricao,";
             $statement .= "fornecedor.descricao AS fornecedor_descricao,fornecedor_id,";
             $statement .= "custo,produto.ativo,referencia, estoque.quantidade AS qtd_loja,(SELECT SUM(estoque.quantidade)";
             $statement .= " FROM estoque WHERE estoque.produto_id=produto.id) AS qtd_total ";
             $statement .= "FROM produto,categoria_produto,fornecedor,estoque ";
             $statement .= "WHERE produto.categoria_produto_id=categoria_produto.id AND fornecedor.id=produto.fornecedor_id ";
             $statement .= "AND produto.id=estoque.produto_id AND estoque.loja_id=2";
             if ($sort) {
                 $statement .= " AND produto.descricao LIKE ? ORDER BY $sort $direction LIMIT ?,? ";
             } else {
                $statement .= " AND produto.descricao LIKE ? ORDER BY descricao ASC LIMIT ?,? ";
             }

             $stmt = $this->pdo_conn->prepare($statement);
             $stmt->bindParam(1,$value,PDO::PARAM_STR);
             $stmt->bindParam(2,$start,PDO::PARAM_INT);
             $stmt->bindParam(3,$limit,PDO::PARAM_INT);
        }

        if ($property == 'id') {            
             $statement = "SELECT produto.id,produto.descricao,preco_normal";
             $statement .= ",preco_promo,desconto_maximo,categoria_produto_id,";
             $statement .= "categoria_produto.descricao AS categoria_produto_descricao,";
             $statement .= "fornecedor.descricao AS fornecedor_descricao,fornecedor_id,";
             $statement .= "custo,produto.ativo,referencia,";
             $statement .= "(SELECT  estoque.quantidade from produto where estoque.loja_id=? AND produto.id=?) AS qtd_loja,";
             $statement .= "(SELECT SUM(estoque.quantidade) FROM estoque WHERE estoque.produto_id=produto.id) AS qtd_total ";
             $statement .= "FROM produto,categoria_produto,fornecedor,estoque ";
             $statement .= "WHERE produto.categoria_produto_id=categoria_produto.id AND fornecedor.id=produto.fornecedor_id ";
             $statement .= "AND produto.id=estoque.produto_id AND estoque.loja_id=?";
             $statement .= " AND produto.id=? ORDER BY descricao ASC";
             $stmt = $this->pdo_conn->prepare($statement);
             $stmt->bindParam(1,$_SESSION['loja'],PDO::PARAM_INT);
             $stmt->bindParam(2,$value,PDO::PARAM_INT);
             $stmt->bindParam(3,$_SESSION['loja'],PDO::PARAM_INT);
             $stmt->bindParam(4,$value,PDO::PARAM_INT);
        }
        
         $result = $stmt->execute();
         return  $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function count() {
         $statement = "SELECT COUNT(id) from produto";
          $stmt = $this->pdo_conn->prepare($statement);
         $result = $stmt->execute();
         $return = $stmt->fetchAll(PDO::FETCH_NUM);
         return  $return[0][0];
    }

    public function countFound() {         
         $statement = 'SELECT FOUND_ROWS()';
         $stmt = $this->pdo_conn->prepare($statement);
         $result = $stmt->execute();
         $return = $stmt->fetchAll(PDO::FETCH_NUM);         
         return  (intval($return[0][0]));
    }



}


?>
