<?php


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('../database/database.php');

class Carteira {

    //Attributes
    private $id = 0;
    private $movimento_id = 0;
    private $conta_id = 0;
    private $valor = 0;
    private $update = '';
    private $listDefaultLenght = 17;

    //Constructor
    public function Carteira() {
       $this->pdo_conn=getPdoConnection();
    }

    //Acessors
    public function setId($id) {
        $this->id = $id;
    }

    public function setMovimentoId($movimentoId) {
        $this->movimento_id = $movimentoId;
    }

    public function setContaId($contaId) {
        $this->conta_id = $contaId;
    }

    public function setValor($valor) {
        $this->valor = $valor;
    }

    public function setUpdate($update) {
        $this->update = $update;
    }

    public function getId() {
        return $this->id;
    }

    public function getMovimentoId() {
        return $this->movimento_id;
    }

    public function getContaId() {
        return $this->conta_id;
    }

    public function getValor() {
        return $this->valor;
    }

    public function getUpdate() {
        return $this->update;
    }
   

    public function getList($start, $limit)  {

        $start = intval($start);
	$limit = intval($limit);

        $sort = 'movimento_id';
        $dir = 'ASC';

        $statment = "SELECT carteira.id,carteira.movimento_id,carteira.conta_id,carteira.valor,carteira.update";
        $statment .= " FROM carteira ORDER BY ";
        $statment .= "$sort $dir LIMIT ?,?";

        $stmt = $this->pdo_conn->prepare($statment);

        $stmt->bindParam(1,$start,PDO::PARAM_INT);
        $stmt->bindParam(2,$limit,PDO::PARAM_INT);

        $result = $stmt->execute();

        return  $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function insert() {
        $statment = "INSERT INTO carteira VALUES(null,?,?,CAST(? AS DECIMAL(8,2)),NOW())";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$this->getMovimentoId(),PDO::PARAM_INT);
        $stmt->bindParam(2,$this->getContaId(),PDO::PARAM_INT);
        $stmt->bindParam(3,$this->getValor(),PDO::PARAM_STR);
        $result = $stmt->execute();
        return $result;

    }

    public function update() {
        $statement = "UPDATE carteira SET ";
        $statement .= "carteira.movimento_id=?,carteira.conta_id=?,carteira.valor=CAST(? AS DECIMAL(6,2)),NOW()";
        $statement .= " WHERE id=?";

        $stmt = $this->pdo_conn->prepare($statement);
        $stmt->bindParam(1,$this->getMovimentoId(),PDO::PARAM_INT);
        $stmt->bindParam(2,$this->getContaId(),PDO::PARAM_INT);
        $stmt->bindParam(3,$this->getValor(),PDO::PARAM_STR);
        $stmt->bindParam(4,$this->getId(),PDO::PARAM_INT);
        $result = $stmt->execute();
        return $result;

    }

 /*   public function search($start, $limit,$property,$value) {

         $value = '%'.$value.'%';

         $statment = "SELECT carteira.id,carteira.descricao,carteira.localizacao";
         $statment .= " FROM carteira WHERE descricao LIKE ? ORDER BY descricao ASC";
         $stmt = $this->pdo_conn->prepare($statment);
         $stmt->bindParam(1,$value,PDO::PARAM_STR);
         $result = $stmt->execute();
         return  $stmt->fetchAll(PDO::FETCH_ASSOC);

    }*/

    public function count() {

         $statment = "SELECT COUNT(id) from carteira";
          $stmt = $this->pdo_conn->prepare($statment);
         $result = $stmt->execute();
         $return = $stmt->fetchAll(PDO::FETCH_NUM);
         return  $return[0][0];

    }

    public function getSituacao($startDate,$endDate) {
        
        $startDate = substr($startDate,0, strpos($startDate, "T"));       
        $startDate.=" 00:00:00";
        
        $endDate = substr($endDate,0, strpos($endDate, "T"));        
        $endDate.=" 23:59:59";

        $statment = "SELECT COALESCE(conta.descricao,'Total de Vendas') AS descricao ,conta_id,sum(valor) AS valor FROM ";
        $statment .= "carteira,conta where (carteira.update between TIMESTAMP(?) AND TIMESTAMP(?) ";
        $statment .= "AND carteira.conta_id=conta.id) AND carteira.movimento_id IN (select id from movimento where tipo_movimento_id=2) ";
        $statment .= "GROUP BY conta.descricao with rollup";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$startDate,PDO::PARAM_STR);
        $stmt->bindParam(2,$endDate,PDO::PARAM_STR);
        $result = $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rs;


    }

}


?>
