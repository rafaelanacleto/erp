<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('../database/database.php');

class Conta {

    //Attributes
    private $id = 0;
    private $descricao = '';
    private $prazo = 0;
    private $listDefaultLenght = 17;

    //Constructor
    public function Conta() {

       $this->pdo_conn=getPdoConnection();

    }

    //Acessors
    public function setId($id) {
        $this->id = $id;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

     public function setPrazo($ocalizacao) {
        $this->prazo = $ocalizacao;
    }

    public function getId() {
        return $this->id;
    }

    public function getDescricao() {
        return $this->descricao;
    }

     public function getPrazo() {
        return $this->prazo;
    }

    public function get() {

    }

    public function getList($start, $limit)  {

        $start = intval($start);
        $limit = intval($limit);

        $sort = 'descricao';
        $dir = 'ASC';

        $statment = "SELECT conta.id,conta.descricao,conta.prazo";
        $statment .= " FROM conta ORDER BY ";
        $statment .= "$sort $dir LIMIT ?,?";

        $stmt = $this->pdo_conn->prepare($statment);

        $stmt->bindParam(1,$start,PDO::PARAM_INT);
        $stmt->bindParam(2,$limit,PDO::PARAM_INT);

        $result = $stmt->execute();

        return  $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function insert() {
        $statment = "INSERT INTO conta VALUES(null,?,?)";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$this->getDescricao(),PDO::PARAM_STR);
        $stmt->bindParam(2,$this->getPrazo(),PDO::PARAM_INT);
        $result = $stmt->execute();
        return $result;

    }

    public function update() {
        $statement = "UPDATE conta SET ";
        $statement .= "conta.descricao=?,conta.prazo=?";
        $statement .= " WHERE id=?";

        $stmt = $this->pdo_conn->prepare($statement);
        $stmt->bindParam(1,$this->getDescricao(),PDO::PARAM_STR);
        $stmt->bindParam(2,$this->getPrazo(),PDO::PARAM_INT);
        $stmt->bindParam(3,$this->getId(),PDO::PARAM_INT);
        $result = $stmt->execute();
        return $result;

    }

    public function search($start, $limit,$property,$value) {

         $value = '%'.$value.'%';

         $statment = "SELECT conta.id,conta.descricao,conta.prazo";
         $statment .= " FROM conta WHERE descricao LIKE ? ORDER BY descricao ASC";
         $stmt = $this->pdo_conn->prepare($statment);
         $stmt->bindParam(1,$value,PDO::PARAM_STR);
         $result = $stmt->execute();
         return  $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function count() {

         $statment = "SELECT COUNT(id) from conta";

          $stmt = $this->pdo_conn->prepare($statment);

         $result = $stmt->execute();

         $return = $stmt->fetchAll(PDO::FETCH_NUM);

         return  $return[0][0];

    }

}


?>
