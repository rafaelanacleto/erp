<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('../database/database.php');

class Cliente {

    //Attributes
    private $id = 0;
    private $nome = '';
    private $nascimento = '';
    private $fone = '';
    private $email = '';
    private $facebook = '';
    private $recebe_email = '';
    private $pdo_conn = '';
    private $listDefaultLenght = 17;

    //Constructor
    public function Cliente() {

        

       $this->pdo_conn=getPdoConnection();

    }

    //Acessors
    public function setId($id) {
        $this->id = $id;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setNascimento($nascimento) {
        $this->nascimento = $nascimento;
    }

    public function setFone($fone) {
        $this->fone = $fone;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setFacebook($facebook) {
        $this->facebook = $facebook;
    }

    public function setRecebeEmail($recebeEmail) {
        $this->recebe_email = $recebeEmail;
    }

    public function getId() {
        return $this->id;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getNascimento() {
        return $this->nascimento;
    }

    public function getFone() {
        return $this->fone;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getFacebook() {
        return $this->facebook;
    }

    public function getRecebe_email() {
        return $this->recebe_email;
    }

    public function get() {

    }

    public function getList($start, $limit)  {

        $start = intval($start);
        $limit = intval($limit);

        $sort = 'nome';
        $dir = 'ASC';
       
        $statment = "SELECT cliente.id,cliente.nome,cliente.nascimento";
        $statment .= ",cliente.fone,cliente.email,cliente.facebook,";
        $statment .= "cliente.recebe_email FROM cliente ORDER BY ";
        $statment .= "$sort $dir LIMIT ?,?";        

        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$start,PDO::PARAM_INT);
        $stmt->bindParam(2,$limit,PDO::PARAM_INT);
        $result = $stmt->execute();        
        return  $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function insert() {
        $statment = "INSERT INTO cliente VALUES(null,?,?,?,?,?,?)";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$this->getNome(),PDO::PARAM_STR);
        $stmt->bindParam(2,$this->getNascimento(),PDO::PARAM_STR);
        $stmt->bindParam(3,$this->getFone(),PDO::PARAM_STR);
        $stmt->bindParam(4,$this->getEmail(),PDO::PARAM_STR);
        $stmt->bindParam(5,$this->getFacebook(),PDO::PARAM_STR);
        $stmt->bindParam(6,$this->getRecebe_email(),PDO::PARAM_STR);
        $result = $stmt->execute();
        return $result;
    }

    public function update() {
        $statement = "UPDATE cliente SET ";
        $statement .= "nome=?,nascimento=?,fone=?,email=?,facebook=?,recebe_email=? ";
        $statement .= " WHERE id=?";        

        $stmt = $this->pdo_conn->prepare($statement);
        $stmt->bindParam(1,$this->getNome(),PDO::PARAM_STR);
        $stmt->bindParam(2,$this->getNascimento(),PDO::PARAM_STR);
        $stmt->bindParam(3,$this->getFone(),PDO::PARAM_STR);
        $stmt->bindParam(4,$this->getEmail(),PDO::PARAM_STR);
        $stmt->bindParam(5,$this->getFacebook(),PDO::PARAM_STR);
        $stmt->bindParam(6,$this->getRecebe_email(),PDO::PARAM_STR);
        $stmt->bindParam(7,$this->getId(),PDO::PARAM_INT);
        $result = $stmt->execute();
        return $result;
    }

    public function search($start, $limit,$property,$value) {

         $value = '%'.$value.'%';
         $statment = "SELECT cliente.id,cliente.nome,cliente.nascimento";
         $statment .= ",cliente.fone,cliente.email,cliente.facebook,";
         $statment .= "cliente.recebe_email FROM cliente ";
         $statment .= "WHERE nome LIKE ? ORDER BY nome ASC";
         $stmt = $this->pdo_conn->prepare($statment);
         $stmt->bindParam(1,$value,PDO::PARAM_STR);         
         $result = $stmt->execute();
         return  $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function count() {
         $statment = "SELECT COUNT(id) from cliente";
          $stmt = $this->pdo_conn->prepare($statment);
         $result = $stmt->execute();
         $return = $stmt->fetchAll(PDO::FETCH_NUM);
         return  $return[0][0];
    }



}


?>
