<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('../database/database.php');

class Item {

    //Attributes
    private $id = 0;
    private $movimento_id = 0;
    private $estoque_id = 0;
    private $valor_unit = 0;
    private $desconto_concedido = 0;
    private $quantidade = 0;
    private $custo = 0;
    private $valor_total = 0;
    private $update = 0;
    private $preco_normal = 0;
    private $preco_promo = 0;

    //Constructor
    public function Item() {
       $this->pdo_conn=getPdoConnection();
    }

    //Acessors
    public function setId($id) {
        $this->id = $id;
    }

    public function setMovimentoId($movimento_id) {
        $this->movimento_id = $movimento_id;
    }

    public function setEstoqueId($estoque_id) {
        $this->estoque_id = $estoque_id;
    }

    public function setValorUnit($valor_unit) {
        $this->valor_unit=$valor_unit;
    }

    public function setDescontoConcedido($desconto_concedido) {
        $this->desconto_concedido = $desconto_concedido;
    }

    public function setQuantidade($quantidade) {
        $this->quantidade = $quantidade;
    }

    public function setCusto($custo) {
        $this->custo = $custo;
    }

    public function setValorTotal($valor_total) {
        $this->valor_total = $valor_total;
    }

    public function setUpdate($update) {
        $this->update = $update;
    }

    public function setPrecoNormal($preco_normal) {
        $this->preco_normal = $preco_normal;
    }

    public function setPrecoPromo($preco_promo) {
        $this->preco_promo = $preco_promo;
    }

    public function getId() {
        return $this->id;
    }

    public function getMovimentoId() {
        return $this->movimento_id;
    }

    public function getEstoqueId() {
        return $this->estoque_id;
    }

    public function getValorUnit() {
        return $this->valor_unit;
    }

    public function getDescontoConcedido() {
        return $this->desconto_concedido;
    }

    public function getQuantidade() {
        return $this->quantidade;
    }

    public function getCusto() {
        return $this->custo;
    }

    public function getValorTotal() {
        return $this->valor_total;
    }

    public function getUpdate() {
        return  $this->update;
    }

    public function getPrecoNormal() {
        return $this->preco_normal;
    }

    public function getPrecoPromo() {
        return $this->preco_promo;
    }

    public function getList($start, $limit)  {
    }

    public function insert() {
        $statment = "INSERT INTO item VALUES (NULL,";
        $statment .= "?,?,CAST(? AS DECIMAL(6,2)),?,?,";
        $statment .= "CAST(? AS DECIMAL(6,2)),CAST(? AS DECIMAL(8,2)),NOW(),";
        $statment .= "CAST(? AS DECIMAL(6,2)),CAST(? AS DECIMAL(6,2)))";
        $stmt = $this->pdo_conn->prepare($statment);
        $stmt->bindParam(1,$this->movimento_id,PDO::PARAM_INT);
        $stmt->bindParam(2,$this->estoque_id,PDO::PARAM_INT);
        $stmt->bindParam(3,$this->valor_unit,PDO::PARAM_STR);
        $stmt->bindParam(4,$this->desconto_concedido,PDO::PARAM_INT);
        $stmt->bindParam(5,$this->quantidade,PDO::PARAM_INT);
        $stmt->bindParam(6,$this->custo,PDO::PARAM_STR);
        $stmt->bindParam(7,$this->valor_total,PDO::PARAM_STR);
        $stmt->bindParam(8,$this->preco_normal,PDO::PARAM_STR);
        $stmt->bindParam(9,$this->preco_promo,PDO::PARAM_STR);       
        $result = $stmt->execute();
        return $result;
    }

    public function update() {       
    }

    public function sync() {    
    }

    public function search($start, $limit,$property,$value) {        
    }

    public function count() {        
    }

}


?>
