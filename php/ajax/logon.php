<?php
session_start();
header('Content-Type: application/json; charset=utf-8');

require_once ('../database/logon.php');

$objLogon = new Logon();
$objLogon->setUserId($_POST['user_id']);
$objLogon->setPwd($_POST['pwd']);
//Salva a loja na sessão
$_SESSION['loja'] = intVal($_POST['loja']);
$result = $objLogon->authenticate();

if ($result == 'OK') {
    $_SESSION['logado']=true;
    $_SESSION['acesso'] = $objLogon->getPermissao();
    echo '{"success":true}';
} else {
    $_SESSION['logado']=false;
    echo '{"success":false,errors: {
    erro: "'.$result.'"}}';   
}

?>
