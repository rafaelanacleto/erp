<?php
header('Content-Type: application/json; charset=utf-8');

require_once ('../database/cliente.php');;

$objCliente = new Cliente();

//route
$action = $_GET['action'];
if(!isset($action)) {
	die();
}
else {
	$action();
}
//


function listCliente() {

    global $objCliente;

    if (isset($_GET['filter'])) {
        
        $json = ($_GET['filter']);       
        $json = str_replace(array('[',']','\\'),'',$json);
        $json = json_decode($json,true);
        $rs = $objCliente->search($_GET['start'], $_GET['limit'],$json['property'],$json['value']);
         $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.sizeof($rs).'}';

    } else {       
        $rs = $objCliente->getList($_GET['start'], $_GET['limit']);
        $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.$objCliente->count().'}';

    } 

    echo $response;

}

function insert() {

     global $objCliente;    
     
      if (isset($_POST['rows'])) {
            $json = ($_POST['rows']);
            $json = str_replace(array('\\\\'),'\\',$json);
            $json = str_replace(array('\\"'),'"',$json);           
            $json = json_decode($json,true);           
            
            $objCliente->setEmail($json['email']);
            $objCliente->setFacebook($json['facebook']);
            $objCliente->setFone($json['fone']);
            $objCliente->setNascimento($json['nascimento']);
            $objCliente->setNome($json['nome']);
            $objCliente->setRecebeEmail($json['recebe_email']);
            
            $result = $objCliente->insert();

           return '{"success":'.$result.'}';          
      }

}

function update() {

       global $objCliente;

      if (isset($_POST['rows'])) {
            $json = ($_POST['rows']);
            $json = str_replace(array('\\\\'),'\\',$json);
            $json = str_replace(array('\\"'),'"',$json);       
            $json = json_decode($json,true);
            

            $objCliente->setId($json['id']);
            $objCliente->setEmail($json['email']);
            $objCliente->setFacebook($json['facebook']);
            $objCliente->setFone($json['fone']);
            $objCliente->setNascimento($json['nascimento']);
            $objCliente->setNome($json['nome']);
            $objCliente->setRecebeEmail($json['recebe_email']);

            $result = $objCliente->update();

           return '{"success":'.$result.'}';
      }

}

?>
