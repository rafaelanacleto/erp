<?php
session_start();
header('Content-Type: application/json; charset=utf-8');

require_once('../database/caixa.php');
require_once('../database/carteira.php');

$objCaixa = new Caixa();
$objCarteira = new Carteira();

//route
$action = $_GET['action'];
if(!isset($action)) {
	die();
}
else {
	$action();
}

 function listAll() {

     global $objCaixa;

    if (isset($_GET['filter'])) {
        $json = ($_GET['filter']);
        $json = str_replace(array('[',']','\\'),'',$json);
        $json = json_decode($json,true);

       if (isset($_GET['sort'])) {
            $jsonS = ($_GET['sort']);
            $jsonS = str_replace(array('\\\\'),'\\',$jsonS);
            $jsonS = str_replace(array('\\"'),'"',$jsonS);
            $jsonS = json_decode($jsonS,true);
            $rs = $objCaixa->search($_GET['start'], $_GET['limit'],$json['property'],$json['value'],$jsonS[0]['property'],$jsonS[0]['direction'],$_SESSION['loja']);
       } else {
        $rs = $objCaixa->search($_GET['start'], $_GET['limit'],$json['property'],$json['value'],null,null,$_SESSION['loja']);
       }

       $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.sizeof($rs).'}';


    } else {

        if (isset($_GET['sort'])) {
            $json = ($_GET['sort']);
            $json = str_replace(array('\\\\'),'\\',$json);
            $json = str_replace(array('\\"'),'"',$json);
            $json = json_decode($json,true);
            $rs = $objCaixa->listAll($_GET['start'], $_GET['limit'],$json[0]['property'],$json[0]['direction'],$_SESSION['loja']);
        } else {
            $rs = $objCaixa->listAll($_GET['start'], $_GET['limit'],null,null,$_SESSION['loja']);
        }
        $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.$objCaixa->count().'}';
    }
    echo $response;



 }

 function getCloseInfo() {

     global $objCaixa;
     $closeInfo = Array();

     foreach ($objCaixa->getLastOpenEntradas() as $key => $value) {
         $closeInfo = array_merge($closeInfo,$value);
     }

     foreach ($objCaixa->getLastOpenSangrias() as $key => $value) {
         $closeInfo = array_merge($closeInfo,$value);
     }

     foreach ($objCaixa->getLastOpen() as $key => $value) {
         $closeInfo = array_merge($closeInfo,$value);
     }

     $response = '{"success": true,"rows":'.json_encode($closeInfo).',"totalCount":'.sizeof($closeInfo).'}';
     echo $response;
 }

 function getOpenInfo() {
     
     global $objCaixa;     
     $openInfo = Array();

     foreach ($objCaixa->getLastCloseInfo() as $key => $value) {
         $openInfo = array_merge($openInfo,$value);
     }
    
     $response = '{"success": true,"rows":'.json_encode($openInfo).',"totalCount":'.sizeof($openInfo).'}';
     echo $response;
 }

 function close() {
     global $objCaixa;
     global $objCarteira;
     if (isset($_POST['rows'])) {
            $json = ($_POST['rows']);
            $json = str_replace(array('\\\\'),'\\',$json);
            $json = str_replace(array('\\"'),'"',$json);
            $json = json_decode($json,true);
            $mov_id = $objCaixa->close($json['origem_loja_id'],$json['troco_final'],$json['usuario_id'],$json['retirada']);
            //Trata sobras e faltas
            $falta = floatval($json['falta']);
            $sobra = floatval($json['sobra']);
            if ($falta > 0) {
                $falta=-1*$falta;
                $mov_id = $objCaixa->ajuste($json['origem_loja_id'], $falta, $json['usuario_id'],'Ajuste de fechamento de Caixa');
                $objCarteira->setMovimentoId($mov_id);
                $objCarteira->setContaId(1); //dinheiro
                $objCarteira->setValor($falta);
                $objCarteira->insert();
            }

            if ($sobra > 0)  {
                $mov_id = $objCaixa->ajuste($json['origem_loja_id'], $sobra, $json['usuario_id'],'Ajuste de fechamento de Caixa');
                $objCarteira->setMovimentoId($mov_id);
                $objCarteira->setContaId(1); //dinheiro
                $objCarteira->setValor($sobra);
                $objCarteira->insert();
            }
            
     }
 }

 function drain() {
     global $objCaixa;
     if (isset($_POST['rows'])) {
            $json = ($_POST['rows']);
            $json = str_replace(array('\\\\'),'\\',$json);
            $json = str_replace(array('\\"'),'"',$json);
            $json = json_decode($json,true);
            $objCaixa->sangria($json['origem_loja_id'],$json['retirada'],$json['usuario_id'],$json['historico']);
     }


    }

 function open() {
    global $objCaixa;
    global $objCarteira;
    if (isset($_POST['rows'])) {
        $json = ($_POST['rows']);
        $json = str_replace(array('\\\\'),'\\',$json);
        $json = str_replace(array('\\"'),'"',$json);
        $json = json_decode($json,true);
        //Trata troco presumido com troco constatado
        $const = floatval($json['troco_inicial_const']);
        $pres = floatval($json['troco_inicial_pres']);
        $diferenca = 0;
        $mov_id = $objCaixa->open($json['origem_loja_id'],$json['troco_inicial_const'],$json['usuario_id']);
        //error_log("consta".$const."pres".$pres,"diff".$diferenca);
        if ($const != $pres) {

            if ($pres > $const) { //lança diferença negativa na carteira
            $diferenca = $const-$pres;
            } else { //Lança diferença positiva na carteira
                $diferenca = $pres-$const;
            }
            $mov_id = $objCaixa->ajuste($json['origem_loja_id'], $diferenca, $json['usuario_id'],'Ajuste de abertura de Caixa');
            $objCarteira->setMovimentoId($mov_id);
            $objCarteira->setContaId(1); //dinheiro
            $objCarteira->setValor($diferenca);
            $objCarteira->insert();
        }

    }
 }


   function getLastOpen() {

    }

    function getLastClose() {

    } 

    function listOpen() {

    }

    function listClose() {

    }

    function listDrains() {

    }



?>