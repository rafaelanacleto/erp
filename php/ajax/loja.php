<?php
header('Content-Type: application/json; charset=utf-8');

require_once ('../database/loja.php');;

$objLoja = new Loja();

//route
$action = $_GET['action']; // listLoja

if(!isset($action)) {

	//
}
else {
	$action();
}



function listLoja() {

    global $objLoja;

    if (isset($_GET['filter'])) {
        $json = ($_GET['filter']);
        $json = str_replace(array('[',']','\\'),'',$json);
        $json = json_decode($json,true);
        $rs = $objLoja->search($_GET['start'], $_GET['limit'],$json['property'],$json['value']);
        $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.sizeof($rs).'}';
    } else {
        $rs = $objLoja->getList($_GET['start'], $_GET['limit']);
        $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.$objLoja->count().'}';
    }
    echo $response;

}

function insert() {

     global $objLoja;
       if (isset($_POST['rows'])) {
            $json = ($_POST['rows']);
            $json = str_replace(array('\\\\'),'\\',$json);
            $json = str_replace(array('\\"'),'"',$json);
            $json = json_decode($json,true);
            $objLoja->setDescricao($json['descricao']);
            $objLoja->setLocalizacao($json['localizacao']);
            $result = $objLoja->insert();
           return '{"success":'.$result.'}';
      }
}

function update() {

       global $objLoja;
       if (isset($_POST['rows'])) {
            $json = ($_POST['rows']);
            $json = str_replace(array('\\\\'),'\\',$json);
            $json = str_replace(array('\\"'),'"',$json);
            $json = json_decode($json,true);
            $objLoja->setId($json['id']);
            $objLoja->setDescricao($json['descricao']);
            $objLoja->setLocalizacao($json['localizacao']);
            $result = $objLoja->update();
           return '{"success":'.$result.'}';
      }

}

?>


