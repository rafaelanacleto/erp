<?php
header('Content-Type: application/json; charset=utf-8');

require_once ('../database/estoque.php');;

$objEstoque= new Estoque();

//route
$action = $_GET['action'];

if(!isset($action)) {

	die();
}
else {
	$action();
}



function getProductPosition() {

    global $objEstoque;

    if (isset($_GET['prod_id'])) {
        $json = ($_GET['prod_id']);
        $json = str_replace(array('[',']','\\'),'',$json);
        $json = json_decode($json,true);
        $rs = $objEstoque->getProductPosition($_GET['prod_id']);
        $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.sizeof($rs).'}';    
    }
    echo $response;

}

?>


