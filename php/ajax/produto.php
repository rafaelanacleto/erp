<?php
header('Content-Type: application/json; charset=utf-8');

require_once ('../database/produto.php');;

$objProduto = new Produto();

//route
$action = $_GET['action'];
if(!isset($action)) {
	die();
}
else {
	$action();
}



function listProduto() {

    global $objProduto;

    if (isset($_GET['filter'])) {
        $json = ($_GET['filter']);
        $json = str_replace(array('[',']','\\'),'',$json);
        $json = json_decode($json,true);

       if (isset($_GET['sort'])) {
            $jsonS = ($_GET['sort']);
            $jsonS = str_replace(array('\\\\'),'\\',$jsonS);
            $jsonS = str_replace(array('\\"'),'"',$jsonS);
            $jsonS = json_decode($jsonS,true);
            $rs = $objProduto->search($_GET['start'], $_GET['limit'],$json['property'],$json['value'],$jsonS[0]['property'],$jsonS[0]['direction']);
       } else {
        $rs = $objProduto->search($_GET['start'], $_GET['limit'],$json['property'],$json['value'],null,null);
       }

       $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.$objProduto->countFound().'}';


    } else {

        if (isset($_GET['sort'])) {
            $json = ($_GET['sort']);
            $json = str_replace(array('\\\\'),'\\',$json);
            $json = str_replace(array('\\"'),'"',$json);
            $json = json_decode($json,true);
            $rs = $objProduto->getList($_GET['start'], $_GET['limit'],$json[0]['property'],$json[0]['direction']);
        } else {
            $rs = $objProduto->getList($_GET['start'], $_GET['limit'],null,null);
        }
        $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.$objProduto->count().'}';       
    }
    echo $response;
}



function insert() {

     global $objProduto;

      if (isset($_POST['rows'])) {
            $json = ($_POST['rows']);
           $json = str_replace(array('\\\\'),'\\',$json);
            $json = str_replace(array('\\"'),'"',$json);
            $json = json_decode($json,true);
            $objProduto->setFornecedor_id($json['fornecedor_id']);
            $objProduto->setDescricao($json['descricao']);
            $objProduto->setPreco_normal($json['preco_normal']);
            $objProduto->setPreco_promo($json['preco_promo']);
            $objProduto->setDesconto_maximo($json['desconto_maximo']);
            $objProduto->setCategoria_produto_id($json['categoria_produto_id']);
            $objProduto->setCusto($json['custo']);
            $objProduto->setAtivo($json['ativo']);
            $objProduto->setReferencia($json['referencia']);
            $result = $objProduto->insert();
           return '{"success":'.$result.'}';
      }
}

function update() {

       global $objProduto;
       if (isset($_POST['rows'])) {
            $json = ($_POST['rows']);
            $json = str_replace(array('[',']','\\'),'',$json);
            $json = json_decode($json,true);
            $objProduto->setId($json['id']);
            $objProduto->setFornecedor_id($json['fornecedor_id']);
            $objProduto->setDescricao($json['descricao']);
            $objProduto->setPreco_normal($json['preco_normal']);
            $objProduto->setPreco_promo($json['preco_promo']);
            $objProduto->setDesconto_maximo($json['desconto_maximo']);
            $objProduto->setCategoria_produto_id($json['categoria_produto_id']);
            $objProduto->setCusto($json['custo']);
            $objProduto->setAtivo($json['ativo']);
            $objProduto->setReferencia($json['referencia']);
            $result =  $objProduto->update();
           return '{"success":'.$result.'}';
      }
}

?>