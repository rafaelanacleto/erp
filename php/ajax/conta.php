<?php
header('Content-Type: application/json; charset=utf-8');

require_once ('../database/conta.php');;

$objConta = new Conta();

//route
$action = $_GET['action'];

if(!isset($action)) {

	die();
}
else {
	$action();
}




function listConta() {

    global $objConta;

    if (isset($_GET['filter'])) {


        $json = ($_GET['filter']);
        $json = str_replace(array('[',']','\\'),'',$json);
        $json = json_decode($json,true);
        $rs = $objConta->search($_GET['start'], $_GET['limit'],$json['property'],$json['value']);
        $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.sizeof($rs).'}';

    } else {

        $rs = $objConta->getList($_GET['start'], $_GET['limit']);
        $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.$objConta->count().'}';

    }

    echo $response;

}

function insert() {

     global $objConta;

       if (isset($_POST['rows'])) {
            $json = ($_POST['rows']);
            $json = str_replace(array('[',']','\\'),'',$json);
            $json = json_decode($json,true);

            $objConta->setDescricao($json['descricao']);
            $objConta->setLocalizacao($json['prazo']);

            $result = $objConta->insert();

           return '{"success":'.$result.'}';
      }

}

function update() {

       global $objConta;

       if (isset($_POST['rows'])) {
            $json = ($_POST['rows']);
            $json = str_replace(array('[',']','\\'),'',$json);
            $json = json_decode($json,true);

            $objConta->setId($json['id']);
            $objConta->setDescricao($json['descricao']);
            $objConta->setLocalizacao($json['prazo']);


            $result = $objConta->update();

           return '{"success":'.$result.'}';
      }

}

?>


