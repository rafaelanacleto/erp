<?php
header('Content-Type: application/json; charset=utf-8');

require_once ('../database/catProduto.php');;

$objCatProduto = new CatProduto();

//route
$action = $_GET['action'];

if(!isset($action)) {

	die();
}
else {
	$action();
}



function listCatProduto() {

    global $objCatProduto;
    if (isset($_GET['filter'])) {


        $json = ($_GET['filter']);
        $json = str_replace(array('[',']','\\'),'',$json);
        $json = json_decode($json,true);
        $rs = $objCatProduto->search($_GET['start'], $_GET['limit'],$json['property'],$json['value']);
         $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.sizeof($rs).'}';

    } else {
        $rs = $objCatProduto->getList($_GET['start'], $_GET['limit']);
        $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.$objCatProduto->count().'}';
    }
    echo $response;
}

function insert() {

     global $objCatProduto;
     if (isset($_POST['rows'])) {
            $json = ($_POST['rows']);
           $json = str_replace(array('\\\\'),'\\',$json);
            $json = str_replace(array('\\"'),'"',$json);
            $json = json_decode($json,true);
            $objCatProduto->setDescricao($json['descricao']);
            $result = $objCatProduto->insert();
           return '{"success":'.$result.'}';
      }
}

function update() {
       global $objCatProduto;
      if (isset($_POST['rows'])) {
            $json = ($_POST['rows']);
            $json = str_replace(array('\\\\'),'\\',$json);
            $json = str_replace(array('\\"'),'"',$json);
            $json = json_decode($json,true);
            $objCatProduto->setId($json['id']);
            $objCatProduto->setDescricao($json['descricao']);
            $result = $objCatProduto->update();
           return '{"success":'.$result.'}';
      }

}

?>


