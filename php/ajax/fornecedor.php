<?php
header('Content-Type: application/json; charset=utf-8');

require_once ('../database/fornecedor.php');

$objFornecedor = new Fornecedor();

//route
$action = $_GET['action'];

if(!isset($action)) {

	die();
}
else {
	$action();
}



function listFornecedor() {
    global $objFornecedor;
    if (isset($_GET['filter'])) {
        $json = ($_GET['filter']);
        $json = str_replace(array('[',']','\\'),'',$json);
        $json = json_decode($json,true);
        $rs = $objFornecedor->search($_GET['start'], $_GET['limit'],$json['property'],$json['value']);
         $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.sizeof($rs).'}';
    } else {
        $rs = $objFornecedor->getList($_GET['start'], $_GET['limit']);
        $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.$objFornecedor->count().'}';
    }
    echo $response;
}

function insert() {

     global $objFornecedor;
       if (isset($_POST['rows'])) {
            $json = ($_POST['rows']);
            $json = str_replace(array('\\\\'),'\\',$json);
            $json = str_replace(array('\\"'),'"',$json);
            $json = json_decode($json,true);
            $objFornecedor->setDescricao($json['descricao']);
            $objFornecedor->setFone($json['fone']);
            $objFornecedor->setEmail($json['email']);
            $objFornecedor->setLocalizacao($json['localizacao']);
            $objFornecedor->setAtivo($json['ativo']);
            $result = $objFornecedor->insert();
           return '{"success":'.$result.'}';
      }

}

function update() {

       global $objFornecedor;

      if (isset($_POST['rows'])) {
            $json = ($_POST['rows']);
            $json = str_replace(array('\\\\'),'\\',$json);
            $json = str_replace(array('\\"'),'"',$json);
            $json = json_decode($json,true);
            $objFornecedor->setId($json['id']);
            $objFornecedor->setDescricao($json['descricao']);
            $objFornecedor->setFone($json['fone']);
            $objFornecedor->setEmail($json['email']);
            $objFornecedor->setLocalizacao($json['localizacao']);
            $objFornecedor->setAtivo($json['ativo']);
            $result =  $objFornecedor->update();
           return '{"success":'.$result.'}';
      }
}

?>