<?php
header('Content-Type: application/json; charset=utf-8');

require_once ('../database/entrada.php');
require_once ('../database/produto.php');
require_once ('../database/movimento.php');
require_once ('../database/estoque.php');
require_once ('../database/item.php');



//route
$action = $_GET['action'];

if(!isset($action)) {

	die();
}
else {
	$action();
}



function listEntrada() {

    $objEntrada = new Entrada();

    if (isset($_GET['filter'])) {


        $json = ($_GET['filter']);
        $json = str_replace(array('[',']','\\'),'',$json);
        $json = json_decode($json,true);
        $rs = $objEntrada->search($_GET['start'], $_GET['limit'],$json['property'],$json['value']);
         $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.sizeof($rs).'}';

    } else {

        $rs = $objEntrada->getList($_GET['start'], $_GET['limit']);
        $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.$objEntrada->count().'}';

    }

    echo $response;

}

function insert() {

    $valor_total_movimento = 0;
    $loja_destino = 0;

    $items = Array();
    $produtos = Array();
    $estoque_updates = Array();
    $movimentos = Array();

    $json_array = Array();

      if (isset($_POST['rows'])) {

            $json = ($_POST['rows']);
            $json = str_replace(array('\\\\'),'\\',$json);
            $json = str_replace(array('\\"'),'"',$json);
            $json = json_decode($json,true);
           

            //Normaliza a entrada
            if (!isset($json[0])) {
                $json_array[0]=$json;
            } else {
                $json_array = $json;
            }

            $objMovimento = New Movimento();
            $objMovimento->setClienteId(0);            
            $objMovimento->setTipoMovimento(1);
            $objMovimento->setOrigemLojaId(0);
            $objMovimento->setUsuarioId($_SESSION['user_id']);
            $objMovimento->setValor(0);
            $objMovimento->setEntregue('Sim');

            foreach ($json_array as $key => $value) {

                //Adicionar os objetos a arrays para a transação
                $objProduto = New Produto();
                $objEstoque = New Estoque();                
                $objItem = New Item();

                //Rotina do produto
                $objProduto->setId($value['produto_id']);
                $objProduto->get();                
                $objEstoque->setProduto_id($objProduto->getId());                
                $q_anterior = doubleval($objEstoque->getQuantidade());               
                $q_atual = doubleval($value['quantidade']);
                $c_anterior = doubleval($objProduto->getCusto());
                $c_atual = doubleval($value['valor_unit']);
                if ($q_anterior > 0) {
                   $valor_ant = ($q_anterior*$c_anterior);
                   $valor_atu = doubleval(($value['valor_unit']*$value['quantidade']));
                   $novo_custo = doubleval(($valor_ant+$valor_atu)/($q_anterior+$q_atual));
                }else{
                   $novo_custo = $value['valor_unit'];
                }                
                $objProduto->setCusto($novo_custo);
                array_push($produtos, $objProduto); //Guarda o produto pronto para update

                //Rotina do estoque
                $objEstoque->setLoja_id($value['destino_estoque_id']);
                $objEstoque->setQuantidade($value['quantidade']);
                array_push($estoque_updates,$objEstoque); //Guarda estoque para sync

                //Rotina do Item
                //$objItem->setMovimentoId($movimento_id); deve ser acrescentado depois
                $objItem->setEstoqueId($objEstoque->getId());
                $objItem->setValorUnit($value['valor_unit']);
                $objItem->setQuantidade($value['quantidade']);
                $objItem->setCusto($value['valor_unit']);
                $objItem->setValorTotal(doubleval(($value['valor_unit']*$value['quantidade'])));
                // Incrementa o valor do movimento.
                $objMovimento->setValor(doubleval($objMovimento->getValor()+$objItem->getValorTotal()));
                //Seta a loja de destino
                $objMovimento->setDestinoLojaId($value['destino_estoque_id']);
                
                $objMovimento->setNota($value['nota']);
                array_push($items, $objItem); //Guarda item para insert

            }

            $movimento_id = $objMovimento->insert();

            foreach ($produtos as $key => $produto) {
                $produto->update();
            }

             foreach ($estoque_updates as $key => $estoque) {

                $items[$key]->setEstoqueId($estoque->sync());
            }

            foreach ($items as $key => $item) {

                $item->setMovimentoId($movimento_id);
                $item->Insert();
            }

           return '{"success":\'\'}';
      }

}

function update() {

       global $objEntrada;

      if (isset($_GET['rows'])) {
            $json = ($_GET['rows']);
            $json = str_replace(array('[',']','\\'),'',$json);
            $json = json_decode($json,true);

            $objEntrada->setId($json['id']);
            $objEntrada->setEmail($json['email']);
            $objEntrada->setFacebook($json['facebook']);
            $objEntrada->setFone($json['fone']);
            $objEntrada->setNascimento($json['nascimento']);
            $objEntrada->setNome($json['nome']);
            $objEntrada->setRecebeEmail($json['recebe_email']);

            $result = $objEntrada->update();

           return '{"success":'.$result.'}';
      }

}

?>