<?php
header('Content-Type: application/json; charset=utf-8');

require_once ('../database/carteira.php');
require_once ('../database/produto.php');
require_once ('../database/movimento.php');
require_once ('../database/estoque.php');
require_once ('../database/item.php');



//route
$action = $_GET['action'];
if(!isset($action)) {
	die();
}
else {
	$action();
}



function listEntrada() {

    $objEntrada = new Entrada();
    if (isset($_GET['filter'])) {
        $json = ($_GET['filter']);
        $json = str_replace(array('[',']','\\'),'',$json);
        $json = json_decode($json,true);
        $rs = $objEntrada->search($_GET['start'], $_GET['limit'],$json['property'],$json['value']);
         $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.sizeof($rs).'}';
    } else {
        $rs = $objEntrada->getList($_GET['start'], $_GET['limit']);
        $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.$objEntrada->count().'}';
    }
    echo $response;
}

function insert() {

    $valor_total_movimento = 0;
    $loja_origem = 0;
    $cliente_id = 0;
    $usuario_id = 0;
    $loja_id = 0;
    $items = Array();
    $estoqueUpdates = Array();
    $carteiraUpdates = Array();

    $jsonArray = $_POST['venda'];
    $jsonArray = str_replace(array('\\'),'',$jsonArray);
    $jsonArray = json_decode($jsonArray,true);

    $cliente_id = $jsonArray['clienteId'];
    $loja_id = $jsonArray['lojaId'];
    $usuario_id = $jsonArray['usuarioId'];

    $movimento = new Movimento();
    $movimento->setTipoMovimento(2); //Venda
    $movimento->setClienteId($cliente_id);
    $movimento->setOrigemLojaId($loja_id);
    $movimento->setUsuarioId($usuario_id);
    $movimento->setDestinoLojaId(0);
    $movimento->setEntregue('Sim');
    
    foreach($jsonArray['listaMovCarteira'] as $k=>$v) {
        $movCarteira = new Carteira();
        $movCarteira->setContaId($v['contaId']);
        $movCarteira->setValor($v['valor']);
        array_push($carteiraUpdates, $movCarteira);
        // Update no valor do movimento
        $movimento->setValor($movimento->getValor()+$v['valor']);

    }

    foreach($jsonArray['listaMovProduto'] as $k=>$v) {
       $estoque = new Estoque();
       $estoque->setLoja_id($loja_id);
       $estoque->setProduto_id($v['prodId']);
       $estoque->setBaixa($v['quantidade']);
       array_push($estoqueUpdates, $estoque);

       $item = new Item();
       $item->setCusto($v['custo']);
       $item->setDescontoConcedido($v['desconto']);
       $item->setQuantidade($v['quantidade']);
       $item->setValorUnit($v['precoFinal']);
       $item->setValorTotal($item->getQuantidade()*$item->getValorUnit());
       $item->setEstoqueId($estoque->retrieveId());
       $item->setPrecoNormal($v['precoNormal']);
       $item->setPrecoPromo($v['precoPromo']);

       array_push($items,$item);
    }

    //Salva no Mysql

    $movimento->insert();

    foreach($estoqueUpdates as $key => $estoqueUpdate) {
        $estoqueUpdate->baixa();
    }

    foreach ($items as $key => $item) {
        $item->setMovimentoId($movimento->getId());
        $item->Insert();
    }

    foreach ($carteiraUpdates as $key => $cartUpdate) {
        $cartUpdate->setMovimentoId($movimento->getId());
        $cartUpdate->insert();
    }

    echo '{"success":true}';
}

function update() {
       global $objEntrada;
      if (isset($_GET['rows'])) {
            $json = ($_GET['rows']);
            $json = str_replace(array('[',']','\\'),'',$json);
            $json = json_decode($json,true);
            $objEntrada->setId($json['id']);
            $objEntrada->setEmail($json['email']);
            $objEntrada->setFacebook($json['facebook']);
            $objEntrada->setFone($json['fone']);
            $objEntrada->setNascimento($json['nascimento']);
            $objEntrada->setNome($json['nome']);
            $objEntrada->setRecebeEmail($json['recebe_email']);
            $result = $objEntrada->update();
           return '{"success":'.$result.'}';
      }
}

?>
