<?php

header('Content-Type: text/javascript; charset=ISO-8859-1');


//include
require_once('../database/database.php');
require_once('../database/movimento.php');
require_once('../database/item.php');
require_once ('../database/produto.php');

$pdo = getPdoConnection();
//set
$objMovimento = new Movimento();
$objItem = new Item();
$objProduto = new Produto();
$valor_mov = 0;

//route
$action = $_GET['action'];

if(!isset($action)) {

	die();
}
else {
	$action();
}

//functions

function createMovimentoEntrada() {

    global $objMovimento;
    global $objItem;
    global $valor_mov;
    global $objProduto;
    
    $tipo_mov = 2;
    $tipo_fin = 7;
    
    $temp = $_POST['items'];
    $data_temp = json_decode($temp,true);
    
    //Tratamento da entrada de um movimento com apenas um item
    
    if (array_key_exists('0', $data_temp)) {
        $data=$data_temp;
    } else {
       $data = array($data_temp);
    }
   

    //Determina valor total do movimento soma dos totais de todos os items do movimento
        foreach ($data as $i=>$item) {
            $qtd = intval($item['quantidade']);
            $vlr = doubleval($item['valor']);
            $valor_mov += ($qtd*$vlr);
        }
   

    //configures o movimento
    $objMovimento->setNominataMov('Recebimento de Mercadoria');
    $objMovimento->setQtdItensMov(sizeof($data));
    $objMovimento->setTipoFinMov($tipo_fin);
    $objMovimento->setTipoMov($tipo_mov);
    $objMovimento->setValorMov($valor_mov);

    //commit to database
    $objMovimento->create();

    //item saving/updating cicle

    //resets the $data array pointer
    reset($data);
    
    foreach ($data as $i=>$item) {        

        //new item's cost calculation

        $qtd = intval($item['quantidade']);
        $vlr = doubleval($item['valor']);
        $valor_item = ($qtd*$vlr);

        //creates register in item table

        $objItem->setItem_mov($objMovimento->getId());
        $objItem->setItem_prod_id($item['idProduto']);
        $objItem->setItem_quant($item['quantidade']);
        $objItem->setItem_ref($item['referenciaProduto']);
        $objItem->setItem_total($valor_item);
        $objItem->setItem_valor($item['valor']);
        $objItem->setItem_nota($item['numNota']);

         //Verifica posição atual do produto no estoque

        $objProduto->setId($item['idProduto']);
        $objProduto->getProduto();

        //calculo do novo valor do produto

        $qtd_ant = intval($objProduto->getQuantidade());
        $vlr_ant = doubleval($objProduto->getCusto());
        $valor_ant = ($qtd_ant*$vlr_ant);
        
        //Elimina problema de qtd/valor zero no produto estoque/entrada
        if ($valor_ant == 0 && $valor_item != 0 ) {
            
            $novo_valor = doubleval($item['valor']);
            
        } else if ($valor_ant != 0 && $valor_item == 0) {
            
             $novo_valor = doubleval($valor_ant);
             
        } else {

            $novo_valor = number_format(doubleval((($valor_ant+$valor_item)/($qtd+$qtd_ant))),2);
        
        }

        //preenche informações do produto para update

        $objProduto->setCusto($novo_valor);
        $objProduto->setQuantidade($objProduto->getQuantidade()+$objItem->getQuantidade());

        //commit to database
        $objProduto->update();
        $objItem->create();
    }
    
    echo '{';
    echo 'success:true';
    echo ',data:""}';

}

function createMovimentoVenda() {

    global $objMovimento;
    global $objItem;
    global $valor_mov;
    global $objProduto;

    $tipo_mov = $_POST['tipo_mov'];
    $tipo_fin = $_POST['tipo_fin_mov'];
    $nominata = $_POST['nominata'];

    $temp = $_POST['items'];
    $data_temp = json_decode($temp,true);

    //Tratamento da entrada de um movimento com apenas um item

    if (array_key_exists('0', $data_temp)) {
        $data=$data_temp;
    } else {
       $data = array($data_temp);
    }


    //Determina valor total do movimento soma dos totais de todos os items do movimento
        foreach ($data as $i=>$item) {
            $qtd = intval($item['quantidade']);
            $vlr = doubleval($item['valor']);
            $valor_mov += ($qtd*$vlr);
        }


    //configures o movimento
    $objMovimento->setNominataMov($nominata);
    $objMovimento->setQtdItensMov(sizeof($data));
    $objMovimento->setTipoFinMov($tipo_fin);
    $objMovimento->setTipoMov($tipo_mov);
    $objMovimento->setValorMov($valor_mov);

    //commit to database
    $objMovimento->create();

    //item saving/updating cicle

    //resets the $data array pointer
    reset($data);

    foreach ($data as $i=>$item) {

        //new item's cost calculation

        $qtd = intval($item['quantidade']);
        $vlr = doubleval($item['valor']);
        $valor_item = ($qtd*$vlr);

        //creates register in item table

        $objItem->setItem_mov($objMovimento->getId());
        $objItem->setItem_prod_id($item['idProduto']);
        $objItem->setItem_quant($item['quantidade']);
        $objItem->setItem_ref($item['referenciaProduto']);
        $objItem->setItem_total($valor_item);
        $objItem->setItem_valor($item['valor']);
        $objItem->setItem_preco_venda($item['valor']);
       
         //Verifica posição atual do produto no estoque

        $objProduto->setId($item['idProduto']);
        $objProduto->getProduto();

        //preenche informações do produto para update
        
        $objProduto->setQuantidade($objProduto->getQuantidade()-$objItem->getQuantidade());

        //commit to database
        $objProduto->update();
        $objItem->create();
    }

    echo '{';
    echo 'success:true';
    echo ',data:""}';
}

function createItem() {
}

function saveMovimento() {
}

function listMovimento() {
}

function countMovimento() {
}

function findMovimento() {
}

function getItemsMovimento() {

}



?>
