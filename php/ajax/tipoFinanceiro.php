<?php

header('Content-Type: text/javascript; charset=ISO-8859-1');


//include
require_once('../database/database.php');
require_once('../database/tipoFinanceiro.php');

//set
$objTipoFinanceiro = new TipoFinanceiro();

//route
$action = $_GET['action'];

if(!isset($action)) {

	die();
}
else {
	$action();
}

//functions

function countTipoFinanceiro() {
        global $objTipoFinanceiro;
        return $objTipoFinanceiro->countTipoFinanceiro($_GET);
}

function listTipoFinanceiro() {
	global $objTipoFinanceiro;
        $rs = $objTipoFinanceiro->listTipoFinanceiro($_GET);

	echo '{"success": true,';
	echo	'"rows":' .sql2json($rs);
	echo 	',"totalCount":' . countTipoFinanceiro();
	echo '}';
}

?>
