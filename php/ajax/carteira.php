<?php
header('Content-Type: application/json; charset=utf-8');

require_once ('../database/carteira.php');;

$objCarteira = new Carteira();

//route
$action = $_GET['action'];

if(!isset($action)) {

	die();
}
else {
	$action();
}

function listCarteira() {

    global $objCarteira;

    if (isset($_GET['filter'])) {
        $json = ($_GET['filter']);
        $json = str_replace(array('[',']','\\'),'',$json);
        $json = json_decode($json,true);
        $rs = $objCarteira->search($_GET['start'], $_GET['limit'],$json['property'],$json['value']);
        $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.sizeof($rs).'}';

    } else {

        $rs = $objCarteira->getList($_GET['start'], $_GET['limit']);
        $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.$objCarteira->count().'}';

    }

    echo $response;

}

function insert() {

     global $objCarteira;

      if (isset($_GET['rows'])) {
            $json = ($_GET['rows']);
            $json = str_replace(array('[',']','\\'),'',$json);
            $json = json_decode($json,true);

            $objCarteira->setMovimentoId($json['movimento_id']);
            $objCarteira->setContaId($json['conta_id']);
            $objCarteira->setValor($json['valor']);

            $result = $objCarteira->insert();

           return '{"success":'.$result.'}';
      }

}

function update() {

       global $objCarteira;

      if (isset($_GET['rows'])) {
            $json = ($_GET['rows']);
            $json = str_replace(array('[',']','\\'),'',$json);
            $json = json_decode($json,true);

            $objCarteira->setId($json['id']);
            $objCarteira->setMovimentoId($json['movimento_id']);
            $objCarteira->setContaId($json['conta_id']);
            $objCarteira->setValor($json['valor']);


            $result = $objCarteira->update();

           return '{"success":'.$result.'}';
      }

}

function getSituacao() {

    global $objCarteira;

    $rs = $objCarteira->getSituacao($_GET['startDate'],$_GET['endDate']);
    $response = '{"success": true,"rows":'.json_encode($rs).',"totalCount":'.sizeof($rs).'}';
    echo $response;



}
?>


